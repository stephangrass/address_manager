# TYPO3 Adressmanager

Mit dem Adressmanager können Adressen bzw. Personendatensätze verwaltet und im Frontend dargestellt werden.

## Features (Basis-Version)
* Zahlreiche Datenfelder stehen zur Verfügung, neben Standardfeldern wie Name und Kontaktinformationen auch
  spezielle Felder für SEO, Social Media-Felder, Geo-Koordinaten und Öffnungszeiten.
* Bilder und Dateianhänge können verwendet werden.
* Adressen können mittels Gruppen (z. B. Abteilungen), Organisationen (z. B. Firmen) und Positionen
  (z. B. Berufsbezeichnungen) strukturiert werden.
* Über eine Volltextsuche können Adressen gesucht werden.
* Über eine mehrstufige Filterfunktion können Adressen im Frontend gefiltert werden.
* Flüssige Darstellung von Listen und Filtern mittels AJAX.
* Adressen können auf einer Karte angezeigt werden (Google-Maps).
* Ein ViewHelper ermöglicht die Anzeige von Adressen in anderen Fluid-Templates (Dritt-Anbieter-Erweiterungen oder
  auch Seiten-Templates).
* Geo-Koordinaten können direkt aus dem TYPO3-Backend heraus ermittelt werden, während Adressen erstellt oder
  bearbeitet werden ("Map-Wizard").

## Features (Pro-Version)
* Ein spezielles Backendmodul ermöglicht die benutzerfreundliche Verwaltung von Adressen und Verknüpfungen.
* Eine Frontend-Management-Funktion ermöglicht die Frontend-Bearbeitung von Adressen.
* Ein Scheduler-Task zur automatischen Ermittlung von Geo-Koordinaten ist enthalten.
* Eine Umkreissuche ist enthalten.

