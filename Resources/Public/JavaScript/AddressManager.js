/**
 * TYPO3 AddressManager
 *
 * @author Thomas Deuling, coding.ms
 * @constructor
 */
var AddressManager = function () {

    /**
     * @type {jQuery} searchForm
     */
    this.searchForm = null;

    /**
     * @type {jQuery} positionSelect
     */
    this.positionSelect = null;

    /**
     * @type {jQuery} groupSelect
     */
    this.groupSelect = null;

    /**
     * @type {jQuery} organisationSelect
     */
    this.organisationSelect = null;
    this.limitSelect = null;
    this.searchField = null;
    this.locationField = null;
    this.distanceSelect = null;


    this.locationAutoComplete = null;

    // Maps
    this.map = null;
    this.mapsMarker = [];
    this.mapBounds = null;
    this.mapsMarkerCluster = null;
    this.countryRestriction = [];

    /**
     * and/or
     * @type {string}
     */
    this.searchFieldConcat = 'or';

    /**
     * Uid of the address list plugin
     * @type {number}
     */
    this.listUid = 0;

    /**
     * Search parameter dummy
     * @type {{tx_addressmanager_jsonapi: {group: number, position: number, organisation: number, limit: number, searchWord: string, listUid: number}}}
     */
    this.searchData = {
        tx_addressmanager_jsonapi: {
            group: 0,
            position: 0,
            organisation: 0,
            offset: 0,
            limit: 15,
            searchWord: '',
            customString: '',
            listUid: 0,
            latitude: 0.0,
            longitude: 0.0,
            distance: 0
        }
    };

    /**
     * Initiate object
     */
    this.initialize = function () {
        //
        // Fetch initial values for offset and limit
        var listWrapper = jQuery('.address-list-wrapper');
        if (listWrapper.length > 0) {
            this.searchData.tx_addressmanager_jsonapi.offset = parseInt(listWrapper.attr('data-address-list-offset'), 10);
            this.searchData.tx_addressmanager_jsonapi.limit = parseInt(listWrapper.attr('data-address-list-limit'), 10);
        }
        //
        // Filter form found -> bind own submit and read list uid
        this.searchForm = jQuery('.tx-address-manager #address-search-form');
        if (this.searchForm.length === 1) {
            this.searchForm.on("submit", function () {
                address_manager.refreshData();
                return false;
            });
            this.listUid = jQuery('.tx-address-manager #address-list-uid');
            if (this.searchForm.length === 1) {
                address_manager.searchData.tx_addressmanager_jsonapi.listUid = parseInt(this.listUid.val(), 10);
            }
        }
        //
        // group
        this.groupSelect = jQuery('.tx-address-manager #address-group-select');
        if (this.groupSelect.length === 1) {
            this.groupSelect.val(0);
            this.groupSelect.on("change", function () {
                var newValue = parseInt(jQuery(this).val(), 10);
                var filterSelectNumber = jQuery(this).data('order');
                if (newValue > 0) {
                    // activate next select
                    if (filterSelectNumber === 0) {
                        address_manager.resetFilterSelect(1);
                        address_manager.activateFilterSelect(1);
                        address_manager.resetFilterSelect(2);
                    }
                    else if (filterSelectNumber === 1) {
                        address_manager.resetFilterSelect(2);
                        address_manager.activateFilterSelect(2);
                    }
                }
                else {
                    // deactivate all higher selects
                    if (filterSelectNumber === 0) {
                        address_manager.resetFilterSelect(1);
                        address_manager.resetFilterSelect(2);
                    }
                    else if (filterSelectNumber === 1) {
                        address_manager.resetFilterSelect(2);
                    }
                }
                // reset search word field
                if (filterSelectNumber === 0 && address_manager.searchFieldConcat === 'or') {
                    address_manager.searchField.val('');
                    address_manager.searchData.tx_addressmanager_jsonapi.searchWord = '';
                    address_manager.searchData.tx_addressmanager_jsonapi.customString = '';
                }
                address_manager.searchData.tx_addressmanager_jsonapi.group = newValue;
                address_manager.searchData.tx_addressmanager_jsonapi.offset = 0;
                address_manager.refreshData();
            });
        }
        // position
        this.positionSelect = jQuery('.tx-address-manager #address-position-select');
        if (this.positionSelect.length === 1) {
            this.positionSelect.val(0);
            this.positionSelect.on("change", function () {
                var newValue = parseInt(jQuery(this).val(), 10);
                var filterSelectNumber = jQuery(this).data('order');
                if (newValue > 0) {
                    // activate next select
                    if (filterSelectNumber === 0) {
                        address_manager.resetFilterSelect(1);
                        address_manager.activateFilterSelect(1);
                        address_manager.resetFilterSelect(2);
                    }
                    else if (filterSelectNumber === 1) {
                        address_manager.resetFilterSelect(2);
                        address_manager.activateFilterSelect(2);
                    }
                }
                else {
                    // deactivate all higher selects
                    if (filterSelectNumber === 0) {
                        address_manager.resetFilterSelect(1);
                        address_manager.resetFilterSelect(2);
                    }
                    else if (filterSelectNumber === 1) {
                        address_manager.resetFilterSelect(2);
                    }
                }
                // reset search word field
                if (filterSelectNumber === 0 && address_manager.searchFieldConcat === 'or') {
                    address_manager.searchField.val('');
                    address_manager.searchData.tx_addressmanager_jsonapi.searchWord = '';
                    address_manager.searchData.tx_addressmanager_jsonapi.customString = '';
                }
                address_manager.searchData.tx_addressmanager_jsonapi.position = newValue;
                address_manager.searchData.tx_addressmanager_jsonapi.offset = 0;
                address_manager.refreshData();

            });
        }
        // organisation
        this.organisationSelect = jQuery('.tx-address-manager #address-organisation-select');
        if (this.organisationSelect.length === 1) {
            this.organisationSelect.val(0);
            this.organisationSelect.on("change", function () {
                var newValue = parseInt(jQuery(this).val(), 10);
                var filterSelectNumber = jQuery(this).data('order');
                if (newValue > 0) {
                    // activate next select
                    if (filterSelectNumber === 0) {
                        address_manager.resetFilterSelect(1);
                        address_manager.activateFilterSelect(1);
                        address_manager.resetFilterSelect(2);
                    }
                    else if (filterSelectNumber === 1) {
                        address_manager.resetFilterSelect(2);
                        address_manager.activateFilterSelect(2);
                    }
                }
                else {
                    // deactivate all higher selects
                    if (filterSelectNumber === 0) {
                        address_manager.resetFilterSelect(1);
                        address_manager.resetFilterSelect(2);
                    }
                    else if (filterSelectNumber === 1) {
                        address_manager.resetFilterSelect(2);
                    }
                }
                // reset search word field
                if (filterSelectNumber === 0 && address_manager.searchFieldConcat === 'or') {
                    address_manager.searchField.val('');
                    address_manager.searchData.tx_addressmanager_jsonapi.searchWord = '';
                    address_manager.searchData.tx_addressmanager_jsonapi.customString = '';
                }
                address_manager.searchData.tx_addressmanager_jsonapi.organisation = newValue;
                address_manager.searchData.tx_addressmanager_jsonapi.offset = 0;
                address_manager.refreshData();
            });
        }
        //
        // limit
        this.limitSelect = jQuery('.tx-address-manager #address-limit-select');
        if (this.limitSelect.length === 1) {
            this.limitSelect.on("change", function () {
                address_manager.searchData.tx_addressmanager_jsonapi.limit = parseInt(jQuery(this).val(), 10);
                address_manager.searchData.tx_addressmanager_jsonapi.offset = 0;
                address_manager.refreshData();
            });
        }
        //
        // name/word
        this.searchField = jQuery('.tx-address-manager #address-search-word');
        if (this.searchField.length === 1) {
            this.searchField.val('');
            this.searchField.on('input', function (event) {
                if (address_manager.searchFieldConcat === 'or') {
                    address_manager.resetFilterSelect(0);
                    address_manager.resetFilterSelect(1);
                    address_manager.resetFilterSelect(2);
                }
                address_manager.searchData.tx_addressmanager_jsonapi.offset = 0;
                address_manager.searchData.tx_addressmanager_jsonapi.searchWord = jQuery(this).val();
                /*
                 Don't use key up, because some users press enter and the the search is send twice!!
                 //address_manager.setSearchWord(jQuery(this).attr('value'));
                 if (address_manager.searchTimer) {
                 clearTimeout(address_manager.searchTimer);
                 }
                 address_manager.searchTimer = setTimeout(function() {
                 address_manager.refreshData();
                 }, 1000);
                 */
                if (event.keyCode === 13) {
                    if(jQuery('[data-search-word-submit-on-enter]').attr('data-search-word-submit-on-enter') === '1') {
                        address_manager.refreshData();
                    }
                }
            });
        }
        //
        // custom string
        this.customStringField = jQuery('.tx-address-manager #address-custom-string');
        if (this.customStringField.length === 1) {
            this.customStringField.val('');
            this.customStringField.on('input', function () {
                if (address_manager.searchFieldConcat === 'or') {
                    address_manager.resetFilterSelect(0);
                    address_manager.resetFilterSelect(1);
                    address_manager.resetFilterSelect(2);
                }
                address_manager.searchData.tx_addressmanager_jsonapi.offset = 0;
                address_manager.searchData.tx_addressmanager_jsonapi.customString = jQuery(this).val();
                /*
                 * Don't use key up, because some users press enter and the the search is send twice!!
                 */
            });
        }
        //
        // Location
        this.locationField = jQuery('.tx-address-manager #address-location-field');
        if (this.locationField.length === 1) {
            address_manager.searchData.tx_addressmanager_jsonapi.latitude = jQuery('.tx-address-manager #address-latitude-field').val();
            address_manager.searchData.tx_addressmanager_jsonapi.longitude = jQuery('.tx-address-manager #address-longitude-field').val();
            //
            // Create the auto-complete object, restricting the search to geographical location types.
            var options = {
                componentRestrictions: {
                    country: this.countryRestriction
                }
            };
            if(typeof google !== 'undefined') {
                this.locationAutoComplete = new google.maps.places.Autocomplete(this.locationField[0], options);
                //
                // When the user selects an address from the dropdown, populate the address fields in the form.
                this.locationAutoComplete.addListener('place_changed', function () {
                    // Get the place details from the autocomplete object.
                    var place = address_manager.locationAutoComplete.getPlace();
                    if (typeof place.geometry !== 'undefined') {
                        address_manager.searchData.tx_addressmanager_jsonapi.latitude = place.geometry.location.lat();
                        address_manager.searchData.tx_addressmanager_jsonapi.longitude = place.geometry.location.lng();
                    }
                    else {
                        address_manager.searchData.tx_addressmanager_jsonapi.latitude = 0.0;
                        address_manager.searchData.tx_addressmanager_jsonapi.longitude = 0.0;
                    }
                    //
                    // Insert into HTML fields.
                    // That is necessary in case of sending values by page request.
                    jQuery('#address-latitude-field').val(address_manager.searchData.tx_addressmanager_jsonapi.latitude);
                    jQuery('#address-longitude-field').val(address_manager.searchData.tx_addressmanager_jsonapi.longitude);
                    //
                    // Ensure to show first page of result
                    address_manager.searchData.tx_addressmanager_jsonapi.offset = 0;
                });
                //
                // Get geo location on enter
                this.locationField.on('keydown', function (event) {
                    //
                    // If location field is empty, reset latitude/longitude
                    if(jQuery(this).val() === '') {
                        address_manager.searchData.tx_addressmanager_jsonapi.latitude = 0.0;
                        address_manager.searchData.tx_addressmanager_jsonapi.longitude = 0.0;
                    }
                    if (event.keyCode === 13) {
                        //
                        jQuery(event.target).blur();
                        var firstResult = '';
                        var firstItemText = jQuery('.pac-container .pac-item:first span:eq(3)').text();
                        if (firstItemText === '') {
                            firstResult = jQuery('.pac-container .pac-item:first .pac-item-query').text();
                        }
                        else {
                            firstResult = jQuery('.pac-container .pac-item:first .pac-item-query').text() + ', ' + firstItemText;
                        }
                        //
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({'address': firstResult}, function (results, status) {
                            if (status === google.maps.GeocoderStatus.OK) {
                                place = results[0];
                                event.target.value = firstResult;
                                if (typeof place.geometry !== 'undefined') {
                                    address_manager.searchData.tx_addressmanager_jsonapi.latitude = place.geometry.location.lat();
                                    address_manager.searchData.tx_addressmanager_jsonapi.longitude = place.geometry.location.lng();
                                }
                                else {
                                    address_manager.searchData.tx_addressmanager_jsonapi.latitude = 0.0;
                                    address_manager.searchData.tx_addressmanager_jsonapi.longitude = 0.0;
                                }
                                //
                                // Insert into HTML fields.
                                // That is necessary in case of sending values by page request.
                                jQuery('#address-latitude-field').val(address_manager.searchData.tx_addressmanager_jsonapi.latitude);
                                jQuery('#address-longitude-field').val(address_manager.searchData.tx_addressmanager_jsonapi.longitude);
                                //
                                // Ensure to show first page of result
                                address_manager.searchData.tx_addressmanager_jsonapi.offset = 0;
                            }
                        });
                        if(jQuery('[data-location-submit-on-enter]').attr('data-location-submit-on-enter') === '1') {
                            address_manager.refreshData();
                        }
                    }
                });
            }
        }
        //
        // Distance
        this.distanceSelect = jQuery('.tx-address-manager #address-distance-select');
        if (this.distanceSelect.length === 1) {
            address_manager.searchData.tx_addressmanager_jsonapi.distance = parseInt(this.distanceSelect.val());
            this.distanceSelect.on("change", function () {
                var selectedValue = jQuery('option:selected', jQuery(this)).val();
                address_manager.searchData.tx_addressmanager_jsonapi.distance = parseInt(selectedValue, 10);
                address_manager.searchData.tx_addressmanager_jsonapi.offset = 0;
            });
        }
        //
        // Reset second and third select field
        address_manager.resetFilterSelect(1);
        address_manager.resetFilterSelect(2);
        //
        // Initialize google map
        if(typeof google !== 'undefined') {
            // initialize only if google maps library available
            address_manager.initializeGoogleMap();
        }
        //
        // Initialize pagination
        address_manager.refreshPagination();
        //
        // Bind events
        jQuery(window).on('address_manager.result', address_manager.onResult);
        jQuery(window).on('address_manager.no_result', address_manager.onNoResult);
        //
        // Fire event after initialization
        jQuery.event.trigger({
            type: 'address_manager.after_initialize'
        });
        //
        // Process address group initial value
        var reloadWithInitialValues = false;
        if(this.searchField.length === 1) {
            var searchFieldInitialValue = this.searchField.attr('data-initial-value');
            if(searchFieldInitialValue !== '') {
                this.searchField.val(searchFieldInitialValue).trigger('input');
                reloadWithInitialValues = true;
            }
        }
        if(this.customStringField.length === 1) {
            var customStringFieldInitialValue = this.customStringField.attr('data-initial-value');
            if(customStringFieldInitialValue !== '') {
                this.customStringField.val(customStringFieldInitialValue).trigger('input');
                reloadWithInitialValues = true;
            }
        }
        if (this.groupSelect.length === 1) {
            var groupInitialValue = this.groupSelect.attr('data-initial-value');
            if(groupInitialValue !== '') {
                groupInitialValue = parseInt(groupInitialValue, 10);
                if(groupInitialValue > 0) {
                    this.groupSelect.val(groupInitialValue).change();
                    reloadWithInitialValues = false;
                }
            }
        }
        if(reloadWithInitialValues) {
            this.searchForm.submit();
        }
    };

    /**
     * Initialize google map
     */
    this.initializeGoogleMap = function () {
        //
        // List map
        var mapWrapper = jQuery('#address-list-map');
        if (mapWrapper.length > 0) {
            var maxZoom = parseInt(mapWrapper.attr('data-max-zoom'), 10);
            //
            // Prepare country restriction
            var countryRestrictionString = mapWrapper.attr('data-country-restriction');
            if(typeof countryRestrictionString !== 'undefined') {
                var countryRestrictionArray = countryRestrictionString.split(',');
                for(var i=0 ; i<countryRestrictionArray.length ; i++) {
                    this.countryRestriction.push(countryRestrictionArray[i].trim());
                }
            }
            //
            address_manager.mapBounds = new google.maps.LatLngBounds();
            address_manager.infoWindow = new google.maps.InfoWindow();
            var latlng = new google.maps.LatLng(51.96066490000001, 7.626134699999966);
            var myOptions = {
                zoom: 12,
                center: latlng,
                zoomControl: true,
                scaleControl: true,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            address_manager.map = new google.maps.Map(document.getElementById("address-list-map-canvas"), myOptions);
            address_manager.map.setOptions({styles: address_manager.getMapStyles(mapWrapper)});
            // Process address items
            jQuery.each(jQuery('.address-list-item'), function (key) {
                var latitude = parseFloat(jQuery(this).data('map-latitude'));
                var longitude = parseFloat(jQuery(this).data('map-longitude'));
                latitude = address_manager.validateLatitude(latitude);
                longitude = address_manager.validateLongitude(longitude);
                var svg = jQuery(this).data('map-marker');
                if (latitude !== 0 && longitude !== 0 && !isNaN(latitude) && !isNaN(longitude)) {
                    // Create address marker
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(latitude, longitude),
                        icon: address_manager.getIcon(mapWrapper, svg),
                        map: address_manager.map
                    });
                    // remind marker for bounds
                    address_manager.mapBounds.extend(marker.position);
                    // Show popup window on click
                    if (jQuery(this).data('map-tooltip') !== '') {
                        google.maps.event.addListener(marker, 'click', (function (marker, key, item) {
                            return function () {
                                address_manager.infoWindow.setContent(jQuery(item).data('map-tooltip'));
                                address_manager.infoWindow.open(address_manager.map, marker);
                            }
                        })(marker, key, jQuery(this)));
                    }
                    // Remind used markers
                    address_manager.mapsMarker.push(marker);
                }
            });
            // Clustering
            var clusteringActive = parseInt(mapWrapper.attr('data-clustering-active'), 10);
            if (clusteringActive === 1) {
                var clusteringImagePath = mapWrapper.attr('data-clustering-image-path');
                address_manager.mapsMarkerCluster = new MarkerClusterer(this.map, address_manager.mapsMarker, {imagePath: clusteringImagePath});
            }
            address_manager.refreshMarkerCluster();
            // Now fit the map to the newly inclusive bounds
            address_manager.map.fitBounds(address_manager.mapBounds);
            // Set zoom
            google.maps.event.addListenerOnce(address_manager.map, 'bounds_changed', function () {
                //if (this.getZoom() > maxZoom) {
                //	this.setZoom(maxZoom);
                //}
            });
        }
        //
        // Detail map
        mapWrapper = jQuery('#address-detail-map');
        if (mapWrapper.length > 0) {
            var zoom = parseInt(mapWrapper.attr('data-zoom'), 10);
            var latitude = parseFloat(mapWrapper.attr('data-latitude'));
            var longitude = parseFloat(mapWrapper.attr('data-longitude'));
            var svg = mapWrapper.data('map-marker');
            address_manager.infoWindow = new google.maps.InfoWindow();
            address_manager.map = new google.maps.Map(mapWrapper[0], {
                zoom: zoom,
                center: new google.maps.LatLng(latitude, longitude),
                zoomControl: true,
                scaleControl: true,
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            address_manager.map.setOptions({styles: address_manager.getMapStyles(mapWrapper)});
            // Create address marker
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(latitude, longitude),
                icon: address_manager.getIcon(mapWrapper, svg),
                map: address_manager.map
            });
            // Show popup window on click
            if (mapWrapper.data('tooltip') !== '') {
                address_manager.infoWindow.setContent(mapWrapper.data('tooltip'));
                google.maps.event.addListener(marker, 'click', function () {
                    address_manager.infoWindow.open(address_manager.map, marker);
                });
            }
        }
    };

    /**
     * Rebuilding google map after refreshing address data
     */
    this.rebuildGoogleMap = function () {
        var mapWrapper = jQuery('#address-list-map');
        if (mapWrapper.length > 0) {
            var maxZoom = parseInt(mapWrapper.attr('data-max-zoom'), 10);
            // Remove All Markers
            while (address_manager.mapsMarker.length) {
                address_manager.mapsMarker.pop().setMap(null);
            }
            // Reset bounds
            address_manager.mapBounds = new google.maps.LatLngBounds();
            // Set current marker
            jQuery.each(jQuery('.address-list-item'), function (key) {
                var latitude = parseFloat(jQuery(this).data('map-latitude'));
                var longitude = parseFloat(jQuery(this).data('map-longitude'));
                latitude = address_manager.validateLatitude(latitude);
                longitude = address_manager.validateLongitude(longitude);
                var svg = jQuery(this).data('map-marker');
                if (latitude !== 0 && longitude !== 0 && !isNaN(latitude) && !isNaN(longitude)) {
                    // Create address marker
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(latitude, longitude),
                        icon: address_manager.getIcon(mapWrapper, svg),
                        map: address_manager.map
                    });
                    // remind marker for bounds
                    address_manager.mapBounds.extend(marker.position);
                    if (jQuery(this).data('map-tooltip') !== '') {
                        google.maps.event.addListener(marker, 'click', (function (marker, key, item) {
                            return function () {
                                address_manager.infoWindow.setContent(jQuery(item).data('map-tooltip'));
                                address_manager.infoWindow.open(address_manager.map, marker);
                            }
                        })(marker, key, jQuery(this)));
                    }
                    // remind marker
                    address_manager.mapsMarker.push(marker);
                }
            });
            address_manager.refreshMarkerCluster();
            // Now fit the map to the newly inclusive bounds
            address_manager.map.fitBounds(address_manager.mapBounds);
            // Set zoom
            google.maps.event.addListenerOnce(address_manager.map, 'bounds_changed', function () {
                //if (this.getZoom() > maxZoom) {
                //	this.setZoom(maxZoom);
                //}
            });
            if (address_manager.map.getZoom() === 22) {
                address_manager.map.setZoom(maxZoom);
            }
        }
    };

    this.refreshMarkerCluster = function () {
        var mapWrapper = jQuery('#address-list-map');
        var clusteringActive = parseInt(mapWrapper.attr('data-clustering-active'), 10);
        if (clusteringActive === 1) {
            address_manager.mapsMarkerCluster.clearMarkers();
            address_manager.mapsMarkerCluster.addMarkers(address_manager.mapsMarker);
        }
    };

    /**
     * Reset select boxes, but not the limit select box
     * @param number 0-3
     */
    this.resetFilterSelect = function (number) {
        var selector = '.tx-address-manager select[data-order=\'' + number + '\']';
        var select = jQuery(selector);
        for (var i = 0; i < select.length; i++) {
            var forSelect = jQuery(select[i]);
            // Don't process limit select box
            if (forSelect.attr('id') !== 'address-limit-select') {
                if (number > 0) {
                    forSelect.attr('disabled', 'disabled');
                }
                forSelect.val('0');
                var id = forSelect.attr('id').replace('address-', '').replace('-select', '');
                address_manager.searchData.tx_addressmanager_jsonapi[id] = 0;
            }
        }
    };

    /**
     * Reset of all filter and shown data
     */
    this.resetAllData = function() {
        for(var i=0 ; i<4 ; i++) {
            address_manager.resetFilterSelect(i);
        }
        jQuery('#address-search-word').val('');
        jQuery('#address-custom-string').val('');
        jQuery('#address-location-field').val('');
        jQuery('#address-latitude-field').val('');
        jQuery('#address-longitude-field').val('');
        address_manager.searchData.tx_addressmanager_jsonapi.searchWord = '';
        address_manager.searchData.tx_addressmanager_jsonapi.customString = '';
        address_manager.searchData.tx_addressmanager_jsonapi.latitude = 0.0;
        address_manager.searchData.tx_addressmanager_jsonapi.longitude = 0.0;
        address_manager.searchData.tx_addressmanager_jsonapi.offset = 0;
        address_manager.refreshData();
    };

    /**
     * Activates the filter select by giving number
     * @param number 0-3
     */
    this.activateFilterSelect = function (number) {
        var selector = '.tx-address-manager select[data-order=\'' + number + '\']';
        var select = jQuery(selector);
        if (select.length > 0) {
            select.val('0').removeAttr('disabled');
        }
    };

    /**
     * Toggles the loading information
     */
    this.toggleLoading = function () {
        jQuery('.address-list').animate({
            height: 'toggle',
            opacity: 'toggle'
        }, 400);
        var addressListLoading = jQuery('.address-list-loading');
        var addressListLoadingClass = addressListLoading.attr('data-hidden-class');
        if (addressListLoading.length > 0) {
            if (addressListLoading.hasClass(addressListLoadingClass)) {
                addressListLoading.removeClass(addressListLoadingClass);
            }
            else {
                addressListLoading.addClass(addressListLoadingClass);
            }
        }
    };

    /**
     * Refresh displayed addresses and filter
     */
    this.refreshData = function () {
        this.toggleLoading();
        jQuery.ajax({
            dataType: 'json',
            url: address_manager.searchForm.attr('action'),
            data: address_manager.searchData,
            method: 'POST',
            success: function (result) {
                address_manager.toggleLoading();
                if (result.list.items.length > 0) {
                    // Remove previously displayed addresses
                    jQuery('.address-list').html('');
                    // Insert new searched addresses
                    jQuery.each(result.list.items, function (key, address) {
                        var html = atob(address.html);
                        html = decodeURIComponent(escape(html));
                        jQuery('.address-list').append(html);
                    });
                    //
                    // Filter the second filter
                    if (typeof(result.list.filterFieldSecond) !== 'undefined') {
                        if (typeof(result.list.filterFieldSecond.items) !== 'undefined') {
                            var selectSecond = jQuery('select[data-order=\'1\']');
                            if (selectSecond.length > 0) {
                                // Set please choose label
                                var labelSecond = jQuery('select[data-order=\'1\'] option[value=\'0\']').html();
                                selectSecond.html('<option value="0">' + labelSecond + '</option>');
                                // Set items
                                jQuery.each(result.list.filterFieldSecond.items, function (index, item) {
                                    var selected = '';
                                    if (parseInt(result.list.filterFieldSecond.selected, 10) === parseInt(item.uid, 10)) {
                                        selected = 'selected="selected"';
                                    }
                                    // rebuild select options
                                    var html = '<option value="' + item.uid + '" ' + selected + '>' + item.title + '</option>';
                                    jQuery('select[data-order=\'1\']').append(html);
                                });
                            }
                        }
                    }
                    //
                    // Filter the third filter
                    if (typeof(result.list.filterFieldThird) !== 'undefined') {
                        var selectThird = jQuery('select[data-order=\'2\']');
                        if (typeof(result.list.filterFieldThird.items) !== 'undefined') {
                            if (selectThird.length > 0) {
                                selectThird.removeAttr('disabled', 'disabled');
                                // Set please choose label
                                var labelThird = jQuery('select[data-order=\'2\'] option[value=\'0\']').html();
                                selectThird.html('<option value="0">' + labelThird + '</option>');
                                // Set items
                                jQuery.each(result.list.filterFieldThird.items, function (index, item) {
                                    var selected = '';
                                    if (parseInt(result.list.filterFieldThird.selected, 10) === parseInt(item.uid, 10)) {
                                        selected = 'selected="selected"';
                                    }
                                    var html = '<option value="' + item.uid + '" ' + selected + '>' + item.title + '</option>';
                                    jQuery('select[data-order=\'2\']').append(html);
                                });
                            }
                        }
                        else {
                            if (selectThird.length > 0) {
                                selectThird.attr('disabled', 'disabled');
                            }
                        }
                    }
                    jQuery.event.trigger({
                        type: 'address_manager.result',
                        result: result
                    });
                }
                else {
                    jQuery.event.trigger({
                        type: 'address_manager.no_result',
                        result: result
                    });
                }
                //
                // Insert current count
                var listWrapper = jQuery('.address-list-wrapper');
                listWrapper.attr('data-address-list-count', result.list.count);
                //
                // Rebuild google map
                address_manager.rebuildGoogleMap();
                //
                // Refresh pagination
                address_manager.refreshPagination();
            }
        });
    };

    this.onResult = function (result) {
        jQuery('#address-list-map').slideDown();
        jQuery('.address-list-pagination').slideDown();
    };
    this.onNoResult = function (result) {
        /**
         * @todo translate, in <div> in template verlagern und nur ein/aus schalten!
         */
        jQuery('.address-list').html('<div class="address-list-no-results col-md-12"><div class="alert alert-info">Es wurden keine passenden Ansprechpartner gefunden</div></div>');
        jQuery('#address-list-map').slideUp();
        jQuery('.address-list-pagination').slideUp();
    };

    this.refreshPagination = function () {
        jQuery.each(jQuery('.address-list-pagination'), function () {
            var listWrapper = jQuery('.address-list-wrapper');
            var pagination = jQuery(this);
            var count = parseInt(listWrapper.attr('data-address-list-count'), 10);
            var limit = address_manager.searchData.tx_addressmanager_jsonapi.limit;
            var offset = address_manager.searchData.tx_addressmanager_jsonapi.offset;
            var pageCount = Math.ceil(count / limit);
            var currentPageNo = (offset / limit) + 1;
            //
            // Remove all page items
            jQuery('.page-item.clone', pagination).remove();
            //
            // Currently on first page
            var paginationPrevious = jQuery('.page-item.previous');
            if (currentPageNo === 1) {
                paginationPrevious.addClass('disabled');
            }
            else if (paginationPrevious.hasClass('disabled')) {
                paginationPrevious.removeClass('disabled');
            }
            if (!paginationPrevious.hasClass('initialized')) {
                paginationPrevious.addClass('initialized');
                paginationPrevious.on('click', function () {
                    var limit = address_manager.searchData.tx_addressmanager_jsonapi.limit;
                    var offset = address_manager.searchData.tx_addressmanager_jsonapi.offset;
                    // Increase offset
                    offset -= limit;
                    if (offset >= 0) {
                        address_manager.searchData.tx_addressmanager_jsonapi.offset = offset;
                        address_manager.refreshData();
                    }
                    return false;
                });
            }
            //
            // Currently on last page
            var paginationNext = jQuery('.page-item.next');
            if (currentPageNo === pageCount) {
                paginationNext.addClass('disabled');
            }
            else if (paginationNext.hasClass('disabled')) {
                paginationNext.removeClass('disabled');
            }
            if (!paginationNext.hasClass('initialized')) {
                paginationNext.addClass('initialized');
                paginationNext.on('click', function () {
                    var count = parseInt(listWrapper.attr('data-address-list-count'), 10);
                    var limit = address_manager.searchData.tx_addressmanager_jsonapi.limit;
                    var offset = address_manager.searchData.tx_addressmanager_jsonapi.offset;
                    // Increase offset
                    offset += limit;
                    if (offset < count) {
                        address_manager.searchData.tx_addressmanager_jsonapi.offset = offset;
                        address_manager.refreshData();
                    }
                    return false;
                });
            }
            //
            // Build page links
            var pageNo = 1;
            for (var i = 0; i < count; i += limit) {
                var pageItemTemplate = jQuery('.page-item.template', pagination).clone();
                pageItemTemplate.removeClass('template').addClass('clone').removeAttr('style');
                jQuery('a', pageItemTemplate).attr('data-page-no', pageNo).text(pageNo);
                // is current page
                if (pageNo === currentPageNo) {
                    pageItemTemplate.addClass('active');
                }
                jQuery(pageItemTemplate).insertBefore(jQuery('.page-item.template', pagination));
                pageNo++;
            }
            jQuery('.page-item.clone a', pagination).on('click', function () {
                var pageLink = jQuery(this);
                var pageNo = parseInt(pageLink.attr('data-page-no'), 10);
                var limit = address_manager.searchData.tx_addressmanager_jsonapi.limit;
                address_manager.searchData.tx_addressmanager_jsonapi.offset = limit * (pageNo - 1);
                address_manager.refreshData();
                return false;
            });
        });
    };

    this.getIcon = function (mapWrapper, svg) {
        var width = parseInt(mapWrapper.attr('data-marker-width'), 10);
        var height = parseInt(mapWrapper.attr('data-marker-height'), 10);
        return icon = {
            anchor: new google.maps.Point((width / 2), height),
            scaledSize: new google.maps.Size(width, height),
            origin: new google.maps.Point(0, 0),
            url: 'data:image/svg+xml;charset=UTF-8;base64,' + btoa(svg)
        };
    };

    this.getMapStyles = function (mapWrapper) {
        //
        // Primary color
        var color = jQuery('body').attr('data-color-primary');
        if (typeof color === 'undefined') {
            color = '#ddb0b7';
        }
        // Map style
        var style = mapWrapper.attr('data-style');
        // Default styles
        // Gray scale
        var mapStyles = [
            {
                featureType: 'landscape',
                stylers: [
                    {saturation: -100},
                    {gamma: 0.90}
                ]
            },
            {
                featureType: 'road',
                stylers: [
                    {saturation: -100},
                    {gamma: 0.90}
                ]
            },
            {
                featureType: 'poi',
                stylers: [
                    {saturation: -100},
                    {gamma: 0.90}
                ]
            },
            {
                featureType: 'transit',
                stylers: [
                    {saturation: -100},
                    {gamma: 0.90}
                ]
            },
            {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [
                    {hue: '#5aa7d5'}
                ]
            }
        ];
        if (style === 'primaryColored') {
            mapStyles = [
                {
                    stylers: [
                        {hue: color},
                        {saturation: -70},
                        {lightness: 30},
                        {gamma: 1}
                    ]
                }
            ];
        }
        else if (style === 'normal') {
            mapStyles = [];
        }
        return mapStyles;
    };

    this.validateLatitude = function (latitude) {
        if (latitude > 85 || latitude < -85) {
            latitude = 0.0;
        }
        return latitude;
    };

    this.validateLongitude = function (longitude) {
        if (longitude > 180 || longitude < -180) {
            longitude = 0.0;
        }
        return longitude;
    };

};
var address_manager = new AddressManager();
jQuery(document).ready(function () {
    address_manager.initialize();
});
