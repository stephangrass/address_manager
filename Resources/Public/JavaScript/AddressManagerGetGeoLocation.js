/**
 * Fetch user geo location on button click and submit search form.
 *
 * Example button:
 * <div id="get-location-button" data-location-text="Eigener Standort">Get location</div>
 *
 */
var AddressManagerGetGeoLocation = {

    /**
     *
     */
    eventType: 'click',

    /**
     * Initiate the shop object
     */
    initialize: function () {
        //
        // Identify event type!
        if (jQuery('html.touch').length === 1) {
            AddressManagerGetGeoLocation.eventType = 'touchend';
        }
        //
        // Get geo location button
        let getLocationButton = jQuery('#get-location-button');
        if(getLocationButton.length > 0) {
            if (navigator.geolocation) {
                getLocationButton.on(AddressManagerGetGeoLocation.eventType, function() {
                    navigator.geolocation.getCurrentPosition(
                        AddressManagerGetGeoLocation.processCurrentPosition,
                        AddressManagerGetGeoLocation.processCurrentPositionError
                    );
                });

            } else {
                // Geo location is not supported by current browser
                getLocationButton.hide();
            }
        }
    },

    /**
     * Insert passed geo coordinates and request
     * @param location
     */
    processCurrentPosition: function(location) {
        // Insert values for page request
        jQuery('#address-latitude-field').val(location.coords.latitude);
        jQuery('#address-longitude-field').val(location.coords.longitude);
        // Insert values for AJAX request
        address_manager.searchData.tx_addressmanager_jsonapi.latitude = location.coords.latitude;
        address_manager.searchData.tx_addressmanager_jsonapi.longitude = location.coords.longitude;
        jQuery('#address-location-field').val(jQuery('#get-location-button').attr('data-location-text'));
        jQuery('#address-search-form').submit();
    },

    /**
     * Error handling for geo location feature
     * @param error
     */
    processCurrentPositionError: function(error) {
        let message = '';
        switch(error.code) {
            case error.PERMISSION_DENIED:
                message = "User denied the request for Geolocation.";
                break;
            case error.POSITION_UNAVAILABLE:
                message = "Location information is unavailable.";
                break;
            case error.TIMEOUT:
                message = "The request to get user location timed out.";
                break;
            case error.UNKNOWN_ERROR:
                message = "An unknown error occurred.";
                break;
        }
        alert(message);
    }

};

jQuery(document).ready(function () {
    AddressManagerGetGeoLocation.initialize();
});
