define(['jquery'], function (jQuery) {

    /**
     * Container for the marker
     * @type {null}
     */
    AddressManager.marker = null;

    /**
     * Uid of the current record
     * @type {number}
     */
    AddressManager.uid = 0;

    /**
     * Container for google's geocoder
     * @type {null}
     */
    AddressManager.geocoder = null;

    /**
     * Container for google's geocoder
     * @type {null}
     */
    AddressManager.map = null;

    /**
     * Geo coordinates
     * @type {null}
     */
    AddressManager.latlng = null;

    /**
     *
     * @constructor
     */
    function AddressManager() {
        //
        AddressManager.map = jQuery('#address-manager-map');
        AddressManager.uid = parseInt(AddressManager.map.attr('data-uid'), 10);
        // Initialize geocoder
        AddressManager.geocoder = new google.maps.Geocoder();
        //
        let latitude = 51.9637546;
        let longitude = 7.6081952;
        AddressManager.latlng = new google.maps.LatLng(latitude, longitude);
        //
        // Create map
        AddressManager.map = new google.maps.Map(document.getElementById('address-manager-map'), {
            // Initial zoom
            zoom: 12,
            center: AddressManager.latlng,
            zoomControl: true,
            scaleControl: true,
            scrollwheel: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        //
        // Update marker
        AddressManager.updateMarker(latitude, longitude);
        //
        // Bind events on buttons
        jQuery.each(jQuery('#address-manager-input *[data-event]'), function () {
            let button = jQuery(this);
            let event = button.attr('data-event');
            switch (event) {
                case 'grabAddressFromRecord':
                    button.on('click', AddressManager.grabAddressFromRecord);
                    break;
                case 'getGeoLocation':
                    button.on('click', AddressManager.getGeoLocation);
                    break;
                case 'showMap':
                    button.on('click', AddressManager.showMap);
                    break;
            }
        });
    }

    /**
     * Show the current geo location on map
     */
    AddressManager.showMap = function () {
        jQuery('#address-manager-map').show();
        let latitude = parseFloat(AddressManager.getFieldValue('map_latitude'));
        let longitude = parseFloat(AddressManager.getFieldValue('map_longitude'));
        AddressManager.updateMarker(latitude, longitude);
    };

    /**
     * Grab geo location from Google-Maps API
     */
    AddressManager.getGeoLocation = function () {
        let addressMap = jQuery('#address-manager-map');
        addressMap.show();
        let address = AddressManager.getFieldValue('map_wizard');
        if (address !== '') {
            AddressManager.geocoder.geocode({address: address}, function (results, status) {
                if (status === google.maps.GeocoderStatus.OK) {
                    let latlng = results[0].geometry.location;
                    AddressManager.setFieldValue('map_latitude', latlng.lat());
                    AddressManager.setFieldValue('map_longitude', latlng.lng());
                    AddressManager.updateMarker(latlng.lat(), latlng.lng());
                    AddressManager.clearMessage();
                } else if (status === google.maps.GeocoderStatus.ZERO_RESULTS) {
                    let message = addressMap.attr('data-message-address-not-found');
                    AddressManager.showMessage(message + ': ' + address, 'info');
                } else {
                    let message = addressMap.attr('data-message-geocoder-status');
                    AddressManager.showMessage(message + ': ' + status, 'danger');
                }
            });
        } else {
            let message = addressMap.attr('data-message-enter-an-address');
            AddressManager.showMessage(message, 'danger');
        }
    };

    /**
     * Update marker
     *
     * @param latitude
     * @param longitude
     */
    AddressManager.updateMarker = function (latitude, longitude) {
        AddressManager.latlng = new google.maps.LatLng(latitude, longitude);
        // Reset marker
        if (AddressManager.marker) {
            AddressManager.marker.setMap(null);
        }
        // Set new marker
        AddressManager.marker = new google.maps.Marker({
            map: AddressManager.map,
            position: AddressManager.latlng,
            draggable: true
        });
        // Set the center of the map
        AddressManager.map.setCenter(AddressManager.latlng);
        // Add event listener for marker-repositioning
        google.maps.event.addListener(AddressManager.marker, "dragend", function () {
            let latitude = AddressManager.marker.getPosition().lat();
            let longitude = AddressManager.marker.getPosition().lng();
            AddressManager.setFieldValue('map_latitude', latitude);
            AddressManager.setFieldValue('map_longitude', longitude);
            AddressManager.setFieldValue('map_wizard', '');
            AddressManager.clearMessage();
        });
        setTimeout(function () {
            google.maps.event.trigger(AddressManager.map, 'resize');
            setTimeout(function () {
                AddressManager.map.setCenter(AddressManager.latlng);
            }, 500);
        }, 500);
    };

    /**
     * Build address string by record fields
     */
    AddressManager.grabAddressFromRecord = function () {
        let addressParts = [];
        // Country available?
        let country = AddressManager.getFieldValue('country');
        if (typeof country === 'string' && country !== '') {
            addressParts.push(country + ',');
        }
        // Region available?
        let region = AddressManager.getFieldValue('region');
        if (typeof region === 'string' && region !== '') {
            addressParts.push(region + ',');
        }
        // Postal code available?
        let postalCode = AddressManager.getFieldValue('postalCode');
        if (typeof postalCode === 'string' && postalCode !== '') {
            addressParts.push(postalCode);
        }
        // City available?
        let city = AddressManager.getFieldValue('city');
        if (typeof city === 'string' && city !== '') {
            addressParts.push(city);
        }
        // Address available?
        let address = AddressManager.getFieldValue('address');
        if (typeof address === 'string' && address !== '') {
            addressParts.push(address);
        }
        AddressManager.setFieldValue('map_wizard', addressParts.join(' '));
        AddressManager.clearMessage();
    };

    /**
     * Get field value
     *
     * @param field
     * @returns string
     */
    AddressManager.getFieldValue = function (field) {
        let value = '';
        let fieldIdentifier = 'data[tx_addressmanager_domain_model_address][' + AddressManager.uid + '][' + field + ']';
        let fieldObject = jQuery('[data-formengine-input-name="' + fieldIdentifier + '"]');
        if (fieldObject.length > 0) {
            value = fieldObject.val();
        }
        return value;
    };

    /**
     * Set field value
     *
     * @param field
     * @param value
     */
    AddressManager.setFieldValue = function (field, value) {
        let fieldIdentifier = 'data[tx_addressmanager_domain_model_address][' + AddressManager.uid + '][' + field + ']';
        TBE_EDITOR.isChanged = 1;
        // Modify the "field has changed" info by adding a class to the container element (based on palette or main field)
        let formField = jQuery('[name="' + fieldIdentifier + '"]');
        let fieldObject = jQuery('[data-formengine-input-name="' + fieldIdentifier + '"]');
        if (!formField.is(fieldObject)) {
            fieldObject.val(value);
            fieldObject.triggerHandler('change');
        }
        formField.val(value);
        let paletteField = formField.closest('.t3js-formengine-palette-field');
        paletteField.addClass('has-change');
    };

    /**
     * Show a message
     *
     * @param message Message text
     * @param status Valid values: info, danger, success
     */
    AddressManager.showMessage = function (message, status) {
        jQuery('#address-manager-message')
            .html(jQuery('<span />', {'class': 'text-' + status}).text(message))
            .show();
    };

    /**
     * Clear message
     */
    AddressManager.clearMessage = function () {
        jQuery('#address-manager-message').html('');
    };

    return new AddressManager();

});
