var openStreetMap = {
    map: null,
    layer_mapnik: null,
    layer_tah: null,
    layer_markers: null,

    jumpTo: function (lon, lat, zoom) {
        var x = this.Lon2Merc(lon);
        var y = this.Lat2Merc(lat);
        map.setCenter(new OpenLayers.LonLat(x, y), zoom);
        return false;
    },

    Lon2Merc: function (lon) {
        return 20037508.34 * lon / 180;
    },

    Lat2Merc: function (lat) {
        var PI = 3.14159265358979323846;
        lat = Math.log(Math.tan( (90 + lat) * PI / 360)) / (PI / 180);
        return 20037508.34 * lat / 180;
    },

    addMarker: function (layer, lon, lat, popupContentHTML) {
        var ll = new OpenLayers.LonLat(this.Lon2Merc(lon), this.Lat2Merc(lat));
        var feature = new OpenLayers.Feature(layer, ll);
        feature.closeBox = true;
        feature.popupClass = OpenLayers.Class(OpenLayers.Popup.FramedCloud, {minSize: new OpenLayers.Size(300, 180) } );
        feature.data.popupContentHTML = popupContentHTML;
        feature.data.overflow = "hidden";

        var marker = new OpenLayers.Marker(ll);
        marker.feature = feature;

        var markerClick = function(evt) {
            if (this.popup == null) {
                this.popup = this.createPopup(this.closeBox);
                map.addPopup(this.popup);
                this.popup.show();
            } else {
                this.popup.toggle();
            }
            OpenLayers.Event.stop(evt);
        };
        marker.events.register("mousedown", feature, markerClick);

        layer.addMarker(marker);
        //map.addPopup(feature.createPopup(feature.closeBox));
    },

    getCycleTileURL: function (bounds) {
        var res = this.map.getResolution();
        var x = Math.round((bounds.left - this.maxExtent.left) / (res * this.tileSize.w));
        var y = Math.round((this.maxExtent.top - bounds.top) / (res * this.tileSize.h));
        var z = this.map.getZoom();
        var limit = Math.pow(2, z);

        if (y < 0 || y >= limit)
        {
            return null;
        } else {
            x = ((x % limit) + limit) % limit;
            return this.url + z + "/" + x + "/" + y + "." + this.type;
        }
    },

    drawmap: function(dataElementId, mapElementId, zoom) {
        let dataElement = '';
        OpenLayers.Lang.setCode('de');

        map = new OpenLayers.Map(mapElementId, {
            projection: new OpenLayers.Projection("EPSG:900913"),
            displayProjection: new OpenLayers.Projection("EPSG:4326"),
            controls: [
                new OpenLayers.Control.Navigation(),
                new OpenLayers.Control.PanZoomBar()],
            maxExtent:
                new OpenLayers.Bounds(
                    -20037508.34,
                    -20037508.34,
                    20037508.34,
                    20037508.34
                ),
            numZoomLevels: 18,
            maxResolution: 156543,
            units: 'meters'
        });

        layer_mapnik = new OpenLayers.Layer.OSM.Mapnik("Mapnik");
        layer_markers = new OpenLayers.Layer.Markers("Address", {
            projection: new OpenLayers.Projection("EPSG:4326"),
            visibility: true,
            displayInLayerSwitcher: false
        });
        map.addLayers([layer_mapnik, layer_markers]);

        // Take the data from the dataElement
        // Add markers and set position and zoom of the map
        let lat = 0;
        let lon = 0;
        if(jQuery('#' + dataElementId).length > 0) {
            // detail view
            dataElement = document.querySelector('#' + dataElementId);
            lat = parseFloat(dataElement.dataset.latitude);
            lon = parseFloat(dataElement.dataset.longitude);
            let popUpContentHtml = dataElement.dataset.tooltip;
            this.addMarker(layer_markers, lon, lat, popUpContentHtml);
        } else {
            // list view
            jQuery('.' + dataElementId).each(function () {
                lat = parseFloat(this.dataset.mapLatitude);
                lon = parseFloat(this.dataset.mapLongitude);
                let popUpContentHtml = this.dataset.mapTooltip;
                openStreetMap.addMarker(layer_markers, lon, lat, popUpContentHtml);
            })
        }

        this.jumpTo(lon, lat, zoom);
    }
}

jQuery(document).ready(function() {
    if(jQuery('#address-detail-map').length > 0) {
        openStreetMap.drawmap(
            'address-detail-map',
            'address-detail-map',
            12
        );
    }
    if(jQuery('.address-list-item').length > 0 && jQuery('#address-list-map').length > 0) {
        openStreetMap.drawmap(
            'address-list-item',
            'address-list-map-canvas',
            5
        );
    }
});
