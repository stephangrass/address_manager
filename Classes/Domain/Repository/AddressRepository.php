<?php

namespace CodingMs\AddressManager\Domain\Repository;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AddressManager\Domain\Model\Address;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Persistence\QueryInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use CodingMs\AddressManager\Domain\Model\AddressGroup;
use CodingMs\AddressManager\Domain\Model\AddressPosition;
use CodingMs\AddressManager\Domain\Model\AddressOrganisation;

/**
 *
 *
 * @package address_manager
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */
class AddressRepository extends Repository
{

    /**
     * Default ordering by sorting, because select box entries can be sorted easily
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => QueryInterface::ORDER_ASCENDING
    ];

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper
     */
    protected function getDataMapper()
    {
        /** @var \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper $dataMapper */
        $dataMapper = $this->objectManager->get(DataMapper::class);
        return $dataMapper;
    }

    /**
     * @param array $filter
     * @param string $sortBy Sort by field
     * @param string $order Sorting order ASC or DESC
     * @param int $offset Offset of address items
     * @param int $limit Limit of address items
     * @param int $storagePid Storage pid - if empty storage will be ignored
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findAllByFilter(array $filter = [], $sortBy = 'name', $order = 'ASC', $offset = 0, $limit = 0, int $storagePid = 0)
    {
        $query = $this->createQuery();
        /** By using the json API, the record storage isn't available! */
        $query->getQuerySettings()->setRespectStoragePage(false);
        $constraints = $this->getConstraints($query, $filter, $storagePid);
        if (!empty($constraints)) {
            if (count($constraints) > 1) {
                $query->matching(
                    $query->logicalAnd($constraints)
                );
            } else {
                $query->matching($constraints[0]);
            }
        }
        // Sort by start date
        if ($order === 'ASC') {
            $orderings = [$sortBy => QueryInterface::ORDER_ASCENDING];
            if ($sortBy === 'genderThenName') {
                $orderings = ['gender' => QueryInterface::ORDER_ASCENDING, 'name' => QueryInterface::ORDER_ASCENDING];
            }
            if ($sortBy === 'cityThenName') {
                $orderings = ['city' => QueryInterface::ORDER_ASCENDING, 'name' => QueryInterface::ORDER_ASCENDING];
            }
            if ($sortBy === 'lastnameThenFirstname') {
                $orderings = ['lastName' => QueryInterface::ORDER_ASCENDING, 'firstName' => QueryInterface::ORDER_ASCENDING];
            }
            $query->setOrderings($orderings);
        } else {
            $orderings = [$sortBy => QueryInterface::ORDER_DESCENDING];
            if ($sortBy === 'genderThenName') {
                $orderings = ['gender' => QueryInterface::ORDER_DESCENDING, 'name' => QueryInterface::ORDER_DESCENDING];
            }
            if ($sortBy === 'cityThenName') {
                $orderings = ['city' => QueryInterface::ORDER_DESCENDING, 'name' => QueryInterface::ORDER_DESCENDING];
            }
            if ($sortBy === 'lastnameThenFirstname') {
                $orderings = ['lastName' => QueryInterface::ORDER_DESCENDING, 'firstName' => QueryInterface::ORDER_DESCENDING];
            }
            $query->setOrderings($orderings);
        }
        // Use the offset, if available
        if ($offset > 0) {
            $query->setOffset($offset);
        }
        // Use the limit, if available
        if ($limit > 0) {
            $query->setLimit($limit);
        }
        return $query->execute();
    }

    /**
     * @param array $filter
     * @return int
     * @param int $storagePid Storage pid - if empty storage will be ignored
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function countAllByFilter(array $filter = [], int $storagePid = 0)
    {
        $query = $this->createQuery();
        /** By using the json API, the record storage isn't available! */
        $query->getQuerySettings()->setRespectStoragePage(false);
        $constraints = $this->getConstraints($query, $filter, $storagePid);
        if (!empty($constraints)) {
            if (count($constraints) > 1) {
                $query->matching(
                    $query->logicalAnd($constraints)
                );
            } else {
                $query->matching($constraints[0]);
            }
        }
        return $query->execute()->count();
    }

    /**
     * @param $query QueryInterface
     * @param $filter array
     * @param int $storagePid Storage pid - if empty storage will be ignored
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function getConstraints($query, $filter, int $storagePid = 0)
    {
        $constraints = [];
        if (!empty($filter)) {
            foreach ($filter as $filterKey => $filterItem) {
                if ($filterItem instanceof AddressGroup) {
                    $constraints[] = $query->contains('groups', $filterItem);
                }
                if ($filterItem instanceof AddressPosition) {
                    $constraints[] = $query->contains('positions', $filterItem);
                }
                if ($filterItem instanceof AddressOrganisation) {
                    $constraints[] = $query->contains('organisations', $filterItem);
                }
                if (is_string($filterItem)) {
                    $constraints[] = $query->like('name', '%' . $filterItem . '%');
                }
                if (is_array($filterItem)) {
                    // Search for a custom string
                    if (isset($filterItem['customString'])) {
                        if (trim($filterItem['customString']['value']) !== '') {
                            $searchFields = GeneralUtility::trimExplode(',', $filterItem['customString']['field']);
                            $subConstraints = [];
                            foreach ($searchFields as $searchField) {
                                $subConstraints[] = $query->like($searchField, '%' . $filterItem['customString']['value'] . '%');
                            }
                            $constraints[] = $query->logicalOr($subConstraints);
                            unset($subConstraints);
                        }
                    } else {
                        // Items found
                        if (count($filterItem) > 0) {
                            $constraints[] = $query->in('uid', $filterItem);
                        } else {
                            // Otherwise there can't be a match!
                            $constraints[] = $query->equals('uid', -99);
                        }
                    }
                }
            }
        }
        if($storagePid === 0) {
            $constraints[] = $query->greaterThan('pid', $storagePid);
        }
        else {
            $constraints[] = $query->equals('pid', $storagePid);
        }
        return $constraints;
    }

    /**
     * Finds all address uids, which are in distance to the giving geo coordinates
     *
     * @param $arguments
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByLocation($arguments)
    {
        $languageAspect = GeneralUtility::makeInstance(Context::class)->getAspect('language');
        //
        // Validate geo coordinates
        $latitude = 0.0;
        if (isset($arguments['latitude'])) {
            $latitude = (float)$arguments['latitude'];
        }
        $longitude = 0.0;
        if (isset($arguments['longitude'])) {
            $longitude = (float)$arguments['longitude'];
        }
        //
        // Search distance
        $distance = isset($arguments['distance']) ? (int)$arguments['distance'] : 50;
        //
        // To search by kilometers instead of miles, replace 3959 with 6371.
        $distanceUnit = 6371;
        if ((bool)$arguments['inMiles']) {
            $distanceUnit = 3959;
        }
        //
        /** @var ConnectionPool $connectionPool */
        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = $connectionPool->getQueryBuilderForTable('tx_addressmanager_domain_model_address');
        $queryBuilder->selectLiteral(
            '*',
            '(' . $distanceUnit . ' * acos
                    (
                        cos(
                            radians(' . $latitude . ')
                        )
                        * cos(
                            radians(map_latitude)
                        ) * cos(
                            radians(map_longitude) - radians(' . $longitude . ')
                        ) + sin(
                            radians(' . $latitude . ')
                        ) * sin(
                            radians(map_latitude)
                        )
                    )
                ) AS distance'
        )
            ->from('tx_addressmanager_domain_model_address')
            ->orWhere(
                $queryBuilder->expr()->eq(
                    'sys_language_uid',
                    $queryBuilder->createNamedParameter($languageAspect->getId(), \PDO::PARAM_INT)
                ),
                $queryBuilder->expr()->eq(
                    'sys_language_uid',
                    $queryBuilder->createNamedParameter(-1, \PDO::PARAM_INT)
                )
            )
            ->having(
                $queryBuilder->expr()->lte(
                    'distance',
                    $queryBuilder->createNamedParameter($distance, \PDO::PARAM_INT)
                )
            )
            ->orderBy('distance', 'ASC');
        return $this->getDataMapper()->map(Address::class, $queryBuilder->execute()->fetchAll());
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $groups
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $positions
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $organisations
     * @param string $sortBy
     * @param string $order
     * @param int $offset
     * @param int $limit
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function findAllForInit(
        QueryResult $groups,
        QueryResult $positions,
        QueryResult $organisations,
        $sortBy = 'name',
        $order = 'ASC',
        $offset = 0,
        $limit = 0
    )
    {
        $query = $this->createQuery();
        $constraints = [];
        if ($groups->count() > 0) {
            $constraintsGroups = [];
            foreach ($groups as $group) {
                $constraintsGroups[] = $query->contains('groups', $group);
            }
            $constraints[] = $query->logicalOr($constraintsGroups);
        }
        if ($positions->count() > 0) {
            $constraintsPositions = [];
            foreach ($positions as $position) {
                $constraintsPositions[] = $query->contains('positions', $position);
            }
            $constraints[] = $query->logicalOr($constraintsPositions);
        }
        if ($organisations->count() > 0) {
            $constraintsOrganisations = [];
            foreach ($organisations as $organisation) {
                $constraintsOrganisations[] = $query->contains('organisations', $organisation);
            }
            $constraints[] = $query->logicalOr($constraintsOrganisations);
        }
        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd($constraints)
            );
        }
        // Sort by start date
        if ($order == 'ASC') {
            $orderings = [$sortBy => QueryInterface::ORDER_ASCENDING];
            if ($sortBy === 'genderThenName') {
                $orderings = ['gender' => QueryInterface::ORDER_ASCENDING, 'name' => QueryInterface::ORDER_ASCENDING];
            }
            if ($sortBy === 'cityThenName') {
                $orderings = ['city' => QueryInterface::ORDER_ASCENDING, 'name' => QueryInterface::ORDER_ASCENDING];
            }
            if ($sortBy === 'lastnameThenFirstname') {
                $orderings = ['lastName' => QueryInterface::ORDER_ASCENDING, 'firstName' => QueryInterface::ORDER_ASCENDING];
            }
            $query->setOrderings($orderings);
        } else {
            $orderings = [$sortBy => QueryInterface::ORDER_DESCENDING];
            if ($sortBy === 'genderThenName') {
                $orderings = ['gender' => QueryInterface::ORDER_DESCENDING, 'name' => QueryInterface::ORDER_DESCENDING];
            }
            if ($sortBy === 'cityThenName') {
                $orderings = ['city' => QueryInterface::ORDER_DESCENDING, 'name' => QueryInterface::ORDER_DESCENDING];
            }
            if ($sortBy === 'lastnameThenFirstname') {
                $orderings = ['lastName' => QueryInterface::ORDER_DESCENDING, 'firstName' => QueryInterface::ORDER_DESCENDING];
            }
            $query->setOrderings($orderings);
        }
        // Use the offset, if available
        if ($offset > 0) {
            $query->setOffset($offset);
        }
        // Use the limit, if available
        if ($limit > 0) {
            $query->setLimit($limit);
        }
        return $query->execute();
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $groups
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $positions
     * @param \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult $organisations
     * @return int
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException
     */
    public function countAllForInit(QueryResult $groups, QueryResult $positions, QueryResult $organisations)
    {
        $query = $this->createQuery();
        $constraints = [];
        if ($groups->count() > 0) {
            $constraintsGroups = [];
            foreach ($groups as $group) {
                $constraintsGroups[] = $query->contains('groups', $group);
            }
            $constraints[] = $query->logicalOr($constraintsGroups);
        }
        if ($positions->count() > 0) {
            $constraintsPositions = [];
            foreach ($positions as $position) {
                $constraintsPositions[] = $query->contains('positions', $position);
            }
            $constraints[] = $query->logicalOr($constraintsPositions);
        }
        if ($organisations->count() > 0) {
            $constraintsOrganisations = [];
            foreach ($organisations as $organisation) {
                $constraintsOrganisations[] = $query->contains('organisations', $organisation);
            }
            $constraints[] = $query->logicalOr($constraintsOrganisations);
        }
        if (!empty($constraints)) {
            $query->matching(
                $query->logicalAnd($constraints)
            );
        }
        return $query->execute()->count();
    }

    /**
     * @param string $name
     * @param bool $respectStaoragePage
     * @return object
     */
    public function findOneByName($name, $respectStoragePage = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage($respectStoragePage);
        $query->matching($query->equals('name', $name));
        return $query->execute()->getFirst();
    }

    /**
     * @param int $uid
     * @param bool $respectStaoragePage
     * @return object
     */
    public function findOneByUid($uid, $respectStoragePage = true)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setRespectStoragePage($respectStoragePage);
        $query->matching($query->equals('uid', $uid));
        return $query->execute()->getFirst();
    }

    /**
     * @param array $filter
     * @param boolean $count
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface|int
     */
    public function findAllForBackendList(array $filter = array(), $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        if($filter['searchWord'] !== '') {
            $constraints = array();
            $constraints[] = $query->like('name', '%' . $filter['searchWord'] . '%');
            $constraints[] = $query->like('email', '%' . $filter['searchWord'] . '%');
            $query->matching($query->logicalOr($constraints));
        }
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] != '') {
                if ($filter['sortingOrder'] == 'asc') {
                    $query->setOrderings(array($filter['sortingField'] => QueryInterface::ORDER_ASCENDING));
                } else {
                    if ($filter['sortingOrder'] == 'desc') {
                        $query->setOrderings(array($filter['sortingField'] => QueryInterface::ORDER_DESCENDING));
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        } else {
            return $query->execute()->count();
        }
    }

    /**
     * @param array $filter
     * @param boolean $count
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface|int
     */
    public function findAllForFrontendList(array $filter = [], $count = false)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        $constraints = [];
        $constraints[] = $query->equals('frontendUser', $filter['frontendUser']->getUid());
        $query->matching($constraints[0]);
        if (!$count) {
            if (isset($filter['sortingField']) && $filter['sortingField'] !== '') {
                if ($filter['sortingOrder'] === 'asc') {
                    $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_ASCENDING]);
                } else {
                    if ($filter['sortingOrder'] === 'desc') {
                        $query->setOrderings([$filter['sortingField'] => QueryInterface::ORDER_DESCENDING]);
                    }
                }
            }
            if ((int)$filter['limit'] > 0) {
                $query->setOffset((int)$filter['offset']);
                $query->setLimit((int)$filter['limit']);
            }
            return $query->execute();
        } else {
            return $query->execute()->count();
        }
    }

    /**
     * @param int $uid
     * @return object
     */
    public function findByIdentifierFrontend(int $uid, int $frontendUser)
    {
        $query = $this->createQuery();
        $query->getQuerySettings()->setIgnoreEnableFields(true);
        $query->getQuerySettings()->setRespectStoragePage(true);
        $constraints = [];
        $constraints[] = $query->equals('uid', $uid);
        $constraints[] = $query->equals('frontendUser', $frontendUser);
        $query->matching($query->logicalAnd($constraints));
        return $query->execute()->getFirst();
    }

}
