<?php

namespace CodingMs\AddressManager\Domain\Model;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use DateTime;

/**
 * Address
 */
class Address extends AbstractEntity
{

    /**
     * @var \DateTime
     */
    protected $creationDate;

    /**
     * @var \DateTime
     */
    protected $modificationDate;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $slug;

    /**
     * @var bool
     */
    protected $hidden = true;

    /**
     * @var bool
     */
    protected $deleted = true;

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    protected $frontendUser = null;

    /**
     * @var string
     */
    protected $gender = '';

    /**
     * @var string
     */
    protected $firstName = '';

    /**
     * @var string
     */
    protected $middleName = '';

    /**
     * @var string
     */
    protected $lastName = '';

    /**
     * @var \DateTime
     */
    protected $birthday = null;

    /**
     * @var string
     */
    protected $title = '';

    /**
     * @var string
     */
    protected $email = '';

    /**
     * @var string
     */
    protected $phone = '';

    /**
     * @var string
     */
    protected $mobile = '';

    /**
     * @var string
     */
    protected $www = '';

    /**
     * @var string
     */
    protected $address = '';

    /**
     * @var string
     */
    protected $building = '';

    /**
     * @var string
     */
    protected $room = '';

    /**
     * @var string
     */
    protected $company = '';

    /**
     * @var string
     */
    protected $city = '';

    /**
     * @var string
     */
    protected $postalCode = '';

    /**
     * @var string
     */
    protected $region = '';

    /**
     * @var string
     */
    protected $country = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $images;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $files;

    /**
     * @var string
     */
    protected $fax = '';

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var string
     */
    protected $mapLatitude = '';

    /**
     * @var string
     */
    protected $mapLongitude = '';

    /**
     * @var integer
     */
    protected $mapZoom = 0;

    /**
     * @var string
     */
    protected $mapTooltip = '';

    /**
     * @var string
     */
    protected $mapLink = '';

    /**
     * @var string
     */
    protected $mapMarker = 'default';

    /**
     * @var float
     */
    protected $distance = '';

    /**
     * @var string
     */
    protected $directions = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressGroup>
     */
    protected $groups = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressPosition>
     */
    protected $positions = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressOrganisation>
     */
    protected $organisations = null;

    /**
     * Custom attribute 1
     * @var string
     */
    protected $attribute1;

    /**
     * Custom attribute 2
     * @var string
     */
    protected $attribute2;

    /**
     * Custom attribute 3
     * @var string
     */
    protected $attribute3;

    /**
     * Custom attribute 4
     * @var string
     */
    protected $attribute4;

    /**
     * Custom attribute 5
     * @var string
     */
    protected $attribute5;

    /**
     * Custom attribute 6
     * @var string
     */
    protected $attribute6;

    /**
     * Custom attribute 7
     * @var string
     */
    protected $attribute7;

    /**
     * Custom attribute 8
     * @var string
     */
    protected $attribute8;

    /**
     * Custom attribute 9
     * @var string
     */
    protected $attribute9;

    /**
     * Custom attribute 10
     * @var string
     */
    protected $attribute10;

    /**
     * Custom attribute 11
     * @var string
     */
    protected $attribute11;

    /**
     * Custom attribute 12
     * @var string
     */
    protected $attribute12;

    /**
     * Custom attribute 13
     * @var string
     */
    protected $attribute13;

    /**
     * Custom attribute 14
     * @var string
     */
    protected $attribute14;

    /**
     * Custom attribute 15
     * @var string
     */
    protected $attribute15;

    /**
     * Custom attribute 16
     * @var string
     */
    protected $attribute16;

    /**
     * Custom attribute 17
     * @var string
     */
    protected $attribute17;

    /**
     * @var string
     */
    protected $htmlTitle;

    /**
     * @var string
     */
    protected $metaAbstract;

    /**
     * @var string
     */
    protected $metaDescription;

    /**
     * @var string
     */
    protected $metaKeywords;

    /**
     * @var string
     */
    protected $facebook = '';

    /**
     * @var string
     */
    protected $instagram = '';

    /**
     * @var string
     */
    protected $twitter = '';

    /**
     * @var string
     */
    protected $youtube = '';

    /**
     * @var string
     */
    protected $openingHoursMondayOpens = '';

    /**
     * @var string
     */
    protected $openingHoursMondayCloses = '';

    /**
     * @var string
     */
    protected $openingHoursTuesdayOpens = '';

    /**
     * @var string
     */
    protected $openingHoursTuesdayCloses = '';

    /**
     * @var string
     */
    protected $openingHoursWednesdayOpens = '';

    /**
     * @var string
     */
    protected $openingHoursWednesdayCloses = '';

    /**
     * @var string
     */
    protected $openingHoursThursdayOpens = '';

    /**
     * @var string
     */
    protected $openingHoursThursdayCloses = '';

    /**
     * @var string
     */
    protected $openingHoursFridayOpens = '';

    /**
     * @var string
     */
    protected $openingHoursFridayCloses = '';

    /**
     * @var string
     */
    protected $openingHoursSaturdayOpens = '';

    /**
     * @var string
     */
    protected $openingHoursSaturdayCloses = '';

    /**
     * @var string
     */
    protected $openingHoursSundayOpens = '';

    /**
     * @var string
     */
    protected $openingHoursSundayCloses = '';

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\Address>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Lazy
     */
    protected $addresses = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }


    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->groups = new ObjectStorage();
        $this->positions = new ObjectStorage();
        $this->organisations = new ObjectStorage();
        $this->addresses = new ObjectStorage();
    }


    /**
     * @return \DateTime $creationDate
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param \DateTime $creationDate
     * @return void
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
    }

    /**
     * @return \DateTime $modificationDate
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }

    /**
     * @param \DateTime $modificationDate
     * @return void
     */
    public function setModificationDate($modificationDate)
    {
        $this->modificationDate = $modificationDate;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return bool
     */
    public function getHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     */
    public function setHidden(bool $hidden): void
    {
        $this->hidden = $hidden;
    }

    /**
     * @return bool
     */
    public function getDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @return bool
     */
    public function isDeleted(): bool
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted(bool $deleted): void
    {
        $this->deleted = $deleted;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
     */
    public function getFrontendUser(): ?\TYPO3\CMS\Extbase\Domain\Model\FrontendUser
    {
        return $this->frontendUser;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $frontendUser
     */
    public function setFrontendUser(\TYPO3\CMS\Extbase\Domain\Model\FrontendUser $frontendUser): void
    {
        $this->frontendUser = $frontendUser;
    }

    /**
     * Returns the name
     *
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     *
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the gender
     *
     * @return string $gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Sets the gender
     *
     * @param string $gender
     * @return void
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * Returns the firstName
     *
     * @return string $firstName
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Sets the firstName
     *
     * @param string $firstName
     * @return void
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * Returns the middleName
     *
     * @return string $middleName
     */
    public function getMiddleName()
    {
        return $this->middleName;
    }

    /**
     * Sets the middleName
     *
     * @param string $middleName
     * @return void
     */
    public function setMiddleName($middleName)
    {
        $this->middleName = $middleName;
    }

    /**
     * Returns the lastName
     *
     * @return string $lastName
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Sets the lastName
     *
     * @param string $lastName
     * @return void
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * Returns the birthday
     *
     * @return \DateTime $birthday
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Sets the birthday
     *
     * @param \DateTime $birthday
     * @return void
     */
    public function setBirthday(\DateTime $birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Returns the mobile
     *
     * @return string $mobile
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * Sets the mobile
     *
     * @param string $mobile
     * @return void
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * Returns the www
     *
     * @return string $www
     */
    public function getWww()
    {
        return $this->www;
    }

    /**
     * Sets the www
     *
     * @param string $www
     * @return void
     */
    public function setWww($www)
    {
        $this->www = $www;
    }

    /**
     * Returns the address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the address
     *
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Returns the building
     *
     * @return string $building
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Sets the building
     *
     * @param string $building
     * @return void
     */
    public function setBuilding($building)
    {
        $this->building = $building;
    }

    /**
     * Returns the room
     *
     * @return string $room
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Sets the room
     *
     * @param string $room
     * @return void
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * Returns the company
     *
     * @return string $company
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Sets the company
     *
     * @param string $company
     * @return void
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the postalCode
     *
     * @return string $postalCode
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Sets the postalCode
     *
     * @param string $postalCode
     * @return void
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;
    }

    /**
     * Returns the region
     *
     * @return string $region
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Sets the region
     *
     * @param string $region
     * @return void
     */
    public function setRegion($region)
    {
        $this->region = $region;
    }

    /**
     * Returns the country
     *
     * @return string $country
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Sets the country
     *
     * @param string $country
     * @return void
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * Adds a image
     *
     * @param \CodingMs\AddressManager\Domain\Model\FileReference $image
     * @return void
     */
    public function addImage(\CodingMs\AddressManager\Domain\Model\FileReference $image)
    {
        $this->images->attach($image);
    }

    /**
     * Removes a image
     *
     * @param \CodingMs\AddressManager\Domain\Model\FileReference $imageToRemove The image to be removed
     * @return void
     */
    public function removeImage(\CodingMs\AddressManager\Domain\Model\FileReference $imageToRemove)
    {
        $this->images->detach($imageToRemove);
    }

    /**
     * Returns the images
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\FileReference> $images
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Sets the images
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\FileReference> $images
     * @return void
     */
    public function setImages(ObjectStorage $images)
    {
        $this->images = $images;
    }

    /**
     * Returns all images marked as preview
     * @return array $images
     */
    public function getImagesPreviewOnly()
    {
        $images = [];
        if ($this->getImages()) {
            /** @var \CodingMs\AddressManager\Domain\Model\FileReference $mediaItem */
            foreach ($this->getImages() as $mediaItem) {
                if ($mediaItem->getOriginalResource()->getProperty('preview')) {
                    $images[] = $mediaItem;
                }
            }
        }
        return $images;
    }

    /**
     * Adds a file
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $file
     * @return void
     */
    public function addFile(\TYPO3\CMS\Extbase\Domain\Model\FileReference $file)
    {
        $this->files->attach($file);
    }

    /**
     * Removes a file
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $fileToRemove The file to be removed
     * @return void
     */
    public function removeFile(\TYPO3\CMS\Extbase\Domain\Model\FileReference $fileToRemove)
    {
        $this->files->detach($fileToRemove);
    }

    /**
     * Returns the files
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $files
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Sets the files
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\TYPO3\CMS\Extbase\Domain\Model\FileReference> $files
     * @return void
     */
    public function setFiles(ObjectStorage $files)
    {
        $this->files = $files;
    }

    /**
     * Returns the fax
     *
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax
     *
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * Returns the description
     *
     * @return string $description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the description
     *
     * @param string $description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Returns the mapLatitude
     *
     * @return string $mapLatitude
     */
    public function getMapLatitude()
    {
        $latitude = $this->mapLatitude;
        if ($latitude > 85 || $latitude < -85) {
            $latitude = 0.0;
        }
        return $latitude;
    }

    /**
     * Sets the mapLatitude
     *
     * @param string $latitude
     * @return void
     */
    public function setMapLatitude($latitude)
    {
        if ($latitude > 85 || $latitude < -85) {
            $latitude = 0.0;
        }
        $this->mapLatitude = $latitude;
    }

    /**
     * Returns the mapLongitude
     *
     * @return string $mapLongitude
     */
    public function getMapLongitude()
    {
        $longitude = $this->mapLongitude;
        if ($longitude > 180 || $longitude < -180) {
            $longitude = 0.0;
        }
        return $longitude;
    }

    /**
     * Sets the mapLongitude
     *
     * @param string $mapLongitude
     * @return void
     */
    public function setMapLongitude($longitude)
    {
        if ($longitude > 180 || $longitude < -180) {
            $longitude = 0.0;
        }
        $this->mapLongitude = $longitude;
    }

    /**
     * Returns the mapZoom
     *
     * @return integer $mapZoom
     */
    public function getMapZoom()
    {
        return $this->mapZoom;
    }

    /**
     * Sets the mapZoom
     *
     * @param integer $mapZoom
     * @return void
     */
    public function setMapZoom($mapZoom)
    {
        $this->mapZoom = $mapZoom;
    }

    /**
     * Returns the mapTooltip
     *
     * @return string $mapTooltip
     */
    public function getMapTooltip()
    {
        return $this->mapTooltip;
    }

    /**
     * Sets the mapTooltip
     *
     * @param string $mapTooltip
     * @return void
     */
    public function setMapTooltip($mapTooltip)
    {
        $this->mapTooltip = $mapTooltip;
    }

    /**
     * Returns the mapLink
     *
     * @return string $mapLink
     */
    public function getMapLink()
    {
        return $this->mapLink;
    }

    /**
     * Sets the mapLink
     *
     * @param string $mapLink
     * @return void
     */
    public function setMapLink($mapLink)
    {
        $this->mapLink = $mapLink;
    }

    /**
     * @return string
     */
    public function getMapMarker()
    {
        if(trim($this->mapMarker) === '') {
            $this->mapMarker = 'default';
        }
        return $this->mapMarker;
    }

    /**
     * @param string $mapMarker
     */
    public function setMapMarker($mapMarker)
    {
        $this->mapMarker = $mapMarker;
    }

    /**
     * @return float
     */
    public function getDistance()
    {
        // Attention:
        // Ensure that distance is from type float!
        return (float)$this->distance;
    }

    /**
     * @param float $distance
     */
    public function setDistance($distance)
    {
        $this->distance = $distance;
    }

    /**
     * Returns the directions
     *
     * @return string $directions
     */
    public function getDirections()
    {
        return $this->directions;
    }

    /**
     * Sets the directions
     *
     * @param string $directions
     * @return void
     */
    public function setDirections($directions)
    {
        $this->directions = $directions;
    }

    /**
     * Adds a AddressGroup
     *
     * @param \CodingMs\AddressManager\Domain\Model\AddressGroup $group
     * @return void
     */
    public function addGroup(AddressGroup $group)
    {
        $this->groups->attach($group);
    }

    /**
     * Removes a AddressGroup
     *
     * @param \CodingMs\AddressManager\Domain\Model\AddressGroup $groupToRemove The AddressGroup to be removed
     * @return void
     */
    public function removeGroup(AddressGroup $groupToRemove)
    {
        $this->groups->detach($groupToRemove);
    }

    /**
     * Returns the groups
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressGroup> $groups
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Sets the groups
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressGroup> $groups
     * @return void
     */
    public function setGroups(ObjectStorage $groups)
    {
        $this->groups = $groups;
    }

    /**
     * @return string
     */
    public function getGroupsString()
    {
        $groups = [];
        /** @var \CodingMs\AddressManager\Domain\Model\AddressGroup $group */
        foreach ($this->getGroups() as $group) {
            $groups[] = $group->getTitle();
        }
        return implode(', ', $groups);
    }

    /**
     * Adds a AddressPosition
     *
     * @param \CodingMs\AddressManager\Domain\Model\AddressPosition $position
     * @return void
     */
    public function addPosition(AddressPosition $position)
    {
        $this->positions->attach($position);
    }

    /**
     * Removes a AddressPosition
     *
     * @param \CodingMs\AddressManager\Domain\Model\AddressPosition $positionToRemove The AddressPosition to be removed
     * @return void
     */
    public function removePosition(AddressPosition $positionToRemove)
    {
        $this->positions->detach($positionToRemove);
    }

    /**
     * Returns the positions
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressPosition> $positions
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * Sets the positions
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressPosition> $positions
     * @return void
     */
    public function setPositions(ObjectStorage $positions)
    {
        $this->positions = $positions;
    }

    /**
     * @return string
     */
    public function getPositionsString()
    {
        $positions = [];
        /** @var \CodingMs\AddressManager\Domain\Model\AddressPosition $position */
        foreach ($this->getPositions() as $position) {
            $positions[] = $position->getTitle();
        }
        return implode(', ', $positions);
    }

    /**
     * Adds a AddressOrganisation
     *
     * @param \CodingMs\AddressManager\Domain\Model\AddressOrganisation $organisation
     * @return void
     */
    public function addOrganisation(AddressOrganisation $organisation)
    {
        $this->organisations->attach($organisation);
    }

    /**
     * Removes a AddressOrganisation
     *
     * @param \CodingMs\AddressManager\Domain\Model\AddressOrganisation $organisationToRemove The AddressOrganisation to be removed
     * @return void
     */
    public function removeOrganisation(AddressOrganisation $organisationToRemove)
    {
        $this->organisations->detach($organisationToRemove);
    }

    /**
     * Returns the organisations
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressOrganisation> $organisations
     */
    public function getOrganisations()
    {
        return $this->organisations;
    }

    /**
     * Sets the organisations
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\AddressOrganisation> $organisations
     * @return void
     */
    public function setOrganisations(ObjectStorage $organisations)
    {
        $this->organisations = $organisations;
    }

    /**
     * @return string
     */
    public function getOrganisationsString()
    {
        $organisations = [];
        /** @var \CodingMs\AddressManager\Domain\Model\AddressOrganisation $organisation */
        foreach ($this->getOrganisations() as $organisation) {
            $organisations[] = $organisation->getTitle();
        }
        return implode(', ', $organisations);
    }

    /**
     * @param bool $withoutdescription
     * @return array
     */
    public function toArray($withoutDescription=false)
    {
        $array = [];
        $array['pid'] = $this->getPid();
        $array['uid'] = $this->getUid();
        $array['name'] = $this->getName();
        if(!$withoutDescription) {
            $array['description'] = $this->getDescription();
        }
        //
        $array['groups'] = [];
        $groups = $this->getGroups();
        if (count($groups) > 0) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressGroup $group */
            foreach ($groups as $group) {
                $array['groups'][] = $group->toArray($withoutDescription);
            }
        }
        //
        $array['positions'] = [];
        $positions = $this->getPositions();
        if (count($positions) > 0) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressPosition $position */
            foreach ($positions as $position) {
                $array['positions'][] = $position->toArray($withoutDescription);
            }
        }
        //
        $array['organisations'] = [];
        $organisations = $this->getOrganisations();
        if (count($organisations) > 0) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressOrganisation $organisation */
            foreach ($organisations as $organisation) {
                $array['organisations'][] = $organisation->toArray($withoutDescription);
            }
        }
        return $array;
    }

    /**
     * Returns the categories as a String
     *
     * @return string
     */
    public function getCssClasses()
    {
        $classesArray = [];
        // Get all groups
        $groups = $this->getGroups();
        if (!empty($groups)) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressGroup $group */
            foreach ($groups as $group) {
                $classesArray[] = 'address-group-' . $group->getUid();
            }
        }
        // Get all positions
        $positions = $this->getPositions();
        if (!empty($positions)) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressPosition $position */
            foreach ($positions as $position) {
                $classesArray[] = 'address-position-' . $position->getUid();
            }
        }
        // Get all organisations
        $organisations = $this->getOrganisations();
        if (!empty($organisations)) {
            /** @var \CodingMs\AddressManager\Domain\Model\AddressOrganisation $organisation */
            foreach ($organisations as $organisation) {
                $classesArray[] = 'address-organisation-' . $organisation->getUid();
            }
        }
        return implode(' ', $classesArray);
    }

    /**
     * @return string
     */
    public function getAttribute1(): ?string
    {
        return $this->attribute1;
    }

    /**
     * @param string $attribute1
     */
    public function setAttribute1(string $attribute1): void
    {
        $this->attribute1 = $attribute1;
    }

    /**
     * @return string
     */
    public function getAttribute2(): ?string
    {
        return $this->attribute2;
    }

    /**
     * @param string $attribute2
     */
    public function setAttribute2(string $attribute2): void
    {
        $this->attribute2 = $attribute2;
    }

    /**
     * @return string
     */
    public function getAttribute3(): ?string
    {
        return $this->attribute3;
    }

    /**
     * @param string $attribute3
     */
    public function setAttribute3(string $attribute3): void
    {
        $this->attribute3 = $attribute3;
    }

    /**
     * @return string
     */
    public function getAttribute4(): ?string
    {
        return $this->attribute4;
    }

    /**
     * @param string $attribute4
     */
    public function setAttribute4(string $attribute4): void
    {
        $this->attribute4 = $attribute4;
    }

    /**
     * @return string
     */
    public function getAttribute5(): ?string
    {
        return $this->attribute5;
    }

    /**
     * @param string $attribute5
     */
    public function setAttribute5(string $attribute5): void
    {
        $this->attribute5 = $attribute5;
    }

    /**
     * @return string
     */
    public function getAttribute6(): ?string
    {
        return $this->attribute6;
    }

    /**
     * @param string $attribute6
     */
    public function setAttribute6(string $attribute6): void
    {
        $this->attribute6 = $attribute6;
    }

    /**
     * @return string
     */
    public function getAttribute7(): ?string
    {
        return $this->attribute7;
    }

    /**
     * @param string $attribute7
     */
    public function setAttribute7(string $attribute7): void
    {
        $this->attribute7 = $attribute7;
    }

    /**
     * @return string
     */
    public function getAttribute8(): ?string
    {
        return $this->attribute8;
    }

    /**
     * @param string $attribute8
     */
    public function setAttribute8(string $attribute8): void
    {
        $this->attribute8 = $attribute8;
    }

    /**
     * @return string
     */
    public function getAttribute9(): ?string
    {
        return $this->attribute9;
    }

    /**
     * @param string $attribute9
     */
    public function setAttribute9(string $attribute9): void
    {
        $this->attribute9 = $attribute9;
    }

    /**
     * @return string
     */
    public function getAttribute10(): ?string
    {
        return $this->attribute10;
    }

    /**
     * @param string $attribute10
     */
    public function setAttribute10(string $attribute10): void
    {
        $this->attribute10 = $attribute10;
    }

    /**
     * @return string
     */
    public function getAttribute11(): ?string
    {
        return $this->attribute11;
    }

    /**
     * @param string $attribute11
     */
    public function setAttribute11(string $attribute11): void
    {
        $this->attribute11 = $attribute11;
    }

    /**
     * @return string
     */
    public function getAttribute12(): ?string
    {
        return $this->attribute12;
    }

    /**
     * @param string $attribute12
     */
    public function setAttribute12(string $attribute12): void
    {
        $this->attribute12 = $attribute12;
    }

    /**
     * @return string
     */
    public function getAttribute13(): ?string
    {
        return $this->attribute13;
    }

    /**
     * @param string $attribute13
     */
    public function setAttribute13(string $attribute13): void
    {
        $this->attribute13 = $attribute13;
    }

    /**
     * @return string
     */
    public function getAttribute14(): ?string
    {
        return $this->attribute14;
    }

    /**
     * @param string $attribute14
     */
    public function setAttribute14(string $attribute14): void
    {
        $this->attribute14 = $attribute14;
    }

    /**
     * @return string
     */
    public function getAttribute15(): ?string
    {
        return $this->attribute15;
    }

    /**
     * @param string $attribute15
     */
    public function setAttribute15(string $attribute15): void
    {
        $this->attribute15 = $attribute15;
    }

    /**
     * @return string
     */
    public function getAttribute16(): ?string
    {
        return $this->attribute16;
    }

    /**
     * @param string $attribute16
     */
    public function setAttribute16(string $attribute16): void
    {
        $this->attribute16 = $attribute16;
    }

    /**
     * @return string
     */
    public function getAttribute17(): ?string
    {
        return $this->attribute17;
    }

    /**
     * @param string $attribute17
     */
    public function setAttribute17(string $attribute17): void
    {
        $this->attribute17 = $attribute17;
    }

    /**
     * @return string
     */
    public function getHtmlTitle(): ?string
    {
        return $this->htmlTitle;
    }

    /**
     * @param string $htmlTitle
     */
    public function setHtmlTitle(string $htmlTitle): void
    {
        $this->htmlTitle = $htmlTitle;
    }

    /**
     * @return string
     */
    public function getMetaAbstract(): ?string
    {
        return $this->metaAbstract;
    }

    /**
     * @param string $metaAbstract
     */
    public function setMetaAbstract(string $metaAbstract): void
    {
        $this->metaAbstract = $metaAbstract;
    }

    /**
     * @return string
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription(string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string
     */
    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaKeywords
     */
    public function setMetaKeywords(string $metaKeywords): void
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @return string
     */
    public function getFacebook(): string
    {
        return $this->facebook;
    }

    /**
     * @param string $facebook
     */
    public function setFacebook(string $facebook): void
    {
        $this->facebook = $facebook;
    }

    /**
     * @return string
     */
    public function getInstagram(): string
    {
        return $this->instagram;
    }

    /**
     * @param string $instagram
     */
    public function setInstagram(string $instagram): void
    {
        $this->instagram = $instagram;
    }

    /**
     * @return string
     */
    public function getTwitter(): string
    {
        return $this->twitter;
    }

    /**
     * @param string $twitter
     */
    public function setTwitter(string $twitter): void
    {
        $this->twitter = $twitter;
    }

    /**
     * @return string
     */
    public function getYoutube(): string
    {
        return $this->youtube;
    }

    /**
     * @param string $youtube
     */
    public function setYoutube(string $youtube): void
    {
        $this->youtube = $youtube;
    }

    /**
     * @return string
     */
    public function getOpeningHoursMondayOpens(): string
    {
        return $this->openingHoursMondayOpens;
    }

    /**
     * @param string $openingHoursMondayOpens
     */
    public function setOpeningHoursMondayOpens(string $openingHoursMondayOpens): void
    {
        $this->openingHoursMondayOpens = $openingHoursMondayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursMondayCloses(): string
    {
        return $this->openingHoursMondayCloses;
    }

    /**
     * @param string $openingHoursMondayCloses
     */
    public function setOpeningHoursMondayCloses(string $openingHoursMondayCloses): void
    {
        $this->openingHoursMondayCloses = $openingHoursMondayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursTuesdayOpens(): string
    {
        return $this->openingHoursTuesdayOpens;
    }

    /**
     * @param string $openingHoursTuesdayOpens
     */
    public function setOpeningHoursTuesdayOpens(string $openingHoursTuesdayOpens): void
    {
        $this->openingHoursTuesdayOpens = $openingHoursTuesdayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursTuesdayCloses(): string
    {
        return $this->openingHoursTuesdayCloses;
    }

    /**
     * @param string $openingHoursTuesdayCloses
     */
    public function setOpeningHoursTuesdayCloses(string $openingHoursTuesdayCloses): void
    {
        $this->openingHoursTuesdayCloses = $openingHoursTuesdayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursWednesdayOpens(): string
    {
        return $this->openingHoursWednesdayOpens;
    }

    /**
     * @param string $openingHoursWednesdayOpens
     */
    public function setOpeningHoursWednesdayOpens(string $openingHoursWednesdayOpens): void
    {
        $this->openingHoursWednesdayOpens = $openingHoursWednesdayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursWednesdayCloses(): string
    {
        return $this->openingHoursWednesdayCloses;
    }

    /**
     * @param string $openingHoursWednesdayCloses
     */
    public function setOpeningHoursWednesdayCloses(string $openingHoursWednesdayCloses): void
    {
        $this->openingHoursWednesdayCloses = $openingHoursWednesdayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursThursdayOpens(): string
    {
        return $this->openingHoursThursdayOpens;
    }

    /**
     * @param string $openingHoursThursdayOpens
     */
    public function setOpeningHoursThursdayOpens(string $openingHoursThursdayOpens): void
    {
        $this->openingHoursThursdayOpens = $openingHoursThursdayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursThursdayCloses(): string
    {
        return $this->openingHoursThursdayCloses;
    }

    /**
     * @param string $openingHoursThursdayCloses
     */
    public function setOpeningHoursThursdayCloses(string $openingHoursThursdayCloses): void
    {
        $this->openingHoursThursdayCloses = $openingHoursThursdayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursFridayOpens(): string
    {
        return $this->openingHoursFridayOpens;
    }

    /**
     * @param string $openingHoursFridayOpens
     */
    public function setOpeningHoursFridayOpens(string $openingHoursFridayOpens): void
    {
        $this->openingHoursFridayOpens = $openingHoursFridayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursFridayCloses(): string
    {
        return $this->openingHoursFridayCloses;
    }

    /**
     * @param string $openingHoursFridayCloses
     */
    public function setOpeningHoursFridayCloses(string $openingHoursFridayCloses): void
    {
        $this->openingHoursFridayCloses = $openingHoursFridayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursSaturdayOpens(): string
    {
        return $this->openingHoursSaturdayOpens;
    }

    /**
     * @param string $openingHoursSaturdayOpens
     */
    public function setOpeningHoursSaturdayOpens(string $openingHoursSaturdayOpens): void
    {
        $this->openingHoursSaturdayOpens = $openingHoursSaturdayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursSaturdayCloses(): string
    {
        return $this->openingHoursSaturdayCloses;
    }

    /**
     * @param string $openingHoursSaturdayCloses
     */
    public function setOpeningHoursSaturdayCloses(string $openingHoursSaturdayCloses): void
    {
        $this->openingHoursSaturdayCloses = $openingHoursSaturdayCloses;
    }

    /**
     * @return string
     */
    public function getOpeningHoursSundayOpens(): string
    {
        return $this->openingHoursSundayOpens;
    }

    /**
     * @param string $openingHoursSundayOpens
     */
    public function setOpeningHoursSundayOpens(string $openingHoursSundayOpens): void
    {
        $this->openingHoursSundayOpens = $openingHoursSundayOpens;
    }

    /**
     * @return string
     */
    public function getOpeningHoursSundayCloses(): string
    {
        return $this->openingHoursSundayCloses;
    }

    /**
     * @param string $openingHoursSundayCloses
     */
    public function setOpeningHoursSundayCloses(string $openingHoursSundayCloses): void
    {
        $this->openingHoursSundayCloses = $openingHoursSundayCloses;
    }

    /**
     * Returns the addresses
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\Address> $addresses
     */
    public function getAddresses()
    {
        return $this->addresses;
    }
    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CodingMs\AddressManager\Domain\Model\Address> $addresses
     */
    public function setAddresses(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $addresses): void
    {
        $this->addresses = $addresses;
    }
}
