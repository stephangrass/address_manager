<?php
namespace CodingMs\AddressManager\Service;

/***************************************************************
*
* Copyright notice
*
* (c) 2019 Thomas Deuling <typo3@coding.ms>
*
* All rights reserved
*
* This script is part of the TYPO3 project. The TYPO3 project is
* free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* The GNU General Public License can be found at
* http://www.gnu.org/copyleft/gpl.html.
*
* This script is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* This copyright notice MUST APPEAR in all copies of the script!
***************************************************************/

use TYPO3\CMS\Extbase\Object\Exception;

/**
 * Services for Geo-Locations
 *
 * @package address_manager
 * @subpackage Service
 *
 * @author Thomas Deuling <typo3@coding.ms>
 */
class GeoLocationService
{

    /**
     * @var string Webservice host, with subsequent slash!
     */
    protected $webserviceHost = 'https://maps.googleapis.com/maps/api/';

    /**
     * Initialize repository
     * @throws Exception
     */
    public function initializeObject()
    {
        // Check curl requirement
        if (!function_exists('curl_version')) {
            throw new Exception('CURL required!');
        }
    }

    /**
     * @param array $params
     * @param string $webservice
     * @param string $action
     * @return mixed
     * @throws Exception
     */
    public function getData(array $params = array(), $webservice = 'geocode', $action = 'json')
    {
        // Build query string from params
        $queryString = $this->buildQueryString($params);
        // Build webservice url
        $url = $this->webserviceHost . $webservice . '/' . $action . $queryString;
        // Init CURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5000);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $data = curl_exec($ch);
        if ($data === false) {
            throw new Exception('Curl error: ' . curl_error($ch) . ' (' . $url . ')');
        }
        curl_close($ch);
        return $data;
    }

    /**
     * Build query string for webservice request
     * @param array $params
     * @return string
     */
    protected function buildQueryString(array $params = array())
    {
        $queryString = '';
        if (!empty($params)) {
            $queryStringParts = array();
            foreach ($params as $key => $value) {
                $queryStringParts[] = $key . '=' . urlencode($value);
            }
            $queryString = '?' . implode('&', $queryStringParts);
        }
        return $queryString;
    }

    /**
     * Get Geo coordinate array from JSON
     * @param $result
     * @return array
     * @throws \Exception
     */
    public function getGeoLocationFromData($result) {
        $return = [];
        // Decode json
        if ($result = json_decode($result, true)) {
            // https://developers.google.com/maps/documentation/geocoding/intro?hl=de
            if($result['status'] == 'OK') {
                if(isset($result['results']) && count($result['results']) > 0) {
                    foreach($result['results'] as $result) {
                        $return = $result['geometry']['location'];
                    }
                }
            }
            else if (isset($result['error_message'])) {
                throw new \Exception($result['error_message']);
            }
        }
        return $return;
    }

}
