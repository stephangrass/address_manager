<?php declare(strict_types=1);

namespace CodingMs\AddressManager\Utility;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Exception;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Tools
 *
 * @package address_manager
 * @subpackage Utility
 * @version 1.2.0
 */
class ToolsUtility
{

    /**
     * Gets a template path from configuration[view]
     *
     * @param array $configurationView Pass $configuration['view'] in this
     * @param string $type Type of template path (for example: template, partial or layout)
     * @param string $file
     * @return string
     * @throws Exception
     */
    public static function getTemplatePath(array $configurationView, string $type = 'template', string $file = ''): string
    {
        $path = '';
        $rootPath = $type . 'RootPath';
        $rootPaths = $type . 'RootPaths';
        // Are there path's' available?!
        if (isset($configurationView[$rootPaths]) && !empty($configurationView[$rootPaths])) {
            // first, sort by key
            krsort($configurationView[$rootPaths]);
            $keys = array_keys($configurationView[$rootPaths]);
            // find the first Template in array
            for ($i = 0; $i < count($keys); $i++) {
                // Template really exists?
                $path = GeneralUtility::getFileAbsFileName($configurationView[$rootPaths][$keys[$i]]);
                if (file_exists($path . $file)) {
                    break;
                }
            }
        } else {
            if (isset($configurationView[$rootPath])) {
                //var_dump($configuration, $type, $file);
                $path = GeneralUtility::getFileAbsFileName($configurationView[$rootPath]);
                if (!file_exists($path . $file)) {
                    throw new Exception('Tools::getTemplatePath (without s) -> path file not exist');
                }
            }
        }
        if ($path == '') {
            throw new Exception('Tools::getTemplatePath not found (' . serialize($configurationView) . ', ' . $type . ', ' . $file . ')');
        }
        return $path;
    }

}
