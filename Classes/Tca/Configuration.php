<?php declare(strict_types=1);

namespace CodingMs\AddressManager\Tca;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 **************************************************************/

/**
 * Configuration presets for TCA fields.
 *
 * @package guidelines
 * @subpackage Tca
 * @author Thomas Deuling <typo3@coding.ms>
 */
class Configuration extends \CodingMs\AdditionalTca\Tca\Configuration
{

    /**
     * @param string $type
     * @param bool $required
     * @param bool $readonly
     * @param string $label
     * @param array $options
     * @return array
     */
    public static function get($type, $required = false, $readonly = false, $label = '', array $options=[]): array
    {
        switch ($type) {
            case 'groupVariant':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'default' => 'default',
                    'items' => [
                        ['default', 'default'],
                        ['primary', 'primary'],
                        ['secondary', 'secondary'],
                        ['success', 'success'],
                        ['info', 'info'],
                        ['warning', 'warning'],
                        ['danger', 'danger'],
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                break;
            case 'openingHours':
                $config = [
                    'type' => 'select',
                    'renderType' => 'selectSingle',
                    'default' => 'closed',
                    'items' => [
                        ['closed', 'closed'],
                        ['00:00', '00:00'],
                        ['00:15', '00:15'],
                        ['00:30', '00:30'],
                        ['00:45', '00:45'],
                        ['01:00', '01:00'],
                        ['01:15', '01:15'],
                        ['01:30', '01:30'],
                        ['01:45', '01:45'],
                        ['02:00', '02:00'],
                        ['02:15', '02:15'],
                        ['02:30', '02:30'],
                        ['02:45', '02:45'],
                        ['03:00', '03:00'],
                        ['03:15', '03:15'],
                        ['03:30', '03:30'],
                        ['03:45', '03:45'],
                        ['04:00', '04:00'],
                        ['04:15', '04:15'],
                        ['04:30', '04:30'],
                        ['04:45', '04:45'],
                        ['05:00', '05:00'],
                        ['05:15', '05:15'],
                        ['05:30', '05:30'],
                        ['05:45', '05:45'],
                        ['06:00', '06:00'],
                        ['06:15', '06:15'],
                        ['06:30', '06:30'],
                        ['06:45', '06:45'],
                        ['07:00', '07:00'],
                        ['07:15', '07:15'],
                        ['07:30', '07:30'],
                        ['07:45', '07:45'],
                        ['08:00', '08:00'],
                        ['08:15', '08:15'],
                        ['08:30', '08:30'],
                        ['08:45', '08:45'],
                        ['09:00', '09:00'],
                        ['09:15', '09:15'],
                        ['09:30', '09:30'],
                        ['09:45', '09:45'],
                        ['10:00', '10:00'],
                        ['10:15', '10:15'],
                        ['10:30', '10:30'],
                        ['10:45', '10:45'],
                        ['11:00', '11:00'],
                        ['11:15', '11:15'],
                        ['11:30', '11:30'],
                        ['11:45', '11:45'],
                        ['12:00', '12:00'],
                        ['12:15', '12:15'],
                        ['12:30', '12:30'],
                        ['12:45', '12:45'],
                        ['13:00', '13:00'],
                        ['13:15', '13:15'],
                        ['13:30', '13:30'],
                        ['13:45', '13:45'],
                        ['14:00', '14:00'],
                        ['14:15', '14:15'],
                        ['14:30', '14:30'],
                        ['14:45', '14:45'],
                        ['15:00', '15:00'],
                        ['15:15', '15:15'],
                        ['15:30', '15:30'],
                        ['15:45', '15:45'],
                        ['16:00', '16:00'],
                        ['16:15', '16:15'],
                        ['16:30', '16:30'],
                        ['16:45', '16:45'],
                        ['17:00', '17:00'],
                        ['17:15', '17:15'],
                        ['17:30', '17:30'],
                        ['17:45', '17:45'],
                        ['18:00', '18:00'],
                        ['18:15', '18:15'],
                        ['18:30', '18:30'],
                        ['18:45', '18:45'],
                        ['19:00', '19:00'],
                        ['19:15', '19:15'],
                        ['19:30', '19:30'],
                        ['19:45', '19:45'],
                        ['20:00', '20:00'],
                        ['20:15', '20:15'],
                        ['20:30', '20:30'],
                        ['20:45', '20:45'],
                        ['21:00', '21:00'],
                        ['21:15', '21:15'],
                        ['21:30', '21:30'],
                        ['21:45', '21:45'],
                        ['22:00', '22:00'],
                        ['22:15', '22:15'],
                        ['22:30', '22:30'],
                        ['22:45', '22:45'],
                        ['23:00', '23:00'],
                        ['23:15', '23:15'],
                        ['23:30', '23:30'],
                        ['23:45', '23:45'],
                    ],
                    'behaviour' => [
                        'allowLanguageSynchronization' => true,
                    ],
                ];
                break;
            default:
                $config = parent::get($type, $required, $readonly, $label, $options);
                break;
        }
        if ($readonly) {
            $config['readOnly'] = true;
        }
        return $config;
    }

}
