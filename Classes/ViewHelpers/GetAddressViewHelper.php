<?php

namespace CodingMs\AddressManager\ViewHelpers;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AddressManager\Domain\Repository\AddressRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;
use CodingMs\AddressManager\Domain\Model\Address;
use Closure;
use Exception;

/**
 * Gets a address by uid or optionally by name
 *
 * {am:getAddress(uid: 123, name: 'Dubai')}
 */
class GetAddressViewHelper extends AbstractViewHelper {

    /**
     * @return void
     */
    public function initializeArguments()
    {
        $this->registerArgument('uid', 'int', 'Address uid');
        $this->registerArgument('name', 'string', 'Address name');
    }

    /**
     * @param array $arguments
     * @param Closure $renderChildrenClosure
     * @param RenderingContextInterface $renderingContext
     * @return string
     * @throws Exception
     */
    public static function renderStatic(array $arguments, Closure $renderChildrenClosure, RenderingContextInterface $renderingContext)
    {
        /** @var ObjectManager $objectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /** @var AddressRepository $addressRepository */
        $addressRepository = $objectManager->get(AddressRepository::class);
        $addressUid = (int)$arguments['uid'];
        if ($addressUid > 0) {
            /** @var Address $address */
            $address = $addressRepository->findOneByUid($addressUid, false);
        } else {
            /** @var Address $address */
            $address = $addressRepository->findOneByName(trim($arguments['name']), false);
        }
        if (!($address instanceof Address)) {
            $address = null;
        }
        return $address;
    }

}
