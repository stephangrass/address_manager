<?php

namespace CodingMs\AddressManager\Form\Element;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Backend\Form\Element\AbstractFormElement;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Generation of TCEform elements of the type "input type=text"
 */
class GeoLocationElement extends AbstractFormElement
{

    /**
     * Object-Manager
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
     */
    protected $configurationManager;

    /**
     * Default width value for a couple of elements like text
     *
     * @var int
     */
    protected $defaultInputWidth = 30;

    /**
     * Minimum width value for a couple of elements like text
     *
     * @var int
     */
    protected $minimumInputWidth = 10;

    /**
     * Maximum width value for a couple of elements like text
     *
     * @var int
     */
    protected $maxInputWidth = 50;

    /**
     * @var array
     */
    protected $extensionConfiguration = [];

    /**
     * @param \TYPO3\CMS\Extbase\Object\ObjectManager $objectManager
     */
    public function injectObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManager $configurationManager
     */
    public function injectConfigurationManager(ConfigurationManagerInterface $configurationManager)
    {
        $this->configurationManager = $configurationManager;
    }

    /**
     * This will render a single-line input form field, possibly with various control/validation features
     *
     * @return array
     */
    public function render(): array
    {
        //
        $extensionKey = 'address_manager';
        $parameterArray = $this->data['parameterArray'];
        //
        // Get extension configuration
        $this->fetchExtensionConfiguration($extensionKey);
        //
        // Google maps
        $apiKey = $this->getGoogleMapsApiKey();
        $message = '';
        if ($apiKey === '') {
            $message = '<span class="text-danger">' . $this->translate('api_key_not_found') . '</span>';
        }
        //
        // Add JavaScript and CSS
        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $mapsUrl = '//maps.google.com/maps/api/js?key=' . $apiKey . '&libraries=places';
        $pageRenderer->addJsFile($mapsUrl, null, false, true, '', true);
        $extRealPath = '../' . PathUtility::stripPathSitePrefix(ExtensionManagementUtility::extPath($extensionKey));
        $pageRenderer->loadRequireJsModule('TYPO3/CMS/AddressManager/Backend/AddressManager');
        $pageRenderer->addCssFile($extRealPath . 'Resources/Public/Stylesheets/Backend/AddressManager.css');
        $resultArray = [
            'additionalJavaScriptPost' => [],
            'additionalJavaScriptSubmit' => [],
            'additionalHiddenFields' => [],
            'additionalInlineLanguageLabelFiles' => [],
            'stylesheetFiles' => [],
            // Can hold strings or arrays, string = requireJS module, array = requireJS module + callback e.g. array('TYPO3/Foo/Bar', 'function() {}')
            'requireJsModules' => [],
            'extJSCODE' => '',
            'inlineData' => [],
            'html' => '',
        ];

        $config = $parameterArray['fieldConf']['config'];
        $size = MathUtility::forceIntegerInRange($config['size'] ?: $this->defaultInputWidth, $this->minimumInputWidth,
            $this->maxInputWidth);
        $evalList = GeneralUtility::trimExplode(',', $config['eval'], true);
        $classes = [];
        $attributes = [];
        $validationRules = [];
        $paramsList = [
            'field' => $parameterArray['itemFormElName'],
            'evalList' => '',
            'is_in' => '',
        ];
        //
        // Set classes
        $classes[] = 'form-control';
        $classes[] = 't3js-clearable';
        $classes[] = 'hasDefaultValue';
        //
        // Calculate attributes
        $attributes['data-formengine-validation-rules'] = json_encode($validationRules);
        $attributes['data-formengine-input-params'] = json_encode($paramsList);
        $attributes['data-formengine-input-name'] = htmlspecialchars($parameterArray['itemFormElName']);
        $attributes['id'] = StringUtility::getUniqueId('formengine-input-');
        $attributes['value'] = '';
        if (isset($config['max']) && (int)$config['max'] > 0) {
            $attributes['maxlength'] = (int)$config['max'];
        }
        if (!empty($classes)) {
            $attributes['class'] = implode(' ', $classes);
        }
        //
        // This is the EDITABLE form field.
        if (!empty($config['placeholder'])) {
            $attributes['placeholder'] = trim($config['placeholder']);
        }
        //
        // Build the attribute string
        $attributeString = '';
        foreach ($attributes as $attributeName => $attributeValue) {
            $attributeString .= ' ' . $attributeName . '="' . htmlspecialchars($attributeValue) . '"';
        }
        //
        // Get translations
        $grabAddressFromRecordTitle = $this->translate('grab_address_from_record');
        $getGeoLocationTitle = $this->translate('get_geo_location');
        $showMapTitle = $this->translate('show_on_map');
        $addressNotFound = $this->translate('address_not_found');
        $geocoderStatus = $this->translate('geocoder_status');
        $enterAnAddress = $this->translate('enter_an_address');
        //
        $elementName = $parameterArray['itemFormElName'];
        $elementValue = htmlspecialchars($parameterArray['itemFormElValue']);
        $iconPath = '/typo3conf/ext/' . $extensionKey . '/Resources/Public/Icons/';
        //
        // Build HTML
        $html = '
            <div class="form-control-wrap">
                <div class="input-group" id="address-manager-input">
                    <input type="text" ' . $attributeString . $parameterArray['onFocus'] . ' />
                    <input type="hidden" name="' . $elementName . '" value="' . $elementValue . '" />
                    <span class="input-group-btn" data-event="grabAddressFromRecord">
                        <label class="btn btn-default" title="' . $grabAddressFromRecordTitle . '">
                            <img width="16" height="16" src="' . $iconPath . 'iconmonstr-undo-1.svg" alt="' . $grabAddressFromRecordTitle . '">
                        </label>
                    </span>
                    <span class="input-group-btn" data-event="getGeoLocation">
                        <label class="btn btn-default" title="' . $getGeoLocationTitle . '">
                            <img width="16" height="16" src="' . $iconPath . 'iconmonstr-map-8.svg" alt="' . $getGeoLocationTitle . '">
                        </label>
                    </span>
                    <span class="input-group-btn" data-event="showMap">
                        <label class="btn btn-default" title="' . $showMapTitle . '">
                            <img width="16" height="16" src="' . $iconPath . 'iconmonstr-eye-6.svg" alt="' . $showMapTitle . '">
                        </label>
                    </span>
                </div>
                <div id="address-manager-message">' . $message . '</div>
            </div>
            <div id="address-manager-map" data-uid="' . $this->data['databaseRow']['uid'] . '" data-message-address-not-found="' . $addressNotFound . '" data-message-geocoder-status="' . $geocoderStatus . '" data-message-enter-an-address="' . $enterAnAddress . '"></div>
            ';
        //
        $resultArray['html'] = $html;
        return $resultArray;
    }

    /**
     * @param $extensionKey
     */
    protected function fetchExtensionConfiguration($extensionKey)
    {
        /** @var ExtensionConfiguration $extensionConfigurationAPI */
        $extensionConfigurationAPI = GeneralUtility::makeInstance(ExtensionConfiguration::class);
        $this->extensionConfiguration = $extensionConfigurationAPI->get($extensionKey);
    }

    /**
     * @return string
     */
    protected function getGoogleMapsApiKey()
    {
        $apiKey = '';
        if (isset($this->extensionConfiguration['googleMaps']) && isset($this->extensionConfiguration['googleMaps']['apiKey'])) {
            $apiKey = trim($this->extensionConfiguration['googleMaps']['apiKey']);
        }
        return $apiKey;
    }

    /**
     * @return \TYPO3\CMS\Lang\LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

    /**
     * @param string $key
     * @return string
     */
    protected function translate($key): string
    {
        $translationKey = 'LLL:EXT:address_manager/Resources/Private/Language/locallang_db.xlf';
        $translationKey .= ':tx_addressmanager_domain_model_address.map_marker_wizard_' . $key;
        $translation = $this->getLanguageService()->sL($translationKey);
        return $translation;
    }

}
