<?php

namespace CodingMs\AddressManager\Controller;

/***************************************************************
 *
 * Copyright notice
 *
 * (c) 2019 Thomas Deuling <typo3@coding.ms>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use CodingMs\AddressManager\Domain\Repository\AddressGroupRepository;
use CodingMs\AddressManager\Domain\Repository\AddressOrganisationRepository;
use CodingMs\AddressManager\Domain\Repository\AddressPositionRepository;
use CodingMs\AddressManager\Domain\Repository\AddressRepository;
use CodingMs\AddressManager\Domain\Model\FileReference;
use CodingMs\AddressManager\PageTitle\PageTitleProvider;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerInterface;
use TYPO3\CMS\Core\MetaTag\MetaTagManagerRegistry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use CodingMs\AddressManager\Domain\Model\Address;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface;
use TYPO3\CMS\Extbase\Service\ImageService;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Persistence\Exception\InvalidQueryException;

/**
 * AddressController
 * @noinspection PhpUnused
 */
class AddressController extends ActionController
{

    /**
     * @var AddressRepository
     */
    protected $addressRepository;

    /**
     * @var AddressGroupRepository
     */
    protected $addressGroupRepository;

    /**
     * @var AddressPositionRepository
     */
    protected $addressPositionRepository;

    /**
     * @var AddressOrganisationRepository
     */
    protected $addressOrganisationRepository;

    /**
     * @param AddressRepository $addressRepository
     * @noinspection PhpUnused
     */
    public function injectAddressRepository(AddressRepository $addressRepository)
    {
        $this->addressRepository = $addressRepository;
    }

    /**
     * @param AddressGroupRepository $addressGroupRepository
     * @noinspection PhpUnused
     */
    public function injectAddressGroupRepository(AddressGroupRepository $addressGroupRepository)
    {
        $this->addressGroupRepository = $addressGroupRepository;
    }

    /**
     * @param AddressPositionRepository $addressPositionRepository
     * @noinspection PhpUnused
     */
    public function injectAddressPositionRepository(AddressPositionRepository $addressPositionRepository)
    {
        $this->addressPositionRepository = $addressPositionRepository;
    }

    /**
     * @param AddressOrganisationRepository $addressOrganisationRepository
     * @noinspection PhpUnused
     */
    public function injectAddressOrganisationRepository(AddressOrganisationRepository $addressOrganisationRepository)
    {
        $this->addressOrganisationRepository = $addressOrganisationRepository;
    }

    /**
     * Initialize actions
     */
    public function initializeAction()
    {
        //
        if(!isset($this->settings['framework']) || trim($this->settings['framework']) === '{$themes.framework}') {
            $this->settings['framework'] = 'Bootstrap4';
        }
        //
        // Use an alternative record storage?
        if((int)$this->settings['alternativeStoragePid'] > 0) {
            /** @var QuerySettingsInterface $defaultQuerySettings */
            $defaultQuerySettings = $this->objectManager->get(QuerySettingsInterface::class);
            $defaultQuerySettings->setStoragePageIds([(int)$this->settings['alternativeStoragePid']]);
            $this->addressRepository->setDefaultQuerySettings($defaultQuerySettings);
            $this->addressGroupRepository->setDefaultQuerySettings($defaultQuerySettings);
            $this->addressPositionRepository->setDefaultQuerySettings($defaultQuerySettings);
            $this->addressOrganisationRepository->setDefaultQuerySettings($defaultQuerySettings);
        }
        //
    }

    /**
     * List action
     *
     * @return void
     * @throws NoSuchArgumentException
     * @throws InvalidQueryException
     * @noinspection PhpUnused
     */
    public function listAction()
    {
        // Static template available?
        // Debug extension?
        if (!isset($this->settings['debug'])) {
            $messageBody = 'Please include the static template!';
            $messageTitle = 'Error';
            $this->addFlashMessage($messageBody, $messageTitle, AbstractMessage::ERROR);
            return;
        }
        if((int)$this->settings['list']['defaultPid'] === 0) {
            $messageBody = 'Please set the page uid of the default address list page!';
            $messageTitle = 'Error';
            $this->addFlashMessage($messageBody, $messageTitle, AbstractMessage::ERROR);
            return;
        }
        $this->settings['debug'] = (boolean)$this->settings['debug'];
        //
        // Display type
        if(!isset($this->settings['displayType']) || trim($this->settings['displayType']) === '') {
            $this->settings['displayType'] = 'Default';
        }
        //
        // Get offset/limit from configuration
        $limit = (int)$this->settings['list']['limit'];
        $offset = (int)$this->settings['list']['offset'];
        // Overwrite limit with user setting
        if ($this->request->hasArgument("limit")) {
            $limit = (int)$this->request->getArgument("limit");
        }
        // Ensure the limit isn't empty
        if ($limit == 0) {
            $limit = 9999;
        }
        //
        // Get sort by
        $sortBy = $this->settings['list']['sortBy'];
        // Get allowed groups for filter function
        $allowedGroups = [];
        if (trim($this->settings['list']['allowedGroups']) != '') {
            $allowedGroups = explode(',', $this->settings['list']['allowedGroups']);
        }
        //
        // Get allowed positions for filter function
        $allowedPositions = array();
        if (trim($this->settings['list']['allowedPositions']) != '') {
            $allowedPositions = explode(',', $this->settings['list']['allowedPositions']);
        }
        //
        // Get allowed organisations for filter function
        $allowedOrganisations = array();
        if (trim($this->settings['list']['allowedOrganisations']) != '') {
            $allowedOrganisations = explode(',', $this->settings['list']['allowedOrganisations']);
        }
        //
        // Identify the current selected group
        $group = null;
        if ($this->request->hasArgument('groupSelect')) {
            $groupUid = (int)$this->request->getArgument('groupSelect');
            $group = $this->addressGroupRepository->findByIdentifier($groupUid);
        }
        // Identify the current selected position
        $position = null;
        if ($this->request->hasArgument('positionSelect')) {
            $positionUid = (int)$this->request->getArgument('positionSelect');
            $position = $this->addressPositionRepository->findByIdentifier($positionUid);
        }
        // Identify the current selected organisation
        $organisation = null;
        if ($this->request->hasArgument('organisationSelect')) {
            $organisationUid = (int)$this->request->getArgument('organisationSelect');
            $organisation = $this->addressOrganisationRepository->findByIdentifier($organisationUid);
        }
        //
        /** @var QueryResult $groupSelect */
        $groupSelect = $this->addressGroupRepository->findAllByAllowedGroups(
            $allowedGroups,
            $this->settings['list']['group']['sortBy'],
            $this->settings['list']['group']['sortOrder']
        );
        /** @var QueryResult $positionSelect */
        $positionSelect = $this->addressPositionRepository->findAllByAllowedPositions(
            $allowedPositions,
            $this->settings['list']['position']['sortBy'],
            $this->settings['list']['position']['sortOrder']
        );
        /** @var QueryResult $organisationSelect */
        $organisationSelect = $this->addressOrganisationRepository->findAllByAllowedOrganisations(
            $allowedOrganisations,
            $this->settings['list']['organisation']['sortBy'],
            $this->settings['list']['organisation']['sortOrder']
        );
        //
        // Get plugins record uid
        $content = $this->configurationManager->getContentObject()->data;
        /** @var QueryResult $address */
        $addressCount = $this->addressRepository->countAllForInit(
            $groupSelect,
            $positionSelect,
            $organisationSelect
        );
        /** @var QueryResult $address */
        $address = $this->addressRepository->findAllForInit(
            $groupSelect,
            $positionSelect,
            $organisationSelect,
            $sortBy,
            'ASC',
            $offset,
            $limit
        );
        if ($address->count() === 0) {
            $messageKey = 'tx_addressmanager_message.info_no_addresses_found';
            $message = LocalizationUtility::translate($messageKey, 'AddressManager');
            $this->addFlashMessage($message, '', FlashMessage::INFO);
        }
        // Explode filter fields
        $this->explodeListFilterFields();
        //
        // Location and co.
        $latitude = '';
        if ($this->request->hasArgument('latitude')) {
            $latitude = $this->request->getArgument('latitude');
        }
        $longitude = '';
        if ($this->request->hasArgument('longitude')) {
            $longitude = $this->request->getArgument('longitude');
        }
        $location = '';
        if ($this->request->hasArgument('location')) {
            $location = $this->request->getArgument('location');
        }
        $distance = '';
        if ($this->request->hasArgument('distance')) {
            $distance = $this->request->getArgument('distance');
        }
        $search = '';
        if ($this->request->hasArgument('searchField')) {
            $search = $this->request->getArgument('searchField');
        }
        $customString = '';
        if ($this->request->hasArgument('customStringField')) {
            $customString = $this->request->getArgument('customStringField');
        }
        $this->view->assign('latitude', $latitude);
        $this->view->assign('longitude', $longitude);
        $this->view->assign('location', $location);
        $this->view->assign('distance', $distance);
        $this->view->assign('search', $search);
        $this->view->assign('customString', $customString);
        //
        $this->view->assign('limit', $limit);
        $this->view->assign('settings', $this->settings);
        $this->view->assign('group', $group);
        $this->view->assign('position', $position);
        $this->view->assign('organisation', $organisation);
        // Select box items
        $this->view->assign('groupSelect', $groupSelect);
        $this->view->assign('positionSelect', $positionSelect);
        $this->view->assign('organisationSelect', $organisationSelect);
        // Address items
        $this->view->assign('addressItems', $address);
        $this->view->assign('addressCount', $addressCount);
        // Uid of this plugin
        $this->view->assign('uid', $content['uid']);
        $this->view->assign('content', $content);
    }

    /**
     * action show
     *
     * @param Address $address
     * @return void
     * @noinspection PhpUnused
     */
    public function showAction(Address $address)
    {
        $this->injectMetaInformation($address, $this->settings['siteName']);
        $this->view->assign('address', $address);
    }

    /**
     * action showSingle
     *
     * @return void
     * @noinspection PhpUnused
     */
    public function showSingleAction()
    {
        if ($this->settings['address'] !== '') {
            /** @var Address $address */
            $address = $this->addressRepository->findByIdentifier((int)$this->settings['address']);
            if($address instanceof Address) {
                $this->injectMetaInformation($address, $this->settings['siteName']);
            }
            $this->view->assign('address', $address);
        }
    }

    /**
     * @param Address $address
     * @param string $siteName
     */
    protected function injectMetaInformation(Address $address, string $siteName = '')
    {
        //
        // Get fallback values
        $htmlTitle = trim($address->getHtmlTitle());
        if($htmlTitle === '') {
            $htmlTitle = $address->getName();
        }
        $description = trim($address->getMetaDescription());
        if($description === '') {
            $description = substr(strip_tags($address->getDescription()), 0, 160);
        }
        //
        /** @var MetaTagManagerRegistry $metaTagManager */
        $metaTagManagerRegistry = GeneralUtility::makeInstance(MetaTagManagerRegistry::class);
        //
        /** @var MetaTagManagerInterface $metaTagManagerDescription */
        $metaTagManagerDescription = $metaTagManagerRegistry->getManagerForProperty('description');
        $metaTagManagerDescription->addProperty('description', $description);
        //
        /** @var MetaTagManagerInterface $metaTagManagerAbstract */
        $metaTagManagerAbstract = $metaTagManagerRegistry->getManagerForProperty('abstract');
        $metaTagManagerAbstract->addProperty('abstract', $address->getMetaAbstract());
        //
        /** @var MetaTagManagerInterface $metaTagManagerKeywords */
        $metaTagManagerKeywords = $metaTagManagerRegistry->getManagerForProperty('keywords');
        $metaTagManagerKeywords->addProperty('keywords', $address->getMetaKeywords());
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgTitle */
        $metaTagManagerOgTitle = $metaTagManagerRegistry->getManagerForProperty('og:title');
        $metaTagManagerOgTitle->addProperty('og:title', $htmlTitle);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgDescription */
        $metaTagManagerOgDescription = $metaTagManagerRegistry->getManagerForProperty('og:description');
        $metaTagManagerOgDescription->addProperty('og:description', $description);
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgType */
        $metaTagManagerOgType = $metaTagManagerRegistry->getManagerForProperty('og:type');
        $metaTagManagerOgType->addProperty('og:type', 'WebPage');
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgSiteName */
        $metaTagManagerOgSiteName = $metaTagManagerRegistry->getManagerForProperty('og:site_name');
        $metaTagManagerOgSiteName->addProperty('og:site_name', $siteName);
        if($siteName === '{$themes.configuration.siteName}') {
            $messageBody = 'Please set TypoScript constant {$themes.configuration.siteName} with sitename for og:site_name';
            $messageTitle = 'Error';
            $this->addFlashMessage($messageBody, $messageTitle, AbstractMessage::ERROR);
        }
        //
        /** @var MetaTagManagerInterface $metaTagManagerOgUrl */
        $metaTagManagerOgUrl = $metaTagManagerRegistry->getManagerForProperty('og:url');
        $metaTagManagerOgUrl->addProperty('og:url', GeneralUtility::getIndpEnv('TYPO3_REQUEST_URL'));
        //
        // Preview image
        $previewImages = $address->getImagesPreviewOnly();
        if (count($previewImages) > 0) {
            /** @var FileReference $previewImage */
            $previewImage = $previewImages[0];
            /** @var ImageService $imageService */
            $imageService = $this->objectManager->get(ImageService::class);
            $image = $imageService->getImage($previewImage, $previewImage, true);
            //
            // Cropping
            if ($image->hasProperty('crop') && $image->getProperty('crop')) {
                $cropString = $image->getProperty('crop');
            }
            $cropVariantCollection = CropVariantCollection::create((string)$cropString);
            $cropVariant = 'default';
            $cropArea = $cropVariantCollection->getCropArea($cropVariant);
            $processingInstructions = [
                'width' => 600,
                'crop' => $cropArea->isEmpty() ? null : $cropArea->makeAbsoluteBasedOnFile($image),
            ];
            //
            $processedImage = $imageService->applyProcessingInstructions($image, $processingInstructions);
            $imageUrl = $imageService->getImageUri($processedImage, true);
            //
            /** @var MetaTagManagerInterface $metaTagManagerOgImage */
            $metaTagManagerOgImage = $metaTagManagerRegistry->getManagerForProperty('og:image');
            $metaTagManagerOgImage->addProperty('og:image', $imageUrl);
        }
        //
        /** @var PageTitleProvider $seoTitlePageTitleProvider */
        $seoTitlePageTitleProvider = GeneralUtility::makeInstance(PageTitleProvider::class);
        $seoTitlePageTitleProvider->setTitle($htmlTitle);
    }

    /**
     * action group teaser
     *
     * @return void
     * @noinspection PhpUnused
     */
    public function groupTeaserAction()
    {
        $allowedGroups = array();
        if (trim($this->settings['list']['allowedGroups']) != '') {
            $allowedGroups = explode(',', $this->settings['list']['allowedGroups']);
        }
        /** @var QueryResult $groupSelect */
        $groups = $this->addressGroupRepository->findAllByAllowedGroups(
            $allowedGroups,
            $this->settings['list']['group']['sortBy'],
            $this->settings['list']['group']['sortOrder']
        );
        $this->view->assign('groups', $groups);
    }

    /**
     * Prepares the filter fields
     */
    protected function explodeListFilterFields()
    {
        if (trim($this->settings['list']['filterFields']) == '') {
            $this->settings['list']['filterFields'] = array();
        }
        else {
            $this->settings['list']['showFilter'] = true;
            $this->settings['list']['filterFields'] = explode(',', $this->settings['list']['filterFields']);
            $this->settings['list']['filterFields'] = array_combine(
                $this->settings['list']['filterFields'],
                $this->settings['list']['filterFields']
            );
        }
    }

}
