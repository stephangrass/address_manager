#
# Table structure for table 'tx_addressmanager_domain_model_address'
#
CREATE TABLE tx_addressmanager_domain_model_address (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	name varchar(255) DEFAULT '' NOT NULL,
	slug varchar(255) DEFAULT '' NOT NULL,
	frontend_user int(11) DEFAULT '0' NOT NULL,
	gender varchar(255) DEFAULT '' NOT NULL,
	first_name varchar(255) DEFAULT '' NOT NULL,
	middle_name varchar(255) DEFAULT '' NOT NULL,
	last_name varchar(255) DEFAULT '' NOT NULL,
	birthday int(11) DEFAULT '0' NOT NULL,
	title varchar(255) DEFAULT '' NOT NULL,
	email varchar(255) DEFAULT '' NOT NULL,
	phone varchar(255) DEFAULT '' NOT NULL,
	mobile varchar(255) DEFAULT '' NOT NULL,
	www varchar(255) DEFAULT '' NOT NULL,
	address varchar(255) DEFAULT '' NOT NULL,
	building varchar(255) DEFAULT '' NOT NULL,
	room varchar(255) DEFAULT '' NOT NULL,
	company varchar(255) DEFAULT '' NOT NULL,
	city varchar(255) DEFAULT '' NOT NULL,
	postal_code varchar(255) DEFAULT '' NOT NULL,
	region varchar(255) DEFAULT '' NOT NULL,
	country varchar(255) DEFAULT '' NOT NULL,
	fax varchar(255) DEFAULT '' NOT NULL,
	facebook varchar(255) DEFAULT '' NOT NULL,
	instagram varchar(255) DEFAULT '' NOT NULL,
	twitter varchar(255) DEFAULT '' NOT NULL,
    youtube varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,
	# Map
	map_latitude varchar(255) DEFAULT '' NOT NULL,
	map_longitude varchar(255) DEFAULT '' NOT NULL,
	map_wizard varchar(255) DEFAULT '' NOT NULL,
	map_zoom int(11) DEFAULT '0' NOT NULL,
	map_tooltip varchar(255) DEFAULT '' NOT NULL,
	map_link varchar(255) DEFAULT '' NOT NULL,
	map_marker varchar(255) DEFAULT '' NOT NULL,
	directions text,
	# Relations
	groups int(11) unsigned DEFAULT '0' NOT NULL,
	positions int(11) unsigned DEFAULT '0' NOT NULL,
	organisations int(11) unsigned DEFAULT '0' NOT NULL,
	images int(11) unsigned NOT NULL default '0',
	files int(11) unsigned NOT NULL default '0',
	# Only for calculating the distance of the radial search.
	# Must be available in DB, otherwise the core migriation wizard will fail!
	distance varchar(255) DEFAULT '' NOT NULL,
	#
	# Tab: SEO
	html_title varchar(255) DEFAULT '' NOT NULL,
	meta_abstract varchar(255) DEFAULT '' NOT NULL,
	meta_description varchar(255) DEFAULT '' NOT NULL,
	meta_keywords varchar(255) DEFAULT '' NOT NULL,

	attribute1 text,
	attribute2 text,
	attribute3 varchar(255) DEFAULT '' NOT NULL,
	attribute4 varchar(255) DEFAULT '' NOT NULL,
	attribute5 varchar(255) DEFAULT '' NOT NULL,
	attribute6 varchar(255) DEFAULT '' NOT NULL,
	attribute7 varchar(255) DEFAULT '' NOT NULL,
	attribute8 varchar(255) DEFAULT '' NOT NULL,
	attribute9 varchar(255) DEFAULT '' NOT NULL,
	attribute10 varchar(255) DEFAULT '' NOT NULL,
	attribute11 varchar(255) DEFAULT '' NOT NULL,
	attribute12 varchar(255) DEFAULT '' NOT NULL,
	attribute13 varchar(255) DEFAULT '' NOT NULL,
	attribute14 varchar(255) DEFAULT '' NOT NULL,
	attribute15 varchar(255) DEFAULT '' NOT NULL,
	attribute16 varchar(255) DEFAULT '' NOT NULL,
	attribute17 varchar(255) DEFAULT '' NOT NULL,

	opening_hours_monday_opens varchar(7) DEFAULT '' NOT NULL,
	opening_hours_monday_closes varchar(7) DEFAULT '' NOT NULL,
	opening_hours_tuesday_opens varchar(7) DEFAULT '' NOT NULL,
	opening_hours_tuesday_closes varchar(7) DEFAULT '' NOT NULL,
	opening_hours_wednesday_opens varchar(7) DEFAULT '' NOT NULL,
	opening_hours_wednesday_closes varchar(7) DEFAULT '' NOT NULL,
	opening_hours_thursday_opens varchar(7) DEFAULT '' NOT NULL,
	opening_hours_thursday_closes varchar(7) DEFAULT '' NOT NULL,
	opening_hours_friday_opens varchar(7) DEFAULT '' NOT NULL,
	opening_hours_friday_closes varchar(7) DEFAULT '' NOT NULL,
	opening_hours_saturday_opens varchar(7) DEFAULT '' NOT NULL,
	opening_hours_saturday_closes varchar(7) DEFAULT '' NOT NULL,
	opening_hours_sunday_opens varchar(7) DEFAULT '' NOT NULL,
	opening_hours_sunday_closes varchar(7) DEFAULT '' NOT NULL,

	addresses int(11) unsigned DEFAULT '0' NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)
);

#
# Table structure for table 'tx_addressmanager_domain_model_addressgroup'
#
CREATE TABLE tx_addressmanager_domain_model_addressgroup (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	slug varchar(255) DEFAULT '' NOT NULL,
	variant varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)
);

#
# Table structure for table 'tx_addressmanager_domain_model_addressposition'
#
CREATE TABLE tx_addressmanager_domain_model_addressposition (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)
);

#
# Table structure for table 'tx_addressmanager_domain_model_addressorganisation'
#
CREATE TABLE tx_addressmanager_domain_model_addressorganisation (

	uid int(11) NOT NULL auto_increment,
	pid int(11) DEFAULT '0' NOT NULL,

	title varchar(255) DEFAULT '' NOT NULL,
	description text NOT NULL,

	tstamp int(11) unsigned DEFAULT '0' NOT NULL,
	crdate int(11) unsigned DEFAULT '0' NOT NULL,
	cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
	deleted tinyint(4) unsigned DEFAULT '0' NOT NULL,
	hidden tinyint(4) unsigned DEFAULT '0' NOT NULL,
	starttime int(11) unsigned DEFAULT '0' NOT NULL,
	endtime int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) DEFAULT '0' NOT NULL,

	t3ver_oid int(11) DEFAULT '0' NOT NULL,
	t3ver_id int(11) DEFAULT '0' NOT NULL,
	t3ver_wsid int(11) DEFAULT '0' NOT NULL,
	t3ver_label varchar(255) DEFAULT '' NOT NULL,
	t3ver_state tinyint(4) DEFAULT '0' NOT NULL,
	t3ver_stage int(11) DEFAULT '0' NOT NULL,
	t3ver_count int(11) DEFAULT '0' NOT NULL,
	t3ver_tstamp int(11) DEFAULT '0' NOT NULL,
	t3ver_move_id int(11) DEFAULT '0' NOT NULL,

	sys_language_uid int(11) DEFAULT '0' NOT NULL,
	l10n_parent int(11) DEFAULT '0' NOT NULL,
	l10n_diffsource mediumblob,
	l10n_state text DEFAULT NULL,

	PRIMARY KEY (uid),
	KEY parent (pid),
	KEY t3ver_oid (t3ver_oid,t3ver_wsid),
	KEY language (l10n_parent,sys_language_uid)
);

#
# Table structure for table 'tx_addressmanager_address_addressgroup_mm'
#
CREATE TABLE tx_addressmanager_address_addressgroup_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_addressmanager_address_addressposition_mm'
#
CREATE TABLE tx_addressmanager_address_addressposition_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_addressmanager_address_addressorganisation_mm'
#
CREATE TABLE tx_addressmanager_address_addressorganisation_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'tx_addressmanager_address_address_mm'
#
CREATE TABLE tx_addressmanager_address_address_mm (
	uid_local int(11) unsigned DEFAULT '0' NOT NULL,
	uid_foreign int(11) unsigned DEFAULT '0' NOT NULL,
	sorting int(11) unsigned DEFAULT '0' NOT NULL,
	sorting_foreign int(11) unsigned DEFAULT '0' NOT NULL,

	KEY uid_local (uid_local),
	KEY uid_foreign (uid_foreign)
);

#
# Table structure for table 'sys_file_reference'
#
CREATE TABLE sys_file_reference (
	preview tinyint(4) DEFAULT '0' NOT NULL
);

