# TYPO3 Address-Manager Migration

## Version 2.0.0

Die Bilder haben eine neue Checkbox für *in Vorschau darstellen* bekommen - d.h. Sie müssen u.U. in jeder Adresse ein Bild selektieren, welches in der Listenansicht verwendet wird. Dieses Feld wurde nach kurzer Testphase von `showinpreview` zu `preview` umbenannt, daher ist ggf. eine Migration notwendig:
`UPDATE sys_file_reference SET preview = '1' WHERE tablenames = 'tx_addressmanager_domain_model_address' AND fieldname = 'images' AND showinpreview = 1;`

## Version 1.8.0

Das List.html hat für das Loading-Info ein neues Data-Attribute bekommen (data-hidden-class). In diesem muss die CSS-Klasse enthalten sein, die das LoadingInfo hinzufügt/entfernt wenn der Status gewechselt wird. Der Inhalt kann bei Bootstrap 4 d-none sein, bei Bootstrap 3 hidden.

## Version 1.7.2

Die Relationen der Adressen wurden so umkonfiguriert, so dass diese in Übersetzungen immer in der default Sprache ausgewählt werden müssen - bzw. nur noch aus gewählt werden können. Dies hilft Verwirrungen bei gleichnamig übersetzten Relationen zu vermeiden. D.h. bei einem Update zu dieser Version sollte sichergestellt sein, das die Relationen alle in der default Sprache ausgewählt sind!


## Version 1.4.0

1.  Pins können jetzt verschiedene Stile haben. Hier müssen Sie möglicherweise einige Änderungen vornehmen:
    1.  Wenn Sie die Listenelementvorlage überschrieben haben, müssen Sie dem Wrapper ein neues Datenattribut hinzufügen:
        `data-map-marker="{am:variable.get(name: 'settings.list.map.marker.style.{address.mapMarker}')}"`
    2.  Die Kartenmarkierungsdefinition erfordert möglicherweise eine Farbzuweisung. Zuvor war die Farbe eingestellt durch:
        `plugin.tx_addressmanager.settings.list.map.marker.color = #337ab7`
        Diese Einstellungen wurden entfernt, da die Farbe und der Stil mithilfe des Einstellungsknotens * style * festgelegt werden können. Beispiel:
        `plugin.tx_addressmanager.settings.list.map.marker.style.default = <svg ... ><path fill="#337ab7" ...`


## Migration von ftm_ext_address

1.  MySQL-Export aller Tabellen und Ersetzen von `tx_ftmextaddress` durch` tx_addressmanager` - danach importieren Sie diese Daten.
2.  MySQL-Anweisung `UPDATE` sys_file_reference` SET `tablenames` = 'tx_addressmanager_domain_model_address' WHERE` tablenames` LIKE '% tx_ftmextaddress%' `
3.  MySQL-Anweisung `UPDATE` tt_content` SET `list_type` = 'addressmanager_addresslist' WHERE` list_type` LIKE 'ftmextaddress_addresslist'`
4.  Suchen Sie im Theme/in der Konfiguration nach `tx_ftmextaddress` und ersetzen Sie diese durch` tx_addressmanager`.
5.  Deinstallieren Sie ftm_ext_address
