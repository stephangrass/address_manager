# Address-Manager Change-Log

## Upcoming version

*   [DOC] Change description how to enable GoogleMapsConsent
*   [DOC] Fix translation error in OpenStreetMap HowTo



## 2021-05-19  Release of version 2.3.1

*   [TASK] Add javascript code to include OpenStreetMap and a HowTo which explains the usage



## 2021-05-14  Release of version 2.3.0

*   [FEATURE] Add a plugin for display a selected address
*   [FEATURE] Add a selection field for assigning related addresses



## 2021-05-13  Release of version 2.2.1

*   [BUGFIX] Fix usage of alternative storage when requesting JSON data without filter



## 2021-03-28  Release of version 2.2.0

*   [FEATURE] Add alternative storage selection in plugins
*   [TASK] Add example code for opening hours
*   [TASK] Display address directions in Fluid template as HTML



## 2021-01-04  Release of version 2.1.7

*   [TASK] Add documentation EN translation file
*   [BUGFIX] Fix TypoScript constant assignment



## 2020-12-08  Release of version 2.1.6

*   [BUGFIX] Fix TypoScript conditions of GoogleMaps consent and sourceopt exclude



## 2020-12-02  Release of version 2.1.5

*   [BUGFIX] Fix JavaScript issue in case of google library isn't defined
*   [BUGFIX] If location filter field is empty, reset latitude/longitude
*   [BUGFIX] Reset really all filter fields on resetAllData()
*   [TASK] Add another how to article about reset buttons



## 2020-11-25  Release of version 2.1.4

*   [BUGFIX] Fix geo location query with translated records



## 2020-11-23  Release of version 2.1.3

*   [BUGFIX] Fix extension loaded conditions



## 2020-11-02  Release of version 2.1.2

*   [BUGFIX] Fix empty birthday field in address creation process
*   [TASK] Fix typo in translations



## 2020-10-04  Release of version 2.1.1

*   [TASK] Add TypoScript constants for configuration see #12
*   [BUGFIX] Add missing default value for calendar event field



## 2020-09-17  Release of version 2.1.0

*   [BUGFIX] Add missing extbase mapping in TypoScript
*   [TASK] Add composer.json information
*   [TASK] Remove console.log from JavaScript file
*   [FEATURE] Google Maps consent: ask for consent before loading Google Maps and store that information in a cookie



## 2020-09-02  Release of version 2.0.0

*   [TASK] Allow groups, organizations and positions in "all" language to be used
*   [TASK] Add additional_tca as requirement and use "information row" instead of "support row" in the TCA and Flexform settings
*   [TASK] Allow multiple fields for fulltext search (eg. plugin.tx_addressmanager.settings.list.customString.field = title,name,first_name,description)
*   [BUGFIX] Add default value for field frontend user
*   [TASK] Optimize page title provider configuration
*   [BUGFIX] Don't cache list view, because of passed search parameters
*   [TASK] Add TypoScript constant for site name
*   [TASK] Remove basic, advanced and expert TypoScript constant category
*   [BREAKING] Change 'showinpreview' field in sys_file_reference to 'preview', because that field (which is provided by the News extensions as well) has change his functionality!
*   [TASK] Add fallback values for meta and open graph tags
*   [BUGFIX] Add TCA for crdate and tstamp in address configuration, for displaying that dates in frontend
*   [BUGFIX] Fix TCA for RTE in address configuration for TYPO3 10
*   [TASK] Fix Map wizard to work with TYPO3 10
*   [TASK] Pass search word and custom string field from Filter plugin to List plugin
*   [BUGFIX] Ensure that the framework type is set in JsonController
*   [TASK] Set closed as default value for opening hours in TCA
*   [TASK] Migrate Geo command from CommandController to Symfony Command
*   [TASK] Add a configuration check in AddressController
*   [TASK] Add translation How To documentation
*   [TASK] Add detail page uid in address item links
*   [FEATURE] Add slug field for address group - address group teaser
*   [TASK] Add mapping for creation and modification date in address model
*   [TASK] Remove remaining editInModal for backend address list
*   [TASK] Add missing labels for search box for backend address list
*   [BUGFIX] Remove backend address list editinmodal
*   [TASK] Add search box for backend address list
*   [FEATURE] Add display type in address list - map-only and filter-only now possible
*   [FEATURE] Add opening hours for each day in address
*   [FEATURE] Add additional files in address
*   [TASK] TCA refactoring
*   [FEATURE] Add frontend user selection for address ownership
*   [FEATURE] Add show-in-preview checkbox for address images
*   [FEATURE] Add frontend management for addresses
*   [FEATURE] Add social media links for Facebook, Instagram, Twitter
*   [FEATURE] Add meta tags, HTML title and open graph tags
*   [FEATURE] Add address group teaser plugin
*   [FEATURE] Add variant select for address groups
*   [BUGFIX] Fix google map pin in single view



## 2020-03-26  Release of version 1.15.0

*	[BUGFIX] Fix query for selecting address select relations on third level (JSON-API)
*	[BUGFIX] Fix usage of storage pid in JsonController - ensure that result items contained in selected storage
*	[TASK] Make jQuery selectors in JavaScript library more precise
*	[TASK] Translation of documentation
*	[TASK] Preparation for documentation translation



## 2020-01-09  Release of version 1.14.0

*	[FEATURE] Add hide/show buttons in backend module
*	[TASK] Add german translations



## 2019-11-20  Release of version 1.13.0

*	[FEATURE] Add another sort by option for ordering by lastname/firstname



## 2019-11-20  Release of version 1.12.1

*	[BUGFIX] Load google maps features only when google maps libraries are available



## 2019-11-15  Release of version 1.12.0

*	[FEATURE] Add backend field and configuration for slugs
*	[TASK] TCA refactoring
*	[TASK] Optimize backend module controller.



## 2019-10-26  Release of version 1.11.0

*	[FEATURE] Add page/user TypoScript authorization options for backend module (create, view or delete records).



## 2019-10-23  Release of version 1.10.0

*	[BUGFIX] Fix storage page for backend module.
*	[FEATURE] Add option for enabling refreshing on enter in search word field.



## 2019-10-21  Release of version 1.9.0

*	[FEATURE] Add backend module for managing records.



## 2019-10-16  Release of version 1.8.2

*	[BUGFIX] Fix version number.


## 2019-10-16  Release of version 1.8.1

*	[TASK] Add attribute17 for one more custom field.



## 2019-10-12  Release of version 1.8.0

*	[BUGFIX] Fix JSON request with negative geo coordinates.
*	[BUGFIX] Modify TCA and MySQL table definition for MySQL strict mode.
*	[FEATURE] Add setting for AJAX result order.
*	[FEATURE] Add ViewHelper for loading addresses by uid or name.
*	[FEATURE] Add Scheduler task for fetching geo coordinates.
*	[BUGFIX] Remove unit px from TypoScript constant definitions.
*	[BUGFIX] Fix image size assignment in detail/show view.
*	[TASK] Optimize image sizes and responsive behaviour.
*	[TASK] Add additional attributes fields for custom data.
*	[FEATURE] Add filter for a custom string. The search field in database is configurable by TypoScript.
*	[FEATURE] Add a data attribute data-hidden-class for loading info. The value contains the CSS class, which is added/removed by switching the loading info.
*	[FEATURE] Add TypoScript setting for optional submitting location filter on enter.
*	[TASK] Sort relations in address record relations alphabetical.



## 2019-05-21  Release of version 1.7.2

*	[BUGFIX] Fix translation handling for JSON API.



## 2019-05-20  Release of version 1.7.1

*	[BUGFIX] Fix displaying HTML description in single view.
*	[BUGFIX] Fix translation handling for JSON API.



## 2019-03-16  Release of version 1.7.0

*   [TASK] Remove inject annotations.
*   [TASK] Update support service.
*   [FEATURE] Add Gitlab-CI configuration.
*   [BUGFIX] Fixing JSON-API when there are no select filter available.
*	[BUGFIX] Removing DEV identifier.
*   [BUGFIX] Adding a base64 encode on description which is passed by JSON data.
*   [BUGFIX] Removing descriptions from JSON data.
*   [BUGFIX] Fixing TCA configuration for using MySQL 5.7 in strict mode.
*   [TASK] Adding a new TypoScript constant for adding a country restriction in Google-Maps auto completer.
*   [TASK] Adding content object data for fluid.
*   [TASK] TypoScript view constants cleanup.



## 2019-01-13  Release of version 1.6.0

*   [TASK] Refactoring of backend JavaScripts.
*   [TASK] Refactoring of database queries.
*   [TASK] Rising PHP  version up to 7.1.
*   [TASK] Rising TYPO3 version up to 8.7 to 9.5.



## 2018-12-10  Release of version 1.5.0

*   [FEATURE] Changing Update-Service by Support-Service.
*   [BUGFIX] Adding validation for markers in Address model in order to avoid invalid positions.
*   [BUGFIX] Fixing database definition file. The distance field must be available in database table, otherwise the core migration wizard will fail.
*   [BUGFIX] Fixing validation of list limit in JSON controller.
*   [FEATURE] Adding a JavaScript method for resetting all filter and data. This is useful for reset buttons.
*   [BUGFIX] Adding validation for markers in order to avoid invalid positions.



## 2018-10-25  Release of version 1.4.2

*   [BUGFIX] Adding missed Fluid namespace in list item partial.
*   [TASK] Adding a migration guide file and optimizing documentation.
*   [TASK] Adding a default value for getMapMarker in Address model.



## 2018-10-24  Release of version 1.4.1

*   [BUGFIX] Fixing sorting by distance in JSON controller.



## 2018-10-11  Release of version 1.4.0

*   [TASK] Removing color of map marker icon.
*   [FEATURE] Adding definition and selection of map marker icons on each address.



## 2018-09-25  Release of version 1.3.0

*   [FEATURE] Adding the possibility to display the distance, in case of displaying items from location search result.
*   [TASK] Search results, which are searched by location, are now always ordered by distance.
*   [TASK] Adding a TypoScript constant for Google-Maps libraries parameter.



## 2018-08-22  Release of version 1.2.0

*   [BUGFIX] Fixing JavaScript conditions on marker locations.
*   [FEATURE] Adding on enter feature for location search.
*   [FEATURE] Adding optionally google maps marker clustering.
*   [FEATURE] Adding optionally static template for non TYPO3-Themes user.
*   [TASK] Implement offset in fetching initial data.
*   [TASK] TypoScript constants refactoring.
*   [FEATURE] Implement a JavaScript based pagination for list view.
*   [FEATURE] Implement a radial search for addresses.
*   [FEATURE] Adding possibility of configurable sorting for Group, Organisation and Position selectbox entries.
*   [BUGFIX] Removing double used palette in Address TCA.
*   [BUGFIX] Fixing Google Map Tooltip reinitialize issue.
*   [BUGFIX] Fixing filter select box reinitialize issue.
*   [TASK] Modifying filter submit, so that it is optional selectable.
*   [BUGFIX] Fixing a Map initialization issue in TYPO3 7.6



## 2017-11-27  Release of version 1.1.2

*   [TASK] Adding a message in backend, if no Google-Maps API-Key is available
*   [BUGFIX] Fixing a Map initialization issue



## 2017-11-26  Release of version 1.1.1

*   [FEATURE] Adding license key



## 2017-11-26  Release of version 1.1.0

*   [FEATURE] Adding wizard for getting geo locations
*   [FEATURE] Adding Variable ViewHelpers
*   [TASK] Adding TCA palettes
*   [TASK] Cleaning up
*   [BUGFIX] Fixing JavaScript merge/concat issue for Google-Libraries
*   [BUGFIX] Fixing getTemplatePath Utility issue
*   [BUGFIX] Fixing an JavaScript reset select box issues
*   [BUGFIX] Fixing repository method, when there're noh relations found
