# TYPO3 Address manager

The address manager extension allows you to manage and display addresses respectively person records.

## Features (Basic version)
* Lots of data fields are available, beside standard fields like name and contact information the extension
  provides special fields for SEO, social media, geo coordinates and opening hours.
* Images and attachments can be used.
* Addresses may be structured using groups, organisations and positions.
* A fulltext search is available.
* A multi-step filter allows you to filter addresses in the frontend.
* Smooth presentation of lists and filters with AJAX.
* Addresses may be presented on a map (Google Maps).
* A viewhelper allows to display addresses in other fluid templates (like third-party-extensions or page templates).
* Geo coordinates can be determined in the TYPO3 backend when addresses are edited ("map wizard").

## Features (Pro version)
* A special backend module allows a user-friendly management of addresses and their relations.
* A frontend mangement function allows to edit addresses in the frontend.
* A scheduler task to fetch geo coordinates automatically is included.
* A radius search is included.
