plugin.tx_addressmanager {
	view {
		templateRootPaths {
			100 = EXT:modules/Resources/Private/Templates/
			200 = EXT:address_manager/Resources/Private/Templates/
			300 = {$themes.resourcesPrivatePath}Extensions/AddressManager/Templates/
			400 = {$themes.configuration.extension.address_manager.view.templateRootPath}
		}
		partialRootPaths {
			100 = EXT:modules/Resources/Private/Partials/
			200 = EXT:address_manager/Resources/Private/Partials/
			300 = {$themes.resourcesPrivatePath}Extensions/AddressManager/Partials/
			400 = {$themes.configuration.extension.address_manager.view.partialRootPath}
		}
		layoutRootPaths {
			#100 = EXT:modules/Resources/Private/Layouts/
			200 = EXT:address_manager/Resources/Private/Layouts/
			300 = {$themes.resourcesPrivatePath}Extensions/AddressManager/Layouts/
			400 = {$themes.configuration.extension.address_manager.view.layoutRootPath}
		}
	}
	persistence {
		storagePid = {$themes.configuration.container.address}
	}
	features {
		# uncomment the following line to enable the new Property Mapper.
		# rewrittenPropertyMapper = 1
		# Skip default arguments from urls
		skipDefaultArguments = 0
		# Disable chash
		requireCHashArgumentForActionArguments = 1
	}
	settings {
		# Identify the framework (like Bootstrap, Bootstrap4)
		framework = {$themes.framework}
		# Debug extension
		debug = 0
		siteName = {$themes.configuration.siteName}
		# List settings
		list {
			defaultPid = {$themes.configuration.pages.address.list}
			image {
				width = {$themes.configuration.extension.address_manager.list.image.width}
				height = {$themes.configuration.extension.address_manager.list.image.height}
			}
			allowedLimits {
				15 = 15
				25 = 25
				50 = 50
			}
			allowedDistances {
				10 = 10
				25 = 25
				50 = 50
			}
			group {
				# Default sorting for groups
				sortBy = {$themes.configuration.extension.address_manager.list.group.sortBy}
				sortOrder = {$themes.configuration.extension.address_manager.list.group.sortOrder}
			}
			organisation {
				# Default sorting for organisations
				sortBy = {$themes.configuration.extension.address_manager.list.organisation.sortBy}
				sortOrder = {$themes.configuration.extension.address_manager.list.organisation.sortOrder}
			}
			position {
				# Default sorting for positions
				sortBy = {$themes.configuration.extension.address_manager.list.position.sortBy}
				sortOrder = {$themes.configuration.extension.address_manager.list.position.sortOrder}
			}
			location {
				submitOnEnter = {$themes.configuration.extension.address_manager.list.location.submitOnEnter}
			}
			searchWord {
				submitOnEnter = {$themes.configuration.extension.address_manager.list.searchWord.submitOnEnter}
			}
			customString {
				# Fields to search in, you can define multiple fields comma separated
				field = {$themes.configuration.extension.address_manager.list.customString.field}
			}
			# Possible values: distance, name
			orderForAjaxResult = {$themes.configuration.extension.address_manager.list.orderForAjaxResult}
			map {
				# Country restriction
				countryRestriction = {$themes.configuration.extension.address_manager.list.map.countryRestriction}
				# Maximum zoom, which is used by setting the viewport to the marker bounds
				maxZoom = {$themes.configuration.extension.address_manager.list.map.maxZoom}
				# Marker clustering configuration
				clustering {
					# Activate/deactivate clustering
					active = {$themes.configuration.extension.address_manager.list.map.clustering.active}
					# Path for clustering images
					imagePath = {$themes.configuration.extension.address_manager.list.map.clustering.imagePath}
				}
				# Options for data-style:
				# primaryColored: Colored in theme primary color, must be in <body data-color-primary="#ED3E49" ..>
				# normal: Googles default colors
				# grayScaled: Gray scaled
				style = {$themes.configuration.extension.address_manager.list.map.style}
				marker {
					width = 32
					height = 32
					style {
						default (
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f00" d="M12 0c-4.198 0-8 3.403-8 7.602 0 6.243 6.377 6.903 8 16.398 1.623-9.495 8-10.155 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.342-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
						)
						pinThin (
							<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path fill="#0f0" d="M12 10c-1.104 0-2-.896-2-2s.896-2 2-2 2 .896 2 2-.896 2-2 2m0-5c-1.657 0-3 1.343-3 3s1.343 3 3 3 3-1.343 3-3-1.343-3-3-3m-7 2.602c0-3.517 3.271-6.602 7-6.602s7 3.085 7 6.602c0 3.455-2.563 7.543-7 14.527-4.489-7.073-7-11.072-7-14.527m7-7.602c-4.198 0-8 3.403-8 7.602 0 4.198 3.469 9.21 8 16.398 4.531-7.188 8-12.2 8-16.398 0-4.199-3.801-7.602-8-7.602"/></svg>
						)
					}
				}
			}
		}
		# Detail settings
		detail {
			defaultPid = {$themes.configuration.pages.address.detail}
			image {
				width = {$themes.configuration.extension.address_manager.detail.image.width}
				height = {$themes.configuration.extension.address_manager.detail.image.height}
			}
			map {
				zoom = {$themes.configuration.extension.address_manager.detail.map.zoom}
				# Options for data-style:
				# primaryColored: Colored in theme primary color, must be in <body data-color-primary="#ED3E49" ..>
				# normal: Googles default colors
				# grayScaled: Gray scaled
				style = {$themes.configuration.extension.address_manager.detail.map.style}
				marker {
					width = 32
					height = 32
					style {
						default (
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f00" d="M12 0c-4.198 0-8 3.403-8 7.602 0 6.243 6.377 6.903 8 16.398 1.623-9.495 8-10.155 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.342-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
						)
						pinThin (
							<svg width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd"><path fill="#0f0" d="M12 10c-1.104 0-2-.896-2-2s.896-2 2-2 2 .896 2 2-.896 2-2 2m0-5c-1.657 0-3 1.343-3 3s1.343 3 3 3 3-1.343 3-3-1.343-3-3-3m-7 2.602c0-3.517 3.271-6.602 7-6.602s7 3.085 7 6.602c0 3.455-2.563 7.543-7 14.527-4.489-7.073-7-11.072-7-14.527m7-7.602c-4.198 0-8 3.403-8 7.602 0 4.198 3.469 9.21 8 16.398 4.531-7.188 8-12.2 8-16.398 0-4.199-3.801-7.602-8-7.602"/></svg>
						)
					}
				}
			}
		}
	}
}

config.pageTitleProviders {
	address_manager {
		provider = CodingMs\AddressManager\PageTitle\PageTitleProvider
		before = seo
	}
}

# Google-Maps and -marker. We store this in a lib object and decide later if we load the scripts or not
lib.googleMaps {
	includeJSFooterlibs {
		googleMaps = https://maps.googleapis.com/maps/api/js?key={$themes.configuration.javascript.google.maps.apiKey}&libraries=places
		googleMaps {
			external = 1
			forceOnTop = 1
			disableCompression = 1
			excludeFromConcatenation = 1
		}
		googleMapsHtmlMarker = EXT:address_manager/Resources/Public/JavaScript/HtmlMarker.js
		googleMapsHtmlMarker {
			external = 0
			disableCompression = 0
			excludeFromConcatenation = 0
		}
		googleMapsClustering = https://cdnjs.cloudflare.com/ajax/libs/js-marker-clusterer/1.0.0/markerclusterer.js
		googleMapsClustering {
			external = 1
			type = text/javascript
			disableCompression = 1
			excludeFromConcatenation = 1
		}
	}
	includeJSFooter {
		googleMaps = EXT:address_manager/Resources/Public/JavaScript/GoogleMaps.js
		googleMaps {
			external = 0
			disableCompression = 0
			excludeFromConcatenation = 0
		}
	}
}

# The Google Maps consent button should to be displayed
["{$themes.configuration.extension.address_manager.googleMapsConsent.active}" == "1"]
page {
	includeJSFooter {
		googleMapsAllowed = EXT:address_manager/Resources/Public/JavaScript/GoogleMapsAllowed.js
		googleMapsAllowed {
			external = 0
			forceOnTop = 0
			disableCompression = 0
			excludeFromConcatenation = 0
		}
	}
}
[ELSE]
# The Google Maps consent button should to be displayed, so include Google Maps
page.includeJSFooterlibs.googleMaps < lib.googleMaps.includeJSFooterlibs.googleMaps
page.includeJSFooterlibs.googleMapsHtmlMarker < lib.googleMaps.includeJSFooterlibs.googleMapsHtmlMarker
page.includeJSFooterlibs.googleMapsClustering  < lib.googleMaps.includeJSFooterlibs.googleMapsClustering
page.includeJSFooter.googleMaps < lib.googleMaps.includeJSFooter.googleMaps
[GLOBAL]

# If Google Maps consent is given by user
[request.getCookieParams() ['googleMapsAllowed'] == 1]
page.includeJSFooterlibs.googleMaps < lib.googleMaps.includeJSFooterlibs.googleMaps
page.includeJSFooterlibs.googleMapsHtmlMarker < lib.googleMaps.includeJSFooterlibs.googleMapsHtmlMarker
page.includeJSFooterlibs.googleMapsClustering  < lib.googleMaps.includeJSFooterlibs.googleMapsClustering
page.includeJSFooter.googleMaps < lib.googleMaps.includeJSFooter.googleMaps
[GLOBAL]

# Default Javascript
page {
	includeJSFooter {
		addressmanager = EXT:address_manager/Resources/Public/JavaScript/AddressManager.js
		addressmanager {
			external = 0
			disableCompression = 0
			excludeFromConcatenation = 0
		}
		addressManagerGetGeoLocation = EXT:address_manager/Resources/Public/JavaScript/AddressManagerGetGeoLocation.js
		addressManagerGetGeoLocation {
			external = 0
			disableCompression = 0
			excludeFromConcatenation = 0
		}
	}
}

address_json_api = PAGE
address_json_api {
	config {
		disableAllHeaderCode = 1
		additionalHeaders {
			10.header = Content-type:application/json;charset=utf-8
		}
		debug = 0
		no_cache = 1
		metaCharset = utf-8
	}
	typeNum = 1452982642
	10 < tt_content.list.20.addressmanager_jsonapi
}
[getTSFE().type == 1452982642]
	config.sourceopt.enabled = 0
[GLOBAL]
