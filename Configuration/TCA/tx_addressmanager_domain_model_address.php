<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$extKey = 'address_manager';
$table = 'tx_addressmanager_domain_model_address';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$GLOBALS['TCA']['tx_addressmanager_domain_model_address'] = [
    'ctrl' => [
        'title' => $lll,
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'sortby' => 'sorting',
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'name,gender,first_name,middle_name,last_name,birthday,title,email,phone,mobile,www,address,building,room,company,city,postal_code,region,country,images,fax,description,map_latitude,map_longitude,map_zoom,map_tooltip,map_link,directions,groups,positions,organisations,',
        'iconfile' => 'EXT:address_manager/Resources/Public/Icons/iconmonstr-id-card-8.svg',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-addressmanager-address']
    ],
    'types' => [
        '1' => [
            'showitem' => 'information, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource,
                slug,
                --palette--;' . $lll . '.palette_gender_title_name;gender_title_name,
                --palette--;' . $lll . '.palette_first_middle_last_name;first_middle_last_name,
                --palette--;' . $lll . '.palette_email_www_birthday;email_www_birthday,
                --palette--;' . $lll . '.palette_company_room_building;company_room_building,
                --palette--;' . $lll . '.palette_address_postal_code_city;address_postal_code_city,
                --palette--;' . $lll . '.palette_region_country;region_country,
                --palette--;' . $lll . '.palette_phone_fax_mobile;phone_fax_mobile,
                --palette--;' . $lll . '.palette_facebook_instagram_twitter_youtube;facebook_instagram_twitter_youtube,
                description,
                frontend_user,


            --div--;' . $lll . '.tab_seo,
                html_title,
                meta_abstract,
                meta_description,
                meta_keywords,

            --div--;' . $lll . '.tab_files,
                images,
                files,

            --div--;' . $lll . '.tab_location,
                --palette--;' . $lll . '.palette_map_latitude_longitude_zoom;map_latitude_longitude_zoom,
                map_wizard,
                --palette--;' . $lll . '.palette_map_marker_link_tooltip;map_marker_link_tooltip,
                directions,
                --palette--;' . $lll . '.palette_opening_hours;opening_hours,

            --div--;' . $lll . '.tab_relations,
                groups,
                positions,
                organisations,
                addresses,

            --div--;' . $lll . '.tab_access,
                hidden;;1,
                starttime,
                endtime'
        ],
    ],
    'palettes' => [
        'gender_title_name' => ['showitem' => 'gender, title, name'],
        'first_middle_last_name' => ['showitem' => 'first_name, middle_name, last_name'],
        'phone_fax_mobile' => ['showitem' => 'phone, fax, mobile'],
        'email_www_birthday' => ['showitem' => 'email, www, birthday'],
        'facebook_instagram_twitter_youtube' => ['showitem' => 'facebook, instagram, twitter, youtube'],
        'company_room_building' => ['showitem' => 'company, room, building'],
        'address_postal_code_city' => ['showitem' => 'address, postal_code, city'],
        'region_country' => ['showitem' => 'region, country'],
        'map_latitude_longitude_zoom' => ['showitem' => 'map_latitude, map_longitude, map_zoom'],
        'map_marker_link_tooltip' => ['showitem' => 'map_marker, map_link, map_tooltip'],
        'opening_hours' => ['showitem' => 'opening_hours_monday_opens, opening_hours_monday_closes, --linebreak--, opening_hours_tuesday_opens, opening_hours_tuesday_closes, --linebreak--, opening_hours_wednesday_opens, opening_hours_wednesday_closes, --linebreak--, opening_hours_thursday_opens, opening_hours_thursday_closes, --linebreak--, opening_hours_friday_opens, opening_hours_friday_closes, --linebreak--, opening_hours_saturday_opens, opening_hours_saturday_closes, --linebreak--, opening_hours_sunday_opens, opening_hours_sunday_closes'],
    ],
    'columns' => [
        'sys_language_uid' => \CodingMs\AddressManager\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AddressManager\Tca\Configuration::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\AddressManager\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AddressManager\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AddressManager\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\AddressManager\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\AddressManager\Tca\Configuration::full('endtime'),
        'information' => \CodingMs\AddressManager\Tca\Configuration::full('information', $table, $extKey),

        'deleted' => [
            'exclude' => false,
            'label' => 'deleted',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'Record is deleted'
                    ]
                ],
            ],
        ],
        'crdate' => [
            'exclude' => 0,
            'label' => $lll . '.crdate',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'readOnly' => 1,
            ],
        ],
        'tstamp' => [
            'exclude' => 0,
            'label' => $lll . '.tstamp',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'max' => 20,
                'eval' => 'datetime',
                'checkbox' => 0,
                'readOnly' => 1,
            ],
        ],

        'name' => [
            'exclude' => 1,
            'label' => $lll . '.name',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string', true),
        ],
        'slug' => [
            'exclude' => 1,
            'label' => $lll . '.slug',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('slug', true, false, '', ['field' => 'name']),
        ],
        'gender' => [
            'exclude' => 1,
            'label' => $lll . '.gender',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'first_name' => [
            'exclude' => 1,
            'label' => $lll . '.first_name',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'middle_name' => [
            'exclude' => 1,
            'label' => $lll . '.middle_name',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'last_name' => [
            'exclude' => 1,
            'label' => $lll . '.last_name',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'birthday' => [
            'exclude' => 1,
            'label' => $lll . '.birthday',
            'config' => \CodingMs\AdditionalTca\Tca\Configuration::get('date', false, false, '', ['dbType' => 'timestamp']),
        ],
        'title' => [
            'exclude' => 1,
            'label' => $lll . '.title',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'email' => [
            'exclude' => 1,
            'label' => $lll . '.email',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'phone' => [
            'exclude' => 1,
            'label' => $lll . '.phone',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'mobile' => [
            'exclude' => 1,
            'label' => $lll . '.mobile',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'www' => [
            'exclude' => 1,
            'label' => $lll . '.www',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'address' => [
            'exclude' => 1,
            'label' => $lll . '.address',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'building' => [
            'exclude' => 1,
            'label' => $lll . '.building',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'room' => [
            'exclude' => 1,
            'label' => $lll . '.room',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'company' => [
            'exclude' => 1,
            'label' => $lll . '.company',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'city' => [
            'exclude' => 1,
            'label' => $lll . '.city',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'postal_code' => [
            'exclude' => 1,
            'label' => $lll . '.postal_code',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'region' => [
            'exclude' => 1,
            'label' => $lll . '.region',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'country' => [
            'exclude' => 1,
            'label' => $lll . '.country',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'images' => [
            'exclude' => 1,
            'label' => $lll . '.images',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'images',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => $lll . '.images_add',
                        'showPossibleLocalizationRecords' => true,
                        'showRemovedLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'images',
                        'tablenames' => 'tx_addressmanager_domain_model_address',
                        'table_local' => 'sys_file',
                    ],
                    // custom configuration for displaying fields in the overlay/reference table
                    // to use the newsPalette and imageoverlayPalette instead of the basicoverlayPalette
                    'overrideChildTca' => [
                        'types' => [
                            '0' => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;addressManagerAddressPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                            \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                                'showitem' => '
                                    --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;addressManagerAddressPalette,
                                    --palette--;;imageoverlayPalette,
                                    --palette--;;filePalette'
                            ],
                        ]
                    ]
                ],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext']
            )
        ],
        'files' => [
            'exclude' => 1,
            'label' => $lll . '.files',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'files',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => $lll . '.files_add',
                        'showPossibleLocalizationRecords' => true,
                        'showRemovedLocalizationRecords' => true,
                        'showAllLocalizationLink' => true,
                        'showSynchronizationLink' => true
                    ],
                    'foreign_match_fields' => [
                        'fieldname' => 'files',
                        'tablenames' => 'tx_addressmanager_domain_model_address',
                        'table_local' => 'sys_file',
                    ]
                ],
                $GLOBALS['TYPO3_CONF_VARS']['SYS']['mediafile_ext']
            )
        ],
        'fax' => [
            'exclude' => 1,
            'label' => $lll . '.fax',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'facebook' => [
            'exclude' => 1,
            'label' => $lll . '.facebook',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'instagram' => [
            'exclude' => 1,
            'label' => $lll . '.instagram',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'twitter' => [
            'exclude' => 1,
            'label' => $lll . '.twitter',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'youtube' => [
            'exclude' => 1,
            'label' => $lll . '.youtube',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'description' => [
            'exclude' => 1,
            'label' => $lll . '.description',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'html_title' => [
            'exclude' => 0,
            'label' => $lll . '.html_title',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'meta_abstract' => [
            'exclude' => 0,
            'label' => $lll . '.meta_abstract',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('textareaSmall'),
        ],
        'meta_description' => [
            'exclude' => 0,
            'label' => $lll . '.meta_description',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('textareaSmall'),
        ],
        'meta_keywords' => [
            'exclude' => 0,
            'label' => $lll . '.meta_keywords',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'map_latitude' => [
            'exclude' => 1,
            'label' => $lll . '.map_latitude',
            'config' => [
                'type' => 'input',
                'size' => 15,
                'eval' => 'trim'
            ],
        ],
        'map_longitude' => [
            'exclude' => 1,
            'label' => $lll . '.map_longitude',
            'config' => [
                'type' => 'input',
                'size' => 15,
                'eval' => 'trim'
            ],
        ],
        'map_wizard' => [
            'exclude' => 1,
            'label' => $lll . '.map_wizard',
            'config' => [
                'type' => 'user',
                'renderType' => 'geoLocationWizard',
            ]
        ],
        'map_zoom' => [
            'exclude' => 1,
            'label' => $lll . '.map_zoom',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => '8',
                'items' => [
                    ['0', '0'],
                    ['1', '1'],
                    ['2', '2'],
                    ['3', '3'],
                    ['4', '4'],
                    ['5', '5'],
                    ['6', '6'],
                    ['7', '7'],
                    ['8', '8'],
                    ['9', '9'],
                    ['10', '10'],
                    ['11', '11'],
                    ['12', '12'],
                    ['13', '13'],
                    ['14', '14'],
                    ['15', '15'],
                    ['16', '16'],
                    ['17', '17'],
                    ['18', '18'],
                    ['19', '19'],
                    ['20', '20'],
                    ['21', '21'],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => 'required,int'
            ]
        ],
        'map_tooltip' => [
            'exclude' => 1,
            'label' => $lll . '.map_tooltip',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'map_link' => [
            'exclude' => 1,
            'label' => $lll . '.map_link',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string'),
        ],
        'map_marker' => [
            'exclude' => 1,
            'label' => $lll . '.map_marker',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 'default',
                'items' => [
                    ['Default', 'default'],
                    ['Pin thin', 'pinThin'],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => 'required'
            ]
        ],
        // Only for internal usage.
        // Otherwise the distance wont be mapped into the Model!
        'distance' => [
            'exclude' => 1,
            'label' => 'Distance !!!internal only!!!',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string', true),
        ],
        'opening_hours_monday_opens' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_monday_opens',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_monday_closes' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_monday_closes',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_tuesday_opens' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_tuesday_opens',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_tuesday_closes' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_tuesday_closes',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_wednesday_opens' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_wednesday_opens',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_wednesday_closes' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_wednesday_closes',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_thursday_opens' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_thursday_opens',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_thursday_closes' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_thursday_closes',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_friday_opens' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_friday_opens',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_friday_closes' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_friday_closes',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_saturday_opens' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_saturday_opens',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_saturday_closes' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_saturday_closes',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_sunday_opens' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_sunday_opens',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'opening_hours_sunday_closes' => [
            'exclude' => 1,
            'label' => $lll . '.opening_hours_sunday_closes',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('openingHours', true),
        ],
        'directions' => [
            'exclude' => 1,
            'label' => $lll . '.directions',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'groups' => [
            'exclude' => 1,
            'label' => $lll . '.groups',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_addressmanager_domain_model_addressgroup',
                'foreign_table_where' => 'AND tx_addressmanager_domain_model_addressgroup.pid=###CURRENT_PID### AND tx_addressmanager_domain_model_addressgroup.sys_language_uid IN (-1,0) ORDER BY tx_addressmanager_domain_model_addressgroup.title ASC',
                'MM' => 'tx_addressmanager_address_addressgroup_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'minitems' => 0,
                'maxitems' => 9999,
                'multiple' => false,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        // TYPO3 6.x style
                        'script' => 'wizard_edit.php',
                        // TYPO3 7.x style
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'edit2.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'add.gif',
                        'params' => [
                            'table' => 'tx_addressmanager_domain_model_addressgroup',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        // TYPO3 6.x style
                        'script' => 'wizard_add.php',
                        // TYPO3 7.x style
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
        'positions' => [
            'exclude' => 1,
            'label' => $lll . '.positions',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_addressmanager_domain_model_addressposition',
                'foreign_table_where' => 'AND tx_addressmanager_domain_model_addressposition.pid=###CURRENT_PID### AND tx_addressmanager_domain_model_addressposition.sys_language_uid IN (-1,0) ORDER BY tx_addressmanager_domain_model_addressposition.title ASC',
                'MM' => 'tx_addressmanager_address_addressposition_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'minitems' => 0,
                'maxitems' => 9999,
                'multiple' => false,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        // TYPO3 6.x style
                        'script' => 'wizard_edit.php',
                        // TYPO3 7.x style
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'edit2.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'add.gif',
                        'params' => [
                            'table' => 'tx_addressmanager_domain_model_addressposition',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        // TYPO3 6.x style
                        'script' => 'wizard_add.php',
                        // TYPO3 7.x style
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
        'organisations' => [
            'exclude' => 1,
            'label' => $lll . '.organisations',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_addressmanager_domain_model_addressorganisation',
                'foreign_table_where' => 'AND tx_addressmanager_domain_model_addressorganisation.pid=###CURRENT_PID### AND tx_addressmanager_domain_model_addressorganisation.sys_language_uid IN (-1,0) ORDER BY tx_addressmanager_domain_model_addressorganisation.title ASC',
                'MM' => 'tx_addressmanager_address_addressorganisation_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'minitems' => 0,
                'maxitems' => 9999,
                'multiple' => false,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        // TYPO3 6.x style
                        'script' => 'wizard_edit.php',
                        // TYPO3 7.x style
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'edit2.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'add.gif',
                        'params' => [
                            'table' => 'tx_addressmanager_domain_model_addressorganisation',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        // TYPO3 6.x style
                        'script' => 'wizard_add.php',
                        // TYPO3 7.x style
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
        'frontend_user' => [
            'exclude' => 0,
            'label' => $lll . '.frontend_user',
            'config' => [
                'type' => 'group',
                'internal_type' => 'db',
                'allowed' => 'fe_users',
                'foreign_table' => 'fe_users',
                'size' => '1',
                'maxitems' => '1',
                'minitems' => '0',
                'eval' => 'int',
                'default' => 0,
            ],
        ],
        'addresses' => [
            'exclude' => 1,
            'label' => $lll . '.addresses',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_addressmanager_domain_model_address',
                'foreign_table_where' => 'AND tx_addressmanager_domain_model_address.uid <> ###THIS_UID### AND tx_addressmanager_domain_model_address.pid = ###CURRENT_PID### AND tx_addressmanager_domain_model_address.sys_language_uid IN (-1,0) ORDER BY tx_addressmanager_domain_model_address.name ASC',
                'MM' => 'tx_addressmanager_address_address_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'minitems' => 0,
                'maxitems' => 9999,
                'multiple' => false,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'type' => 'popup',
                        'title' => 'Edit',
                        // TYPO3 6.x style
                        'script' => 'wizard_edit.php',
                        // TYPO3 7.x style
                        'module' => [
                            'name' => 'wizard_edit'
                        ],
                        'icon' => 'edit2.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'type' => 'script',
                        'title' => 'Create new',
                        'icon' => 'add.gif',
                        'params' => [
                            'table' => 'tx_addressmanager_domain_model_address',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                        // TYPO3 6.x style
                        'script' => 'wizard_add.php',
                        // TYPO3 7.x style
                        'module' => [
                            'name' => 'wizard_add'
                        ],
                    ],
                ],
            ],
        ],
    ],
];

// 7.6.x fixes
if ((int)TYPO3_version >= 7) {
    $GLOBALS['TCA']['tx_addressmanager_domain_model_address']['columns']['groups']['config']['wizards']['edit']['icon'] =
        'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif';
    $GLOBALS['TCA']['tx_addressmanager_domain_model_address']['columns']['groups']['config']['wizards']['add']['icon'] =
        'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif';
    $GLOBALS['TCA']['tx_addressmanager_domain_model_address']['columns']['positions']['config']['wizards']['edit']['icon'] =
        'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif';
    $GLOBALS['TCA']['tx_addressmanager_domain_model_address']['columns']['positions']['config']['wizards']['add']['icon'] =
        'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif';
    $GLOBALS['TCA']['tx_addressmanager_domain_model_address']['columns']['organisations']['config']['wizards']['edit']['icon'] =
        'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif';
    $GLOBALS['TCA']['tx_addressmanager_domain_model_address']['columns']['organisations']['config']['wizards']['add']['icon'] =
        'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif';
    $GLOBALS['TCA']['tx_addressmanager_domain_model_address']['columns']['description']['config']['wizards']['RTE']['icon'] =
        'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_rte.gif';
    $GLOBALS['TCA']['tx_addressmanager_domain_model_address']['columns']['directions']['config']['wizards']['RTE']['icon'] =
        'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_rte.gif';
}

return $GLOBALS['TCA']['tx_addressmanager_domain_model_address'];
