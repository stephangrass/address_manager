<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$extKey = 'address_manager';
$table = 'tx_addressmanager_domain_model_addressgroup';
$lll = 'LLL:EXT:' . $extKey . '/Resources/Private/Language/locallang_db.xlf:' . $table;

$GLOBALS['TCA']['tx_addressmanager_domain_model_addressgroup'] = [
    'ctrl' => [
        'title' => $lll,
        'label_alt' => 'sys_language_uid',
        'label_alt_force' => true,
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => true,
        'sortby' => 'sorting',
        'versioningWS' => 2,
        'versioning_followPages' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,description,',
        'iconfile' => 'EXT:address_manager/Resources/Public/Icons/iconmonstr-link-2.svg',
        'typeicon_classes' => ['default' => 'mimetypes-x-content-addressmanager-addressgroup']
    ],
    'types' => [
        '1' => ['showitem' => 'information, sys_language_uid;;;;1-1-1, l10n_parent, l10n_diffsource, hidden;;1, title, slug, description;;;richtext:rte_transform[mode=ts_links], variant'],
    ],
    'palettes' => [
        '1' => ['showitem' => ''],
    ],
    'columns' => [
        'sys_language_uid' => \CodingMs\AddressManager\Tca\Configuration::full('sys_language_uid'),
        'l10n_parent' => \CodingMs\AddressManager\Tca\Configuration::full('l10n_parent', $table, $extKey),
        'l10n_diffsource' => \CodingMs\AddressManager\Tca\Configuration::full('l10n_diffsource'),
        't3ver_label' => \CodingMs\AddressManager\Tca\Configuration::full('t3ver_label'),
        'hidden' => \CodingMs\AddressManager\Tca\Configuration::full('hidden'),
        'starttime' => \CodingMs\AddressManager\Tca\Configuration::full('starttime'),
        'endtime' => \CodingMs\AddressManager\Tca\Configuration::full('endtime'),
        'information' => \CodingMs\AddressManager\Tca\Configuration::full('information', $table, $extKey),
        'title' => [
            'exclude' => 1,
            'label' => $lll . '.title',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('string', true),
        ],
        'slug' => [
            'exclude' => 1,
            'label' => $lll . '.slug',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('slug', true),
        ],
        'description' => [
            'exclude' => 1,
            'label' => $lll . '.description',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('rte'),
            'defaultExtras' => 'richtext:rte_transform[flag=rte_enabled|mode=ts]',
        ],
        'variant' => [
            'exclude' => 0,
            'label' => $lll . '.variant',
            'config' => \CodingMs\AddressManager\Tca\Configuration::get('groupVariant'),
        ],
    ],
];

return $GLOBALS['TCA']['tx_addressmanager_domain_model_addressgroup'];
