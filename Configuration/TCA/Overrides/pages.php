<?php
defined('TYPO3_MODE') or die();

$GLOBALS['TCA']['pages']['columns']['module']['config']['items'][] = [
    0 => 'LLL:EXT:address_manager/Resources/Private/Language/locallang_db.xlf:tx_addressmanager_label.contains_addresses',
    1 => 'addresses',
    2 => 'apps-pagetree-folder-contains-addresses'
];
$GLOBALS['TCA']['pages']['ctrl']['typeicon_classes']['contains-addresses'] = 'apps-pagetree-folder-contains-addresses';
