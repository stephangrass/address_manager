<?php
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('address_manager', 'Configuration/TypoScript', 'Address manager');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('address_manager', 'Configuration/TypoScript/Themes', 'Address manager - Themes-Settings (required for non TYPO3-Themes user!)');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('address_manager', 'Configuration/TypoScript/Addresslist/Stylesheet', 'Address manager - Address list default stylesheets');
