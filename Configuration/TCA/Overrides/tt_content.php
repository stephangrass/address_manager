<?php
defined('TYPO3_MODE') or die();

// Plugins
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'CodingMs.AddressManager',
    'Addresslist',
    'Address-Manager - List'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'CodingMs.AddressManager',
    'Addressgroupteaser',
    'Address-Manager - Group-Teaser'
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'CodingMs.AddressManager',
    'Addresssingle',
    'Address-Manager - Single-View'
);

$flexForms = ['Addresslist', 'Addressgroupteaser', 'Addresssingle'];
foreach ($flexForms as $pluginName) {
    $extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase('address_manager');
    $pluginSignature = strtolower($extensionName) . '_' . strtolower($pluginName);
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
    $flexForm = 'FILE:EXT:address_manager/Configuration/FlexForms/' . $pluginName . '.xml';
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, $flexForm);
    $GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'recursive,pages';
}
