<?php
declare(strict_types=1);

return [
    \CodingMs\AddressManager\Domain\Model\FileReference::class => [
        'tableName' => 'sys_file_reference',
    ],
    \CodingMs\AddressManager\Domain\Model\Address::class => [
        'tableName' => 'tx_addressmanager_domain_model_address',
        'properties' => [
            'creationDate' => [
                'fieldName' => 'crdate'
            ],
            'modificationDate' => [
                'fieldName' => 'tstamp'
            ],
        ],
    ],
];
