# TYPO3 Address-Manager Migration

## Version 2.0.0

The images gets a new chechbox *show in preview*, this means that you might need select the image for the list view on each address. This field name was changed after a short testing duration from `showinpreview` to `preview`, therefore you might migrate that:
`UPDATE sys_file_reference SET preview = '1' WHERE tablenames = 'tx_addressmanager_domain_model_address' AND fieldname = 'images' AND showinpreview = 1;`

## Version 1.8.0

List.html now includes a data attribute for loading info (data-hidden-class). This must contain a CSS class that adds/removes the loading info when the status changes. The content can be d-none with Bootstrap 4, hidden with Bootstrap 3.

## Version 1.7.2

The address relations have been reconfigured so that they have to be selected in the default language in translations or can only be selected using the default language. This helps to avoid confusion with relations translated with the same name. Therefore when updating to this version you should ensure that the relations are all selected in the default language!


## Version 1.4.0

1.  Pins now can have different styles. Here you might need some modifications:
    1.  If you have overridden the list item template, you have to add a new data attribute to the wrapper:
        `data-map-marker="{am:variable.get(name: 'settings.list.map.marker.style.{address.mapMarker}')}"`
    2.  The map marker definition might need a color assignment. Previously the color was set by:
        `plugin.tx_addressmanager.settings.list.map.marker.color = #337ab7`
        This setting has been removed, because the color and style can be set by using the *style* settings node, for example:
        `plugin.tx_addressmanager.settings.list.map.marker.style.default = <svg ... ><path fill="#337ab7" ...`


## Migration from ftm_ext_address

1.  MySQL-Export of all tables and replace `tx_ftmextaddress` with `tx_addressmanager` - afterwards import that data.
2.  MySQL statement `UPDATE `sys_file_reference` SET `tablenames` = 'tx_addressmanager_domain_model_address' WHERE `tablenames` LIKE '%tx_ftmextaddress%'`
3.  MySQL statement `UPDATE `tt_content` SET `list_type` = 'addressmanager_addresslist' WHERE `list_type` LIKE 'ftmextaddress_addresslist'`
4.  Search in the theme/configuration for `tx_ftmextaddress` and replace with `tx_addressmanager`.
5.  Uninstall ftm_ext_address
