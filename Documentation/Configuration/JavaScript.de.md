# JavaScript

## JavaScript-Events

Es werden folgende JavaScript-Events geworfen:

*   `address_manager.after_initialize` wird geworfen, wenn der AddressManager initialisiert wurde.
*   `address_manager.result` wird geworfen, wenn der AddressManager ein Ergebnis gefunden hat.
*   `address_manager.no_result` wird geworfen, wenn der AddressManager kein Ergebnis gefunden hat.

Die Events werden wie folgt verwendet:

```javascript
jQuery(window).on('address_manager.result', function() {
    alert('test')
});
```
