# Google Maps

Um Adressen auf der Karte darzustellen, benötigen Sie einen Google Maps Api Key.
Diesen Key erhalten Sie über die Google Cloud Platform (https://console.cloud.google.com).

## Zustimmung einholen (optional)

Im Konstanten-Manager können Sie die Funktion "Google Maps Zustimmung" aktivieren. Wenn diese Funktion aktiviert ist,
wird der Besucher zunächst gefragt, ob er Inhalte von Google Maps nachladen will.

## API-Key

Der API-Key benötigt Zugriff auf:

*   Google Maps Geocoding API (Convert between addresses and geographic coordinates.)
*   Google Maps JavaScript API
*   Google Maps Places API

Der API-Key muss an zwei Stellen hinterlegt werden:
* Der API-Key muss im Extension-Manger unter "address_manager" eingetragen werden.
  Dieser Key wird dann für die Geo-Kodierung im Backend verwendet.
* Für das Frontend muss der Key mittels der Konstante
   ```
   themes.configuration.javascript.google.maps.apiKey
   ```
   konfiguriert werden.

## Libraries

Wenn der AutoCompleter verwendet werden soll, muss die `places` Library via TypoScript-Konstante angegeben werden.


>	#### Notice: {.alert .alert-info}
>
>	Wenn die Umkreissuche verwendet wird, wird immer nach Distanz sortiert!



## Map-Marker

Die Map-Marker können via TypoScript wie folgt konfiguriert werden:

```typo3_typoscript
plugin.tx_addressmanager {
	settings {
		list {
			map {
				marker {
					width = 32
					height = 32
					style {
						default (
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f00" d="M12 0c-4.198 0-8 3.403-8 7.602 0 6.243 6.377 6.903 8 16.398 1.623-9.495 8-10.155 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.342-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
						)
						green (
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f00" d="M12 0c-4.198 0-8 3.403-8 7.602 0 6.243 6.377 6.903 8 16.398 1.623-9.495 8-10.155 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.342-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
						)
					}
				}
			}
		}
		detail {
			map {
				marker {
					width = 32
					height = 32
					style {
						default (
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f00" d="M12 0c-4.198 0-8 3.403-8 7.602 0 6.243 6.377 6.903 8 16.398 1.623-9.495 8-10.155 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.342-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
						)
						green (
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f00" d="M12 0c-4.198 0-8 3.403-8 7.602 0 6.243 6.377 6.903 8 16.398 1.623-9.495 8-10.155 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.342-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
						)
					}
				}
			}
		}
	}
}
```

Die *width* und *height* wird in Pixeln angegeben. Die Spitze des Markers muss unten ausgerichtet in der Mitte liegen.
Im `style` Knoten können beliebig viele unterschiedliche Pins definiert werden. Hierbei wird einfach der Inhalt eines
SVGs in dem value platziert. Der Eintrag *default* sollte am besten immer definiert sein, da dies der Fallback ist.

Diese Einträge müssen dann im Backend via Page-TypoScript definiert werden, so dass diese auswählbar sind:

```typo3_typoscript
TCEFORM {
    tx_addressmanager_domain_model_address {
        # Define some new marker
        map_marker.addItems {
            bad = Bad
            haus = Haus
            parken = Parken
            radstation = Radstation
            thermal = Thermal
        }
        map_marker.removeItems = pinThin
    }
}
```

## Die Entfernung in Suchergebnis-Einträgen anzeigen

Wenn Sie die Entfernung in den Suchergebnis-Einträgen anzeigen möchten, können Sie das folgende Code-Schnipsel verwenden.
Aber stellen Sie sicher das Sie die Ausgabe in eine entsprechende Anfrage setzen, weil `address.distance` unter
Umständen nicht gesetzt ist, wenn eine Adresse angegeben ist!

```xml
<f:if condition="{address.distance}">
	<f:format.number decimals="2">{address.distance}</f:format.number> KM
</f:if>
```

## Ermittlung des Besucher-Standorts

Die AddressManager Erweiterung kann mit einem Button-Klick den Standort des Besuchers abrufen und dann die
Location-Suche abschicken. Dafür müssen Sie lediglich einen Button wie folgt einbinden:

```xml
<div id="get-location-button">Get location</div>
```

## Google-Map nur initialisieren, wenn erlaubt wurde

Wenn Sie Ihre Besucher aktiv auffordern möchten, der Nutzung von Google Maps zuzustimmen, dann können Sie diese Funktion ganz einfach über den Konstanten Editor aktivieren.

Wählen Sie im Modul Template die Konstanten Editor Ansicht aus. Im Konstanten Editor suchen Sie bitte nach dem Eintrag zum Thema GoogleMapsConsent und setzen dort den Wert auf 1.

Nach dem neu Laden der Webseite wo die Google Map ausgegeben werden soll, wird eine Box angezeigt, in der eine Checkbox zu sehen ist. Damit die Google Map erscheint muss der Webseiten Besucher nun zunächst die Checkbox anklicken und sein Einverständnis speichern.

Sie können das Verhalten jederzeit durch aktivieren oder deaktivieren der Option Ein- oder Ausschalten.

