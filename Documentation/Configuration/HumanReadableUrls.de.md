# Sprechende URLs

## Slug-Konfiguration (ab TYPO3 9.5)

Für die JSON/AJAX-Anfragen brauchen Sie ein Page-Suffix wie z.B.:

```yaml
routeEnhancers:
  PageTypeSuffix:
    type: PageType
    map:
      address-manager.json: 1452982642
  AddressPlugin:
    type: Extbase
    limitToPages:
      # Page uid of list/search and show page
      - 50
    extension: AddressManager
    plugin: Addresslist
    routes:
      -
        routePath: '/{address_slug}'
        _controller: 'Address::show'
        _arguments:
          address_slug: address
      -
        routePath: '/{address_group}'
        _controller: 'Address::list'
        _arguments:
          address_group: groupSelect
    defaultController: 'Address::list'
    aspects:
      address_slug:
        type: PersistedAliasMapper
        tableName: tx_addressmanager_domain_model_address
        routeFieldName: slug
        routeValuePrefix: /
      address_group:
        type: PersistedAliasMapper
        tableName: tx_addressmanager_domain_model_addressgroup
        routeFieldName: slug
        routeValuePrefix: /
```


## Realurl-Konfiguration (bis TYPO3 9.5)

Beispiel-Konfiguration für sprechende URLs mit der Realurl-Erweiterung.


```php
$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = [
    'domain.de' => [

        'fixedPostVars' => [
            'addressConfiguration' => [
                0 => [
                    'GETvar' => 'tx_addressmanager_addresslist[action]',
                    'valueMap' => [
                        'show' => '',
                    ],
                    'noMatch' => 'bypass',
                ],
                1 => [
                    'GETvar' => 'tx_addressmanager_addresslist[controller]',
                    'valueMap' => [],
                    'noMatch' => 'bypass',
                ],
                2 => [
                    'GETvar' => 'tx_addressmanager_addresslist[address]',
                    'lookUpTable' => [
                        'table' => 'tx_addressmanager_domain_model_address',
                        'id_field' => 'uid',
                        'alias_field' => 'name',
                        'addWhereClause' => ' AND NOT deleted',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                        'languageGetVar' => 'L',
                        'languageExceptionUids' => '',
                        'languageField' => 'sys_language_uid',
                        'transOrigPointerField' => 'l10n_parent',
                        'autoUpdate' => 1,
                        'expireDays' => 180,
                    ],
                ],
            ],
            48 => 'addressConfiguration',
        ],

    ],
];
```
