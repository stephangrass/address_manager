# Google maps

In order to display addresses on a google map you need a Google Maps Api Key.
You can get it on the Google Cloud Platform (https://console.cloud.google.com).

## Ask for consent (optional)

In the constant editor you can activate the function "Google Maps Consent". If this function is activated, the user
will be asked if content from Google Maps should be loaded.

## API Key

The API key requires access to:

*   Google Maps Geocoding API (Converts between addresses and geographic coordinates.)
*   Google Maps JavaScript API
*   Google Maps Places API

The API key needs to be stored in two places:
* The API key must be made available via the extension manager configuration for the "address_manager" extension.
  This key will be used for the geo coding in the backend.
* For the frontend you need to set the key via the constant
   ```
   themes.configuration.javascript.google.maps.apiKey
   ```

## Libraries

If the the AutoCompleter is used, the `places` library must be specified via a TypoScript constant.


>	#### Notice: {.alert .alert-info}
>
>	The radial search function sorts by distance!



## Map Markers

Map markers can be configured in TypoScript as follows:

```typo3_typoscript
plugin.tx_addressmanager {
	settings {
		list {
			map {
				marker {
					width = 32
					height = 32
					style {
						default (
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f00" d="M12 0c-4.198 0-8 3.403-8 7.602 0 6.243 6.377 6.903 8 16.398 1.623-9.495 8-10.155 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.342-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
						)
						green (
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f00" d="M12 0c-4.198 0-8 3.403-8 7.602 0 6.243 6.377 6.903 8 16.398 1.623-9.495 8-10.155 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.342-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
						)
					}
				}
			}
		}
		detail {
			map {
				marker {
					width = 32
					height = 32
					style {
						default (
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f00" d="M12 0c-4.198 0-8 3.403-8 7.602 0 6.243 6.377 6.903 8 16.398 1.623-9.495 8-10.155 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.342-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
						)
						green (
							<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="#f00" d="M12 0c-4.198 0-8 3.403-8 7.602 0 6.243 6.377 6.903 8 16.398 1.623-9.495 8-10.155 8-16.398 0-4.199-3.801-7.602-8-7.602zm0 11c-1.657 0-3-1.343-3-3s1.342-3 3-3 3 1.343 3 3-1.343 3-3 3z"/></svg>
						)
					}
				}
			}
		}
	}
}
```

The *width* and *height* are given in pixels. The tip of the marker must be centered at the bottom.
Any number of different pins can be defined in the `style` node. Simply place the content of the SVG in the value.
A *default* entry should be defined as a fallback.

These entries must then be defined in the backend via Page TypoScript so that they can be selected:

```typo3_typoscript
TCEFORM {
    tx_addressmanager_domain_model_address {
        # Define a new marker
        map_marker.addItems {
            bath = Bath
            house = House
            parking = Parking
            bicyclestation = BicycleStation
            thermal = Thermal
        }
        map_marker.removeItems = pinThin
    }
}
```

## Showing distances in search results

To show distances in search results, use the following code snippet. However, wrap the variable in an if condition,
because `address.distance` is only set if a search location is given!

```xml
<f:if condition="{address.distance}">
	<f:format.number decimals="2">{address.distance}</f:format.number> KM
</f:if>
```

## Determining visitor location

The AddressManager extension can retrieve the visitor's location and start a location search with one button click.
Just add a button as follows:

```xml
<div id="get-location-button">Get location</div>
```

## Initialize Google Maps only with visitor consent

If you want to actively ask your visitors to agree to the use of Google Maps, you can easily enable this feature using the Constants Editor.

In the Template module, select the Constants Editor view. In the Constants Editor, look for the GoogleMapsConsent entry and set the value there to 1.

After reloading the web page where the Google Map is to be output, a box is displayed in which a checkbox can be seen. In order for the Google Map to appear, the website visitor must first click on the checkbox and save his consent.

You can switch the behavior on or off at any time by enabling or disabling the option.
