# JavaScript

## JavaScript Events

The following Javascript events are triggered:

*   `address_manager.after_initialize` triggered when the AddressManager is initialized.
*   `address_manager.result` triggered when the AddressManager finds a result
*   `address_manager.no_result` triggered when the AddressManager doesn't find a result

Events are used as follows: 

```javascript
jQuery(window).on('address_manager.result', function() {
    alert('test')
});
```
