# Installation

1. Installieren Sie address_manager über den Erweiterungsmanager oder mittels composer:
   ```
   composer req codingms/address-manager
   ```
2. Legen Sie einen Sys-Ordner an, in dem Sie die Address-Datensätze anlegen. Bei Bedarf legen Sie außerdem
   Datensätze vom Typ "Gruppe", "Organisation" oder "Position" an.
3. Legen Sie eine Seite an, auf der Sie die Adressdatensätze anzeigen lassen möchten. Auf dieser Seite legen Sie ein
   Plugin vom Typ "Address-Manager - List" an.
4. Setzen Sie im Konstanten-Editor unter "Address-Manager" den Wert für "Container for address records"
   (themes.configuration.container.address), tragen Sie die ID des Sys-Ordners ein, auf dem die Adressen angelegt sind.

## Wichtiger Hinweis zu Gruppen, Organisationen und Positionen
Wenn Gruppen, Organisationen oder Positionen angelegt sind, muss auch jede Adresse eine Gruppe, Organisation oder
Position zugewiesen bekommen, da sonst keine Inhalte angezeigt werden oder es zu ungewollten Suchergebnissen
kommen kann.

Gruppe, Position und Organisation müssen im Plugin-Datensatz *Filter-Felder* als erstes ausgewählt werden,
andernfalls kann es sein das diese Felder im Frontend deaktiviert angezeigt werden!
