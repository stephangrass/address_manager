# Installation

1. Install the extension using the extension manager or via composer:
   ```
   composer req codingms/address-manager
   ```
2. Create a sys-folder and create address records in that sys-folder. If necessary create "group", "organisation"
   and "position" records.
3. Create a page on which you want to display the address records. On this page create a plugin
   "Address-Manager - List".
4. Set the constant "Address-Manager" -> "Container for address records"
   (themes.configuration.container.address) to the ID of the sys-folder where the address records are stored.

## Important note on groups, organisations or positions
Whenever groups, organisations or positions are created, every address record has to have a group, organisation or
position, otherwise nothing will be shown in the list view or there will be unexpected search results.

Group, position and organisation have to be the first to be selecte din the Plugin setting "filter fields", otherwise
it is possible that these fields are displayed deactivated in the frontend.
