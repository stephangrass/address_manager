# Display address with ViewHelper

To display an address in a different template, e.g. in a website footer or in a news entry, use the GetAddressViewHelper.

```xml
<div xmlns="http://www.w3.org/1999/xhtml" lang="en"
	 xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	 xmlns:am="http://typo3.org/ns/CodingMs/AddressManager/ViewHelpers">

	<!-- Get address by uid -->
	<f:debug>{am:getAddress(uid: 123)}</f:debug>

	<!-- Get address by name -->
	<f:debug>{am:getAddress(name: 'Dubai')}</f:debug>

 </div>
 ```
