# Include OpenStreetMap

If you want to use OpenStreetMap instead of Google Maps, do the following:

## TypoScript

Include the following Setup-TypoScript:

```
# Include OpenStreetMap and remove Google Maps libraries
page {
	includeJSFooterlibs {
		openLayers = https://openlayers.org/api/OpenLayers.js
		openStreetMap = https://openstreetmap.org/openlayers/OpenStreetMap.js
		openStreetMapLocal = typo3conf/ext/address_manager/Resources/Public/JavaScript/Openstreetmap.js
		googleMaps =
		googleMapsHtmlMarker =
		googleMapsClustering =
	}
}
```

## List view

* Include the OpenStreetMap copyright notice

Template file: `Resources/Private/Templates/Address/List.html`

```
<f:if condition="{settings.useMapOverview}">
	<f:render partial="Address/GoogleMapsConsent" section="{settings.framework}" arguments="{_all}" />
	<div id="map-wrapper">
		<div id="address-list-map"
			 class="card"
			 data-country-restriction="{settings.list.map.countryRestriction}"
			 data-max-zoom="{settings.list.map.maxZoom}"
			 data-style="{settings.list.map.style}"
			 data-marker-width="{settings.list.map.marker.width}"
			 data-marker-height="{settings.list.map.marker.height}"
			 data-clustering-active="{settings.list.map.clustering.active}"
			 data-clustering-image-path="{settings.list.map.clustering.imagePath}">
			<div id="address-list-map-canvas"></div>
		</div>
		<div id="osm">© <a href="http://www.openstreetmap.org">OpenStreetMap</a>
			und <a href="http://www.openstreetmap.org/copyright">Mitwirkende</a>,
			<a href="http://creativecommons.org/licenses/by-sa/2.0/deed.de">CC-BY-SA</a>
		</div>
	</div>
</f:if>
```

## Detail view

* Include the OpenStreetMap copyright notice
* If necessary, adjust the height of the map section. To do this, replace the two places where "250px" is written, for example by "500px"

Template file: `Resources/Private/Templates/Address/Show.html`

```
<div class="address-detail-map-wrapper" style="width: 100%;height: 250px">
	<f:render partial="Address/GoogleMapsConsent" section="{settings.framework}" arguments="{_all}" />
	<div id="map-wrapper">
		<div id="address-detail-map"
			 data-tooltip="{address.name}"
			 data-zoom="{address.mapZoom}"
			 data-style="{settings.detail.map.style}"
			 data-latitude="{address.mapLatitude}"
			 data-longitude="{address.mapLongitude}"
			 data-map-marker="{am:variable.get(name: 'settings.list.map.marker.style.{address.mapMarker}')}"
			 data-marker-width="{settings.detail.map.marker.width}"
			 data-marker-height="{settings.detail.map.marker.height}"
			 style="width: 100%; height: 250px;"></div>
	</div>
	<div id="osm">© <a href="http://www.openstreetmap.org">OpenStreetMap</a>
		und <a href="http://www.openstreetmap.org/copyright">Mitwirkende</a>,
		<a href="http://creativecommons.org/licenses/by-sa/2.0/deed.de">CC-BY-SA</a>
	</div>
```

## Adjustments

To make individual adjustments, have a look at the script `Resources/Public/JavaScript/Openstreetmap.js`. You can copy this, modify it and integrate it instead of the supplied version.
