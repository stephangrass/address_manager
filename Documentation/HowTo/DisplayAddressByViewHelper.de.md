# Eine Adresse mit dem ViewHelper anzeigen

Wenn Sie eine Adresse in einem anderen Template darstellen wollen, bspw. im Footer einer Website oder auch zu einem News-Eintrag, so können Sie dies mit dem mitgelieferten GetAddressViewHelper erreichen.

```xml
<div xmlns="http://www.w3.org/1999/xhtml" lang="en"
	 xmlns:f="http://typo3.org/ns/fluid/ViewHelpers"
	 xmlns:am="http://typo3.org/ns/CodingMs/AddressManager/ViewHelpers">

	<!-- Get address by uid -->
	<f:debug>{am:getAddress(uid: 123)}</f:debug>

	<!-- Get address by name -->
	<f:debug>{am:getAddress(name: 'Dubai')}</f:debug>

 </div>
 ```
