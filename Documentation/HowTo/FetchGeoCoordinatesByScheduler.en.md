# Determine geo-coordinates using the scheduler

The Address Manager extension has a CommandController in its scheduler for easily determining the geo-coordinates of your addresses.

The scheduler has the following parameters:

*   **apiKey:** A Google Maps API key, which includes server-side restrictions and the GeoCoding and Places APIs.
*   **useFields:** A comma-separated list of database fields from the address table which locates the address.
*   **limit:** Maximum number of addresses to be processed in one execution. 0 indicates no limit.
