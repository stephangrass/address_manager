# Add new address groups

Address groups can be modified in Page-TypoScript. The following code snippet removes the existing entries: 

```typo3_typoscript
TCEFORM {
	tx_addressmanager_domain_model_addressgroup {
		variant {
			removeItems = default, primary, secondary, success, info, warning, danger
		}
    }
}
```

Use this snippet to add your own entries:

```typo3_typoscript
TCEFORM {
	tx_addressmanager_domain_model_addressgroup {
		variant {
			addItems.wordpress = Wordpress
			addItems.shopware = Shopware
			addItems.magento = Magento
			addItems.contao = Contao
			addItems.drupal = Drupal
		}
    }
}
```
