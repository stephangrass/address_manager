# Zurücksetzen Button erstellen

Ein Zurücksetzen-Button für das Filter Formular lässt sich wie folgt erstellen:

```javascript
address_manager.resetAllData();
```

```html
<a href="#" onclick="address_manager.resetAllData();return false">Reset</a>
```
