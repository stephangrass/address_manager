# Create a reset button

Create a reset button for the filter form as follows:

```javascript
address_manager.resetAllData();
```

```html
<a href="#" onclick="address_manager.resetAllData();return false">Reset</a>
```
