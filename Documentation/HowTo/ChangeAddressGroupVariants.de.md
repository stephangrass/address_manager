# Adressgruppen Varianten anpassen

Die Adressgruppen Varianten können einfach mit Page-TypoScript angepasst werden. Mit dem folgenden Snippet entfernen Sie die vorhandenen Einträgen:

```typo3_typoscript
TCEFORM {
	tx_addressmanager_domain_model_addressgroup {
		variant {
			removeItems = default, primary, secondary, success, info, warning, danger
		}
    }
}
```

Mit dem nächsten Snippet können Sie eigene Einträge bereitstellen:

```typo3_typoscript
TCEFORM {
	tx_addressmanager_domain_model_addressgroup {
		variant {
			addItems.wordpress = Wordpress
			addItems.shopware = Shopware
			addItems.magento = Magento
			addItems.contao = Contao
			addItems.drupal = Drupal
		}
    }
}
```
