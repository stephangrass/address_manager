# Adressliste mit übergebener Adressgruppe öffnen

Die Adressgruppen haben im AddressManager ein paar Zusatzfeatures. Durch das zusätzliche Plugin für den Adressgruppen-Teaser ist es möglich die anzuzeigende Gruppe der Adresselist via Parameter zu übergeben. Dies kann mit dem folgenden Parameter erreicht werden:

```text
?tx_addressmanager_addresslist%5BgroupSelect%5D=42
bzw
?tx_addressmanager_addresslist[groupSelect]=42
```
