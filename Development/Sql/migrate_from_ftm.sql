RENAME TABLE `tx_ftmextaddress_domain_model_address` TO `tx_addressmanager_domain_model_address`;
RENAME TABLE `tx_ftmextaddress_domain_model_addressgroup` TO `tx_addressmanager_domain_model_addressgroup`;
RENAME TABLE `tx_ftmextaddress_domain_model_addressposition` TO `tx_addressmanager_domain_model_addressposition`;
RENAME TABLE `tx_ftmextaddress_domain_model_addressorganisation` TO `tx_addressmanager_domain_model_addressorganisation`;

RENAME TABLE `tx_ftmextaddress_address_addressgroup_mm` TO `tx_addressmanager_address_addressgroup_mm`;
RENAME TABLE `tx_ftmextaddress_address_addressposition_mm` TO `tx_addressmanager_address_addressposition_mm`;
RENAME TABLE `tx_ftmextaddress_address_addressorganisation_mm` TO `tx_addressmanager_address_addressorganisation_mm`;


# --> SELECT * FROM `tt_content` WHERE `list_type` LIKE 'ftmextaddress_addresslist'
# ---> addressmanager_addresslist


# --> SELECT CONCAT(uid, ',') FROM `sys_file_reference` WHERE `tablenames` LIKE 'tx_ftmextaddress_domain_model_address' ORDER BY `tablenames` DESC
# ---> UPDATE `sys_file_reference` SET `tablenames` = 'tx_addressmanager_domain_model_address' WHERE `sys_file_reference`.`uid` IN (3509, 2879, 2878, 2877, 2876, 2875, 2874, 2867, 2866, 2865, 2864, 2863, 2862, 2861, 2805, 2880, 2881, 3508, 3507, 2893, 2892, 2891, 2890, 2889, 2888);
