<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'CodingMs.address_manager',
    'Addresslist',
    ['Address' => 'list, show'],
    // Don't cache list view, because of passed search parameters
    ['Address' => 'list']
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'CodingMs.address_manager',
    'Addressgroupteaser',
    ['Address' => 'groupTeaser'],
    []
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'CodingMs.address_manager',
    'JsonApi',
    ['JsonApi' => 'select'],
    ['Address' => 'select']
);
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'CodingMs.address_manager',
    'Addresssingle',
    ['Address' => 'showSingle']
);
//
// Page TypoScript
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
    '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:address_manager/Configuration/PageTS/tsconfig.typoscript">'
);
//
// Add new field type to NodeFactory in order to render the map_wizard
$GLOBALS['TYPO3_CONF_VARS']['SYS']['formEngine']['nodeRegistry'][1591191326] = [
    'nodeName' => 'geoLocationWizard',
    'priority' => '70',
    'class' => \CodingMs\AddressManager\Form\Element\GeoLocationElement::class,
];
