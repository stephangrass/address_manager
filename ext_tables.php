<?php

defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function () {
        //
        // register svg icons: identifier and filename
        $iconsSvg = [
            'module-address-manager' => 'Resources/Public/Icons/module-address-manager.svg',
            'contains-addresses' => 'Resources/Public/Icons/iconmonstr-id-card-8.svg',
            'apps-pagetree-folder-contains-addresses' => 'Resources/Public/Icons/iconmonstr-id-card-8.svg',
            'content-plugin-addressmanager-addresslist' => 'Resources/Public/Icons/iconmonstr-map-9.svg',
            'mimetypes-x-content-addressmanager-address' => 'Resources/Public/Icons/iconmonstr-id-card-8.svg',
            'mimetypes-x-content-addressmanager-addressgroup' => 'Resources/Public/Icons/iconmonstr-link-2.svg',
            'mimetypes-x-content-addressmanager-addressorganisation' => 'Resources/Public/Icons/iconmonstr-link-2.svg',
            'mimetypes-x-content-addressmanager-addressposition' => 'Resources/Public/Icons/iconmonstr-link-2.svg',
        ];
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        foreach ($iconsSvg as $identifier => $path) {
            $iconRegistry->registerIcon(
                $identifier,
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => 'EXT:address_manager/' . $path]
            );
        }
        //
        // Table configuration arrays
        $tables = [
            'tx_addressmanager_domain_model_address' => [
                'csh' => 'EXT:address_manager/Resources/Private/Language/locallang_csh_address.xlf'
            ],
            'tx_addressmanager_domain_model_addressgroup' => [
                'csh' => 'EXT:address_manager/Resources/Private/Language/locallang_csh_addressgroup.xlf'
            ],
            'tx_addressmanager_domain_model_addressorganisation' => [
                'csh' => 'EXT:address_manager/Resources/Private/Language/locallang_csh_addressorganisation.xlf'
            ],
            'tx_addressmanager_domain_model_addressposition' => [
                'csh' => 'EXT:address_manager/Resources/Private/Language/locallang_csh_addressposition.xlf'
            ]
        ];
        foreach ($tables as $table => $data) {
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr($table, $data['csh']);
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages($table);
        }
    }
);
